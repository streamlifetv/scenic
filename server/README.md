# Scenic Backend
The backend is a custom assembly of [ExpressJS](http://expressjs.com/) and [Socket.io](http://socket.io/) that uses a
node addon wrapping [switcher](https://github.com/sat-metalab/switcher).

Since the backend uses node 5.11, ES2015/ES6 greatness is allowed and welcomed.

## Directory Structure

### server/src
Server source files

#### server/src/controller
ExpressJS routes controller.

#### server/src/exceptions
Custom exception classes.

#### server/src/lib
Helper libs.

#### server/src/net
Modules related to client/server communications.

##### server/src/net/commands
List of commands callable by the client. The commands wrap methods of the managers with boilerplate regarding their use
in the context of client/server communication that should not go inside the managers. They are executed in the context 
of the client in order to have access to localization methods for the error messages.

#### server/src/scenic
Modules related to the interaction with **switcher**.

#### server/src/settings
Server configuration

#### server/src/templates
Pages to be server to the client.

#### server/src/utils
Utility modules.

### server/test
Server test files