"use strict";

const fs       = require( 'fs' );
const log      = require( '../../lib/logger' );
const Quiddity = require( "./Quiddity" );

/**
 * Timelapse Quiddity
 */
class Timelapse extends Quiddity {

    /**
     * Create a new Timelapse
     *
     * @param {SwitcherController} switcherController
     * @param {string} id
     * @param {Object} [initProperties]
     * @param {Object} [config]
     */
    constructor( switcherController, id, initProperties, config ) {
        super( switcherController, "timelapse", id, initProperties, config );
        this.managedProperties['last_image'] = true;

        if ( !config.path ) {
            config.path = "/tmp";
        }

        this.type = initProperties.type;

        this.switcherController.switcher.set_property_value( id, 'imgdir', `${config.path}/${id}` );
        this.switcherController.switcher.set_property_value( id, 'framerate', `${initProperties.framerate}/1` );
        this.switcherController.switcher.set_property_value( id, 'num_files', String( true ) );
        this.switcherController.switcher.set_property_value( id, 'notify_last_files', String( true ) );
        this.switcherController.switcher.set_property_value( id, 'maxfiles', String( 2 ) );
        this.switcherController.switcher.set_property_value( id, 'quality', String( initProperties.quality ) );
        this.switcherController.switcher.set_property_value( id, 'width', String( initProperties.width ) );
        this.switcherController.switcher.set_property_value( id, 'height', String( initProperties.height ) );
    }

    /**
     * Property Changed
     *
     * @param {string} property
     * @param {string} value
     */
    onProperty( property, value ) {
        switch( property ) {
            case 'last_image':
                if ( value != this.lastImage ) {
                    // THUMBNAIL PUBLISHING
                    var shmdata    = value.substring( value.lastIndexOf( '/' ) + 1, value.lastIndexOf( '_' ) );
                    var interested = this.switcherController.clients.filter( client => {
                        const sub = client.previewSubscriptions[this.id];
                        return sub === true || ( sub && sub.shmdata == shmdata );
                    } );

                    // Don't read if nobody cares
                    // TODO: Investigate if streaming the file would be a better option (more io but less ram...)
                    if ( interested.length ) {
                        fs.readFile( value, ( err, data ) => {
                            if ( err ) {
                                return log.warn( 'Could not read file ' + value + ' while trying to send ' + this.type );
                            }
                            interested.forEach( client => {
                                if ( client.socket ) {
                                    client.socket.emit( "shmdata." + this.type, shmdata, data );
                                }
                            } );
                        } );
                    }
                    this.lastImage = value;
                }
                break;
        }
    }

}

module.exports = Timelapse;