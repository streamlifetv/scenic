"use strict";

const fs       = require( 'fs' );
const log      = require( '../../lib/logger' );
const Quiddity = require( "./Quiddity" );

/**
 * Timelapse Quiddity
 */
class UserQuiddity extends Quiddity {

    /**
     * Create a new User quiddity
     *
     * @param {SwitcherController} switcherController
     * @param {string} id
     * @param {Object} [initUserData]
     * @param {Object} [config]
     */
    constructor( switcherController, id, initUserData, config ) {
        super( switcherController, "emptyquid", id, initUserData, config );

        this.type = initUserData.type;

        if ( initUserData ) {
            var result = this.switcherController.switcher.set_user_data_branch( id , initUserData.path, JSON.stringify( initUserData.value ) );
            if ( result && result.error ) {
                throw new Error( result.error );
            }
        }
    }
}

module.exports = UserQuiddity;