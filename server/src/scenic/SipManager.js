'use strict';

const _           = require( 'underscore' );
const fs          = require( 'fs' );
const i18n        = require( 'i18next' );
const cryptoJS    = require( 'crypto-js' );
const async       = require( 'async' );
const log         = require( '../lib/logger' );
const BaseManager = require( './BaseManager' );
const SIP         = require( './quiddities/SIP' );

const secretString = 'Les patates sont douces!';

/**
 * SIP Manager
 * @extends BaseManager
 */
class SipManager extends BaseManager {

    /**
     * Make a SIP Manager instance
     */
    constructor( switcherController ) {
        super( switcherController );
        this.contacts = {};
    }

    /**
     * @override
     */
    initialize() {
        super.initialize();

        this._loadContacts();
    }

    //  ██████╗  █████╗ ██████╗ ███████╗███████╗██████╗ ███████╗
    //  ██╔══██╗██╔══██╗██╔══██╗██╔════╝██╔════╝██╔══██╗██╔════╝
    //  ██████╔╝███████║██████╔╝███████╗█████╗  ██████╔╝███████╗
    //  ██╔═══╝ ██╔══██║██╔══██╗╚════██║██╔══╝  ██╔══██╗╚════██║
    //  ██║     ██║  ██║██║  ██║███████║███████╗██║  ██║███████║
    //  ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝╚══════╝

    /**
     * Parse contact into a more manageable format for the client
     *
     * @param contact
     * @private
     * @deprecated
     */
    _parseContact( contact ) {
        contact.id = contact.uri;
        return contact;
    };

    //  ██╗███╗   ██╗████████╗███████╗██████╗ ███╗   ██╗ █████╗ ██╗     ███████╗
    //  ██║████╗  ██║╚══██╔══╝██╔════╝██╔══██╗████╗  ██║██╔══██╗██║     ██╔════╝
    //  ██║██╔██╗ ██║   ██║   █████╗  ██████╔╝██╔██╗ ██║███████║██║     ███████╗
    //  ██║██║╚██╗██║   ██║   ██╔══╝  ██╔══██╗██║╚██╗██║██╔══██║██║     ╚════██║
    //  ██║██║ ╚████║   ██║   ███████╗██║  ██║██║ ╚████║██║  ██║███████╗███████║
    //  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝  ╚═╝╚══════╝╚══════╝

    /**
     * Load contacts from file
     *
     * @returns {Object} Contact list
     * @private
     */
    _loadContacts() {
        log.info( 'Loading contacts', this.config.contactsPath );
        var contacts = {};
        var exists   = fs.existsSync( this.config.contactsPath );

        if ( exists ) {
            try {
                var data           = fs.readFileSync( this.config.contactsPath );
                var parsedContacts = JSON.parse( data );
                if ( parsedContacts ) {
                    // Cleanup
                    // We have those appearing at one point during dev, just clearing them out
                    _.values( parsedContacts ).forEach( contact => delete contact['null'] );
                    contacts = parsedContacts;
                }
            } catch ( e ) {
              log.warn( 'Error reading contacts file', e);
            }
        }
        return this.contacts = contacts;
    };

    /**
     * Save Contacts
     *
     * @param {Function} [callback]
     * @private
     */
    saveContacts( callback ) {
        log.info( 'Saving contacts', this.config.contactsPath );
        fs.writeFile( this.config.contactsPath, JSON.stringify( this.contacts ), error => {
            if ( error ) {
                log.error( error );
            }
            if ( callback ) {
                return callback( error );
            }
        } );
    };

    /**
     * Reconnect the call to a contact
     *
     * @param {string} uri - Contact's URI
     * @returns {boolean} - Success of the reconnection or true if it wasn't necessary to reconnect
     * @private
     */
    _reconnect( uri ) {
        var contacts = this.getContacts();
        if ( !contacts ) {
            log.warn( 'Could not get contacts while trying to reconnect' );
            return false;
        }
        var contact = _.findWhere( contacts, { uri: uri } );
        if ( !contact ) {
            log.warn( 'Could not find contact while trying to reconnect', uri );
            return false;
        } else if ( contact.send_status == 'calling' ) {
            log.debug( 'Reconnecting to', uri );
            var hungUp = this.hangUpContact( uri );
            if ( !hungUp ) {
                log.warn( 'Could not hang up on contact while trying to reconnect', uri );
            }
            var called = this.callContact( uri );
            if ( !called ) {
                log.warn( 'Could not call contact while trying to reconnect', uri );
            }
            // Only the end result of establishing th call is important
            return called;
        } else {
            return true;
        }
    };

    /**
     * Get self uri from the sip tree
     *
     * @returns {string} self uri
     * @private
     */
    getSelf() {
        // TODO: Only get self when tree is modified to support returning values
        try {
            var tree = this.switcherController.quiddityManager.getTreeInfo( this.config.sip.quiddName );
        } catch ( e ) {
            log.error( e );
        }
        if ( !tree ) {
            log.warn( 'Could not get SIP tree' );
            return null;
        }
        return !_.isEmpty( tree.self ) ? tree.self : null;
    };

    //  ██╗      ██████╗  ██████╗ ██╗███╗   ██╗
    //  ██║     ██╔═══██╗██╔════╝ ██║████╗  ██║
    //  ██║     ██║   ██║██║  ███╗██║██╔██╗ ██║
    //  ██║     ██║   ██║██║   ██║██║██║╚██╗██║
    //  ███████╗╚██████╔╝╚██████╔╝██║██║ ╚████║
    //  ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝╚═╝  ╚═══╝

    /**
     * SIP Login
     *
     * @param {Object} credentials - Credentials used to log into SIP
     * @returns {Boolean} Success
     */
    login( credentials ) {
        log.info( 'SIP login attempt: ' + credentials.user + '@' + credentials.server + ':' + credentials.port );

        this.config.sip.port = parseInt( credentials.port );

        var uri = credentials.user + '@' + credentials.server;

        // Check if we already have a SIP quiddity and create it if necessary
        var hasSip = this.switcherController.quiddityManager.exists( this.config.sip.quiddName );
        if ( !hasSip ) {
            log.debug( 'Creating new SIP quiddity' );
            const sip = new SIP(
                this.switcherController,
                this.config.sip.quiddName,
                this.config.sip
            );

            this.switcherController.quiddityManager.managedQuiddities[sip.id] = sip;
            //var sip = this.switcherController.quiddityManager.create( 'sip', this.config.sip.quiddName );
            if ( sip == null ) {
                log.error( 'Could not create SIP quiddity' );
                return false;
            }
        }

        // Port
        log.debug( 'Setting SIP port' );
        var portSet = this.switcherController.quiddityManager.setPropertyValue( this.config.sip.quiddName, 'port', this.config.sip.port );
        if ( !portSet ) {
            log.error( 'Could not set SIP port', this.config.sip.port );
            return false;
        }

        // STUN/TURN
        if ( credentials.stunServer ) {
            log.debug( 'Configuring STUN/TURN' );
            var decrypted  = cryptoJS.AES.decrypt( credentials.turnPassword, secretString ).toString( cryptoJS.enc.Utf8 );
            var configured = this.switcherController.quiddityManager.invokeMethod(
                this.config.sip.quiddName,
                'set_stun_turn',
                [
                    credentials.stunServer,
                    credentials.turnServer,
                    credentials.turnUser,
                    decrypted
                ]
            );
            if ( !configured ) {
                log.warn( 'STUN/TURN Configuration failed' );
                return false;
            }
        }

        // Register
        log.debug( 'Registering user' );
        var decrypted  = cryptoJS.AES.decrypt( credentials.password, secretString ).toString( cryptoJS.enc.Utf8 );
        var registered = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'register', [uri, decrypted] );
        if ( !registered ) {
            log.warn( 'SIP Authentication failed' );
            return false;
        }

        return true;
    };

    /**
     * Logout
     *
     * @returns {Boolean} Success
     */
    logout() {
        log.info( 'Logging out of SIP' );

        var loggedOut = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'unregister', [] );
        if ( !loggedOut ) {
            log.warn( 'Could not log out of SIP' );
            return false;
        }

        return true;
    };

    //   ██████╗ ██████╗ ███╗   ██╗████████╗ █████╗  ██████╗████████╗███████╗
    //  ██╔════╝██╔═══██╗████╗  ██║╚══██╔══╝██╔══██╗██╔════╝╚══██╔══╝██╔════╝
    //  ██║     ██║   ██║██╔██╗ ██║   ██║   ███████║██║        ██║   ███████╗
    //  ██║     ██║   ██║██║╚██╗██║   ██║   ██╔══██║██║        ██║   ╚════██║
    //  ╚██████╗╚██████╔╝██║ ╚████║   ██║   ██║  ██║╚██████╗   ██║   ███████║
    //   ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝   ╚═╝   ╚══════╝


    /**
     * Detect if the self contact is in a not bother mode.
     *
     * @returns {Boolean}
     * @private
     */
    isInNotBotherMode() {
        var selfURI = this.getSelf();
        var contacts = this.contacts[selfURI];
        var notBother = false;

        _.each( contacts,  contact => {
            if(contact.blocked == true ) {
                notBother = true;
            }
        })

        return notBother;
    };

    /**
     * Add Contact to SIP
     *
     * @param {string} uri - Contact's URI
     * @param {string} name - Contact's name
     * @param {Boolean} doNotSave - Skip the saving of the contacts (when doing batch adding, for example)
     */
    addContact( uri, name, authorized, doNotSave ) {
        if ( _.isEmpty( name ) ) {
            name = uri;
        }

        var contactAdded = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'add_buddy', [uri] );
        if ( !contactAdded ) {
            log.warn( 'Could not add contact', uri );
            return false;
        }

        var setName = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'name_buddy', [name, uri] );
        if ( !setName ) {
            log.warn( 'Could not set contact name', uri, name );
            return false;
        }

        var whitelisted = authorized != undefined ? authorized : true;
        var setAuthorized = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'authorize', [uri, whitelisted] );
        if ( !setAuthorized ) {
            log.warn( 'Could not set contact authorization ', uri, name );
            return false;
        }

        // Get self from tree
        var selfURI = this.getSelf();
        if ( selfURI ) {
            // Add to local contact list
            this._loadContacts();
            if ( !this.contacts[selfURI] ) {
                this.contacts[selfURI] = {};
            }
            this.contacts[selfURI][uri] = { 'name': name, 'authorized' :whitelisted, 'blocked': false };
            if ( !doNotSave ) {
                this.saveContacts();
            }
        } else {
            log.warn( 'Self URI not found' );
        }

        return true;
    };

    /**
     * Remove Contact from SIP
     *
     * @param {string} uri - Contact's URI
     * @param {Boolean} doNotSave - Skip the saving of the contacts (when doing batch adding, for example)
     * @returns {Boolean} Success of the removal
     */
    removeContact( uri, doNotSave ) {
        log.info( 'Removing contact', uri );
        var selfURI = this.getSelf();
        if ( selfURI && this.contacts[selfURI] && this.contacts[selfURI][uri] ) {
            delete this.contacts[selfURI][uri];
            if ( !doNotSave ) {
                this.saveContacts();
            }
        }

        var contactRemoved = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'del_buddy', [uri] );
        if ( !contactRemoved ) {
            log.warn( 'Could not remove contact', uri );
            return false;
        }

        return true;
    };

    /**
     * Get Contacts List
     *
     * @returns {Array} List of contacts
     * @deprecated
     */
    getContacts() {
        log.info( 'Getting contacts' );

        var hasSipQuiddity = this.switcherController.quiddityManager.exists( this.config.sip.quiddName );
        if ( !hasSipQuiddity ) {
            log.warn( 'Trying to get contacts without a SIP quiddity' );
            return null;
        }

        try {
            var contacts = this.switcherController.quiddityManager.getTreeInfo( this.config.sip.quiddName, '.buddies' );
        } catch ( e ) {
            log.error( e );
        }
        if ( !contacts ) {
            log.warn( 'Could not get contacts from SIP quiddity' );
            return null;
        }

        // Parse contacts
        contacts = _.values( contacts );
        _.each( contacts, this._parseContact, this );

        return contacts;
    };

    /**
     * Authorize contact
     *
     * @param {string} uri - Contact's URI
     * @param {Boolean} authorized
     * @returns {Boolean} Success
     */
    authorizeContact( uri, authorized ) {
        log.info( 'blockContact contact', uri, authorized );

        var success = true;
        var result = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'authorize', [uri, authorized] );

        if ( !result ) {
            log.warn( 'Could not block contact ', uri, authorized );
            success = false;
        } else {
            // Add to local contact list
            var selfURI = this.getSelf();
            if ( selfURI && this.contacts[selfURI] ) {
                this.contacts[selfURI][uri].authorized = authorized;
                this.saveContacts();
            }
        }
        return success;
    }

    /**
     * Block all contacts
     *
     * @returns {Boolean} Success
     */
    blockAllContact() {
        log.info( 'blocked All Contact');
        this._loadContacts();
        var selfURI = this.getSelf();
        if ( selfURI && this.contacts[selfURI] ) {
            var contacts = this.getContacts();
            var success = true;

            contacts.forEach( contact => {
                if ( contact.uri != selfURI && contact.whitelisted == true && ( contact.connections == undefined || contact.connections.length == 0 )) {
                    var result = this.switcherController.quiddityManager.invokeMethod(this.config.sip.quiddName, 'authorize', [contact.uri, false]);

                    if (!result) {
                        log.warn('Could not block contact ', contact.uri, false);
                        success = false;
                    } else {
                        // Update local contact list
                        this.contacts[selfURI][contact.uri].authorized = false;
                        this.contacts[selfURI][contact.uri].blocked  = true;

                    }
                } else {
                    this.contacts[selfURI][contact.uri].blocked  = false;
                }
            });

            this.saveContacts();

            return success;
        }
        return false;
    }


    /**
     * unblocked  all contacts
     *
     * @returns {Boolean} Success
     */
    unblockAllContact() {
        log.info( 'unblocked All Contact');
        this._loadContacts();
        var selfURI = this.getSelf();
        if ( selfURI && this.contacts[selfURI] ) {
            var contacts = this.getContacts();
            var success = true;

            contacts.forEach( contact => {
                if ( contact.uri != selfURI && this.contacts[selfURI][contact.uri].blocked == true ) {
                    var result = this.switcherController.quiddityManager.invokeMethod(this.config.sip.quiddName, 'authorize', [contact.uri, true]);

                    if (!result) {
                        log.warn('Could not unblock contact ', contact.uri, true);
                        success = false;
                    } else {
                        // Update local contact list
                        this.contacts[selfURI][contact.uri].authorized = true;
                        this.contacts[selfURI][contact.uri].blocked  = false;

                    }
                }
            });

            this.saveContacts();
            return success;
        }
        return false;
    }

    /**
     * Update contact
     *
     * @param {string} uri - Contact's URI
     * @param {Object} info - Object containing the information to update
     * @returns {Boolean} Success
     */
    updateContact( uri, info ) {
        log.info( 'Updating contact', uri, info );

        var success = true;

        if ( info.name ) {
            log.debug( 'Updating name of the contact ' + uri + ' to ' + info.name );
            var nameUpdated = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'name_buddy', [info.name, uri] );
            if ( !nameUpdated ) {
                log.warn( 'Could not update contact name', uri, info.name );
                success = false;
            } else {
                // Add to local contact list
                var selfURI = this.getSelf();
                if ( selfURI && this.contacts[selfURI] ) {
                    this.contacts[selfURI][uri].name = info.name;
                    this.saveContacts();
                }
            }
        }

        if ( info.status && uri == this.getSelf() ) {
            log.debug( 'Updating status of the contact ' + uri + ' to ' + info.status );
            // Be careful, status needs to be uppercase to be recognized by switcher
            var statusSet = this.switcherController.quiddityManager.setPropertyValue( this.config.sip.quiddName, 'status', info.status.toUpperCase() );
            if ( !statusSet ) {
                log.warn( 'Could not change contact status', uri, info.status );
                success = false;
            }
        }

        if ( info.status_text && uri == this.getSelf() ) {
            log.debug( 'Updating status text of the contact ' + uri + ' to ' + info.status_text );
            var textSet = this.switcherController.quiddityManager.setPropertyValue( this.config.sip.quiddName, 'status-note', info.status_text );
            if ( !textSet ) {
                log.warn( 'Could not change contact status message', uri, info.status_text );
                success = false;
            }
        }

        return success;
    };

    //   ██████╗ ██████╗ ███╗   ██╗███╗   ██╗███████╗██╗  ██╗██╗ ██████╗ ███╗   ██╗███████╗
    //  ██╔════╝██╔═══██╗████╗  ██║████╗  ██║██╔════╝╚██╗██╔╝██║██╔═══██╗████╗  ██║██╔════╝
    //  ██║     ██║   ██║██╔██╗ ██║██╔██╗ ██║█████╗   ╚███╔╝ ██║██║   ██║██╔██╗ ██║███████╗
    //  ██║     ██║   ██║██║╚██╗██║██║╚██╗██║██╔══╝   ██╔██╗ ██║██║   ██║██║╚██╗██║╚════██║
    //  ╚██████╗╚██████╔╝██║ ╚████║██║ ╚████║███████╗██╔╝ ██╗██║╚██████╔╝██║ ╚████║███████║
    //   ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝

    /**
     * Attach shmdata to SIP contact
     *
     * @param {string} uri - URI of the contact
     * @param {string} path - Path of the shmdata
     * @returns {boolean} - Success
     */
    attachShmdataToContact( uri, path ) {
        log.info( 'Attaching shmdata', path, 'to contact', uri );
        var attached = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'attach_shmdata_to_contact', [path, uri, String( true )] );
        if ( !attached ) {
            log.warn( 'Could not attach shmdata', path, 'to contact', uri );
            return false;
        }
        var reconnected = this._reconnect( uri );
        return attached && reconnected;
    };

    /**
     * Detach shmdata from SIP contact
     *
     * @param {string} uri - Contact URI
     * @param {string} path - Shmdata path
     */
    detachShmdataFromContact( uri, path ) {
        log.info( 'Detaching shmdata', path, 'from contact', uri );
        var detached = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'attach_shmdata_to_contact', [path, uri, String( false )] );
        if ( !detached ) {
            log.warn( 'Could not detach shmdata', path, 'from contact', uri );
            return false;
        }
        var reconnected = this._reconnect( uri );
        return detached && reconnected;
    };

    /**
     * Call a SIP contact
     *
     * @param {string} uri - Contact's URI
     * @returns {boolean} - Success
     */
    callContact( uri ) {
        log.info( 'Calling contact', uri );
        var called = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'send', [uri] );
        if ( !called ) {
            log.warn( 'Could not call contact', uri );
        }
        return called;
    };

    /**
     * Hang up on the contact
     *
     * @param {string} uri - Contact's URI
     * @returns {boolean} - Success
     */
    hangUpContact( uri ) {
        log.info( 'Hanging up on contact', uri );
        var hungUp = this.switcherController.quiddityManager.invokeMethod( this.config.sip.quiddName, 'hang-up', [uri] );
        if ( !hungUp ) {
            log.warn( 'Could not hang up on contact' );
        }
        return hungUp;
    };

    /**
     * Hang up on all outgoing calls
     */
    hangUpAllCalls() {
        log.info( 'Hanging up all calls' );
        var contacts = this.getContacts();
        let allHungUp = true;
        if ( contacts ) {
            contacts.forEach( contact => {
                const hungUp = this._cleanUpContact( contact );
                if ( !hungUp ) {
                    allHungUp = false;
                }
            } );
        }
        return allHungUp;
    };

    /**
     * Disconnect from the contact
     *
     * @param {string} uri - Contact's URI
     * @returns {boolean} - Success
     */
    disconnectContact( uri ) {
        log.info( 'Disconnecting contact', uri );

        // Get the contact list, without it we can't disconnect a contact
        var contacts = this.getContacts();
        if ( !contacts ) {
            return false;
        }

        // Find the related contact, without it there's nothing to disconnect
        var contact = _.findWhere( contacts, { uri: uri } );
        if ( !contact ) {
            log.warn( 'Contact not found', uri );
            return false;
        }

        return this._cleanUpContact(contact);
    };

    _cleanUpContact(contact) {
        // If we are calling, hang up first
        if ( contact.send_status == 'calling' ) {
            var hungUp = this.hangUpContact( contact.uri );
            if ( !hungUp ) {
                return false;
            }
        }

        // Then detach shmdatas
        var allDetached = true;
        if ( contact.connections && _.isArray( contact.connections ) ) {
            _.each( contact.connections, function ( path ) {
                var detached = this.detachShmdataFromContact( contact.uri, path );
                if ( !detached ) {
                    allDetached = false;
                }
            }, this );
        }

        return allDetached
    }
}

module.exports = SipManager;
