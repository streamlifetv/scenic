'use strict';

// const IRCClient   = require( '../irc/IRCClient' );
const log         = require( '../lib/logger' );
const BaseManager = require( './BaseManager' );
const _           = require( 'underscore' );
const irc         = require( 'irc' );

/**
 * SIP Manager
 * @extends BaseManager
 */
class IRCManager  extends BaseManager {

    /**
     * @inheritdoc
     */
    constructor( switcherController ) {
        super( switcherController );
    }

    /**
     * @inheritdoc
     */
    initialize() {
        super.initialize();
        this.ircClient = null;
    };

    createClient ( username ) {
        log.debug( "IRCManager createClient", username );
        if ( this.ircClient == null ) {
            this.username = username;
            this.connected = false;

            this.ircClient = new irc.Client('irc.freenode.net', this.username, {
                autoConnect: false
            });

            this.ircClient.connect(3, this.onConnected.bind(this));
            this.ircClient.addListener('message', this.onMessage.bind(this));
            this.ircClient.addListener('error', this.onError.bind(this));
            this.ircClient.addListener('join', this.onJoin.bind(this));
            this.ircClient.addListener('names', this.onNames.bind(this));
            this.ircClient.addListener('registered', this.onRegistered.bind(this));
            this.ircClient.addListener('quit', this.onClose.bind(this));

            this.defaultName = '#scenic_group_';
        }

        return true;
    };

    onError ( e ) {
        log.debug(this.username + " error : ", e);
    };

    onRegistered ( message ) {
        this.switcherController.clients.forEach( client => {
            if (this.config.host == client.config.host) {
                client.socket.emit('irc.connected', message );
            }
        });
        this.connected = true;
    };

    onConnected ( serverReply ) {
        this.connected = true;
    };

    onClose ( nick, reason, channels, message ) {
        log.debug( "Connection Closed " +  this.username + ' ', reason );
    };

    onJoin ( channel, nick, message ) {
        log.debug(nick + ' user joined', channel);
        this.switcherController.clients.forEach( client => {
            if (this.config.host == client.config.host) {
                client.socket.emit('irc.channel.joined', channel, nick, message );
            }
        });
    };


    onNames ( channel, nicks  ) {
        log.debug(channel + ' chanel user names', nicks);
        this.switcherController.clients.forEach( client => {
            if (this.config.host == client.config.host) {
                client.socket.emit('irc.channel.names', channel, nicks );
            }
        });
    };

    onMessage ( nick, to, text, message ) {
        log.debug( "onMessage" + nick + ' => ' + to + ': ', message);
        this.switcherController.clients.forEach( client => {
            if (this.config.host == client.config.host) {
                client.socket.emit('irc.message', nick, to, message);
            }
        });
    };

    connectClient ( target ) {
        log.debug( target + " join ", this.username );
        if ( this.connected ) {
            this.ircClient.join( target );
        }
    };

    disconnectClient ( target ) {
        this.ircClient.part( target );
    };

    sendMessage ( target, message ) {
        log.debug( this.username + ' => ' + target + ' : ', message);
        this.ircClient.say(target, message);

    };

    createChannel ( contacts, channelName ) {
        log.debug(channelName + ' create channel', contacts)
        this.ircClient.join( channelName, event => {
            log.debug('Join create event', event)
            this.switcherController.clients.forEach( client => {
                if (this.config.host == client.config.host) {
                    client.socket.emit('irc.create.channel', channelName );
                }
            });

            _.each( contacts,  contact => {
                this.sendMessage( contact, 'JOIN_CHANNEL ' + channelName );
            })
        });
    };

    joinChannel ( channel ) {
        this.ircClient.join( channel, event => {
            log.debug('joinChannel', event)
        });
    };

    getNicknames ( channel ) {
        this.ircClient.send( 'NAMES', channel);
    };

    deleteClient () {
        if ( this.ircClient ) {
            this.ircClient.disconnect();
            this.ircClient = null;
        }
    };

    quit () {
        log.debug( this.username + ' quit ');
        this.ircClient.disconnect();
        this.ircClient = null;
    }
}


module.exports = IRCManager;
