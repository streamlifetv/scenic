"use strict";

/**
 * Server Main Bootstrap File
 * This is the entry point to launch scenic.
 *
 * @module server/server
 */

//  ┌─┐┬─┐┌─┐┌─┐┌┬┐┬┌┐┌┌─┐
//  │ ┬├┬┘├┤ ├┤  │ │││││ ┬
//  └─┘┴└─└─┘└─┘ ┴ ┴┘└┘└─┘

// Require the minimum to display greeting message as fast as possible
const config = require( './settings/config' );
const colors = require( 'colors/safe' );

// Parse command line
require( './lib/command-line' ).parse( config );

// Greeting message
console.log( colors.red.bold( "Scenic " + config.version ) + "\n" );

//  ┬ ┬┌─┐┬─┐┬┌─┬┌┐┌┌─┐  ┌┬┐┬┬─┐┌─┐┌─┐┌┬┐┌─┐┬─┐┬ ┬
//  ││││ │├┬┘├┴┐│││││ ┬   │││├┬┘├┤ │   │ │ │├┬┘└┬┘
//  └┴┘└─┘┴└─┴ ┴┴┘└┘└─┘  ─┴┘┴┴└─└─┘└─┘ ┴ └─┘┴└─ ┴

const fs = require( 'fs' );

// Create scenic home directory
if ( !fs.existsSync( config.homePath ) ) {
    try {
        fs.mkdirSync( config.homePath );
    } catch ( err ) {
        console.error( "Could not create directory: " + config.homePath + " Error: " + err.toString() );
        process.exit( 1 );
    }
}

//  ┌┐ ┌─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐
//  ├┴┐│ ││ │ │ └─┐ │ ├┬┘├─┤├─┘
//  └─┘└─┘└─┘ ┴ └─┘ ┴ ┴└─┴ ┴┴

const http     = require( 'http' );
const socketIo = require( 'socket.io' );
const locale   = require( "locale" );
const async    = require( 'async' );
const rpad     = require( 'underscore.string/rpad' );
const repeat   = require( 'underscore.string/repeat' );

const checkPort          = require( './utils/check-port' );
const mergeConfig        = require( './utils/mergeConfig' );
const log                = require( './lib/logger' );
const i18n               = require( './lib/i18n' );
const scenicIo           = require( './lib/scenic-io' );
const routes             = require( './controller/routes' );
const SwitcherController = require( './scenic/SwitcherController' );

const cookieParser = require('cookie-parser');
const bodyParser   = require('body-parser');
const session      = require('express-session');


var app;
var server;
var server_instance;
var io;
var switcher;
var passport;
var MemoryStore;
var irc;

async.series( [

    // Translations
    callback => {
        i18n.initialize( callback );
    },

    // Config file
    callback => {
        log.info( "Loading config file" );
        var exists = fs.existsSync( config.configFile );
        if ( exists ) {
            var data = fs.readFileSync( config.configFile );
            try {
                var parsedConfig = JSON.parse( data );
            } catch ( e ) {
                log.error( e );
                return callback( `Could not parse config file ${config.configFile}` );
            }
            mergeConfig( config, parsedConfig );
        } else {
            log.warn( `Config file ${config.configFile} not found, continuing without it.` );
        }

        callback();
    },

    // Express Server
    callback => {
        log.info( "Setting up server..." );

        app = require( 'express' )();
        app.use( locale( config.locale.supported ) );

        passport = require('passport');

        require('./controller/passport')(passport); // pass passport for configuration

        app.use(cookieParser()); // read cookies (needed for auth)
        app.use(bodyParser()); // get information from html forms

        // passport
        // 
        MemoryStore = require('session-memory-store')(session); // make the session auto expire ( by default half a day )

        app.use(session({ secret: 'metalabScenicSecretToken', store: new MemoryStore() })); 
        app.use(passport.initialize());
        app.use(passport.session()); 


        callback();
    },

    callback => {
        server = http.createServer( app );
        checkPort( 'Scenic GUI', config.scenic, callback );
    },

    // Socket.io
    callback => {
        log.info( "Setting up socket.io..." );
        io = socketIo( server, {
            pingInterval: 25000,
            pingTimeout: 60000
        } );
        callback();
    },

    // Switcher
    callback => {
        log.info( "Initializing Switcher..." );
        switcher = new SwitcherController( config, io );
        switcher.initialize( callback );
    },

    // ScenicIo Client
    callback => {
        log.info( "Initializing IO..." );
        scenicIo.initialize( config, io, switcher);
        callback();
    },

    // Express Server startup
    callback => {
        log.info( "Starting server..." );
        server_instance = server.listen( config.scenic.port, callback );
    },

    // Express Routes
    callback => {
        log.info( "Setting up routes..." );
        routes( app, passport, config, __dirname );
        callback();
    }

], err => {
    if ( err ) {
        log.error( err );
        return process.exit( 1 );
    }

    let message = "\nConfiguration\n";
    message += colors.gray( repeat( '–', 50 ) ) + '\n';
    message += colors.yellow( rpad( " Home path", 25 ) ) + config.homePath + "\n";
    message += colors.yellow( rpad( " Scenic GUI", 25 ) ) + "http://" + config.host + ":" + config.scenic.port + "\n";
    message += colors.yellow( rpad( " SOAP port", 25 ) ) + config.soap.port + "\n";
    message += colors.yellow( rpad( " RTP session name", 25 ) ) + config.rtp.quiddName + "\n";
    message += colors.yellow( rpad( " Identification", 25 ) ) + config.nameComputer + "\n";
    message += colors.yellow( rpad( " Log level", 25 ) ) + config.logLevel + "\n";
    message += "\nSIP Information\n";
    message += colors.gray( repeat( '–', 50 ) ) + '\n';
    message += colors.yellow( rpad( " Address", 25 ) ) + config.sip.server + "\n";
    message += colors.yellow( rpad( " Port", 25 ) ) + config.sip.port + "\n";
    message += colors.yellow( rpad( " Username", 25 ) ) + config.sip.user + "\n";
    message += colors.gray( repeat( '–', 50 ) ) + '\n';
    message += "\n";
    if ( config.standalone ) {
        message += 'Launching in standalone mode\n';
    } else {
        message += 'Launching in GUI mode\n';
    }
    console.log( message );

    // GUI, unless -n is used on the command line, it will launch a chrome instance
    if ( !config.standalone ) {
        log.info( "Opening default browser: http://" + config.host + ":" + config.scenic.port );
        const chrome = require( 'child_process' ).spawn( "chromium-browser", ["--app=http://" + config.host + ":" + config.scenic.port, "--touch-events"], {
            detached: true
        } );
        chrome.unref();
        chrome.stdout.on( 'data', data => log.verbose( 'chromium-browser:', data.toString().trim() ) );
        chrome.stderr.on( 'data', data => log.debug( 'chromium-browser:', 'error:', data.toString().trim() ) );
    }
} );

/**
 * close switcher when process exits
 */
process.on( 'exit', () => {
    server_instance.close();
    if ( switcher ) {
        switcher.close();
    }
} );

/**
 * Gracefully exit when interrupting process
 */
process.on( 'SIGINT', () => {
    process.exit( 0 );
} );

/**
 * Nodemon kill signal
 */
process.once('SIGUSR2', () => {
    server_instance.close();
    if ( switcher ) {
        switcher.close();
    }
    process.kill(process.pid, 'SIGUSR2');
});