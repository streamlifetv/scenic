"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/irc/send/message
 */
module.exports = {

    /**
     * Irc create command
     *
     * @param {string}  username
     */

    execute: function ( target, message ) {
        if ( _.isEmpty( target ) ) {
            return cb( i18n.t( 'Missing target parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( target ) ) {
            return cb( i18n.t( 'Invalid target (__target__)', {
                lng:  this.lang
            } ) );
        }
        if ( _.isEmpty( message ) ) {
            return cb( i18n.t( 'Missing message parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( message ) ) {
            return cb( i18n.t( 'Invalid message (__message__)', {
                lng:  this.lang
            } ) );
        }

        this.switcherController.ircManager.sendMessage( target, message );
    }
};