"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/file/import
 */
module.exports = {

    /**
     * Import file command
     *
     * @param {string} filename Name of the file to import
     * @param {string} file Content of the file to import
     * @param {Function} cb Callback
     */
    execute: function ( filename, file, cb ) {
        if ( _.isEmpty( filename ) ) {
            return cb( i18n.t( 'Missing file name parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( filename ) ) {
            return cb( i18n.t( 'Invalid file name (__file__)', {
                lng:  this.lang,
                filename: filename
            } ) );
        }

        if ( _.isEmpty( file ) ) {
            return cb( i18n.t( 'Missing file content parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( file ) ) {
            return cb( i18n.t( 'Invalid file content (__file__)', {
                lng:  this.lang,
                file: file
            } ) );
        }

        try {
            this.switcherController.importFile( filename, file );
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred while saving file __file__ (__error__)', {
                lng:   this.lang,
                file:  file,
                error: e.toString()
            } ) );
        }

        cb();
    }
};