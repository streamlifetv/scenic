"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );
const NameExistsError = require( '../../exceptions/NameExistsError' );

/**
 * @module server/net/commands/quiddity/create
 */
module.exports = {

    /**
     * Create a quiddity
     *
     * @param {string} quiddityClass - Class of the quiddity we want to create
     * @param {string} [quiddityName] - Name of the quiddity (if not specified, the name of the quiddity will be the id)
     * @param {Object} [initProperties] - Properties to initialize the quiddity with
     * @param {string} socketId - Socket Id of the caller (soon to be deprecated)
     * @param {Function} cb Callback
     */
    execute: function ( quiddityClass, quiddityName, initProperties, socketId, cb ) {
        if ( _.isEmpty( quiddityClass ) ) {
            return cb( i18n.t( 'Missing quiddity class parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( quiddityClass ) ) {
            return cb( i18n.t( 'Invalid quiddity class (__quiddityClass__)', {
                lng: this.lang,
                quiddityClass: quiddityClass
            } ) );
        }

        if ( _.isEmpty( quiddityName ) ) {
            quiddityName = null; // Just make sure we send null and not any other "empty" value
        }

        try {
            var result = this.switcherController.quiddityManager.create( quiddityClass, quiddityName, initProperties, socketId );
        } catch ( e ) {
            if ( e instanceof NameExistsError ) {
                return cb( i18n.t( 'Quiddity name "__name__" already used', {
                    lng:  this.lang,
                    name: quiddityName
                } ) );
            } else {
                return cb( i18n.t( 'An error occurred while creating quiddity of type __quiddityClass__ (__error__)', {
                    lng:           this.lang,
                    quiddityClass: quiddityClass,
                    error:         e.toString()
                } ) );
            }
        }

        if ( result == null ) {
            return cb( i18n.t( 'Could not create quiddity of type __quiddityClass__', {
                lng: this.lang,
                quiddityClass: quiddityClass
            } ) );
        }

        cb( null, result );
    }
};