"use strict";

const _ = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/quiddity/method/list
 */
module.exports = {

    /**
     * Get methods command
     *
     * @param {string} quiddityId Quiddity for which we want to retrieve the methods
     * @param {Function} cb Callback
     */
    execute: function ( quiddityId, cb ) {
        if ( _.isEmpty( quiddityId ) ) {
            return cb( i18n.t( 'Missing quiddity id parameter', {
                lng: this.lang
            }) );
        } else if ( !_.isString( quiddityId ) ) {
            return cb( i18n.t( 'Invalid quiddity id (__quiddity__)', {
                lng: this.lang,
                quiddity: quiddityId
            } ) );
        }
        try {
            var quiddities = this.switcherController.quiddityManager.getMethods( quiddityId );
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred while getting methods for quiddity __quiddity__ (__error__)', {
                lng: this.lang,
                quiddity: quiddityId,
                error:    e.toString()
            } ) );
        }
        cb( null, quiddities );
    }
};