"use strict";

/**
 * @module server/net/commands/preview/thumbnails/unsubscribe
 */
module.exports = {

    /**
     * Unsubscribe from the preview thumbnails
     */
    execute: function() {
        delete this.previewSubscriptions[this.config.timelapse.thumbnail.quiddName];
    }
};