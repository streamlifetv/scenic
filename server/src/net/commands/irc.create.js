"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/irc/create
 */
module.exports = {

    /**
     * Irc create command
     *
     * @param {string}  username
     */

    execute: function ( username ) {
        if ( _.isEmpty( username ) ) {
            return cb( i18n.t( 'Missing username parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( username ) ) {
            return cb( i18n.t( 'Invalid name (__username__)', {
                lng:  this.lang
            } ) );
        }

        this.switcherController.ircManager.createClient( username );
    }
};