"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/quiddity/property/set
 */
module.exports = {

    /**
     * Set quiddity user data
     *
     * @param {string} quiddityId Quiddity for which we want to set user data
     * @param {string} path Tree path to set
     * @param {*} value Value to set the tree branch to
     * @param {Function} cb Callback
     */
    execute: function ( quiddityId, path, value, cb ) {
        if ( _.isEmpty( quiddityId ) ) {
            return cb( i18n.t( 'Missing quiddity id parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( quiddityId ) ) {
            return cb( i18n.t( 'Invalid quiddity id (__quiddity__)', {
                lng:      this.lang,
                quiddity: quiddityId
            } ) );
        }

        if ( _.isEmpty( path ) ) {
            return cb( i18n.t( 'Missing path parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( path ) ) {
            return cb( i18n.t( 'Invalid path (__path__)', {
                lng:  this.lang,
                path: path
            } ) );
        }

        try {
            var result = this.switcherController.quiddityManager.setUserData( quiddityId, path, value );
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred while setting value of user data __path__ to __value__ on quiddity __quiddity__ (__error__)', {
                lng:      this.lang,
                quiddity: quiddityId,
                path:     path,
                value:    value,
                error:    e.toString()
            } ) );
        }

        if ( !result ) {
            return cb( i18n.t( 'Could not set value of user data __path__ to __value__ on quiddity __quiddity__', {
                lng:      this.lang,
                quiddity: quiddityId,
                path:     path,
                value:    value
            } ) );
        }

        cb();
    }
};