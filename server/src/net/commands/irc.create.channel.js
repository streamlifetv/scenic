"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/irc/create/channel
 */
module.exports = {

    /**
     * Irc create command
     *
     * @param {string}  username
     */

    execute: function ( contacts, channelName ) {
        if ( _.isEmpty( contacts ) ) {
            return cb( i18n.t( 'Missing contacts parameter', {
                lng: this.lang
            } ) );
        }
        if ( _.isEmpty( channelName ) ) {
            return cb( i18n.t( 'Missing channel name parameter', {
                lng: this.lang
            } ) );
        }
        this.switcherController.ircManager.createChannel( contacts, channelName );
    }
};