"use strict";

const _ = require('underscore');
const i18n = require('i18next');

/**
 * @module server/net/commands/quiddity/unsubscribe
 */
module.exports = {

    /**
     * Subscribe to quiddity's properties
     * 
     * @param {String} quiddityId
     * @param {Function} cb Callback
     */
    execute: function( quiddityId, cb ) {
        if ( _.isEmpty( quiddityId ) ) {
            return cb( i18n.t( 'Missing quiddity parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( quiddityId ) ) {
            return cb( i18n.t( 'Invalid quiddity (__quiddity__)', {
                lng: this.lang,
                quiddity: quiddityId
            } ) );
        }
        
        delete this.subscriptions[quiddityId];

        cb( );
    }
};