"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/quiddity/nickname/set
 */
module.exports = {

    /**
     * Set quiddity nickname
     *
     * @param {string} quiddityId Quiddity for which we want to set nickname
     * @param {*string} value Value to set tthe nickname
     * @param {Function} cb Callback
     */
    execute: function ( quiddityId, value, cb ) {
        if ( _.isEmpty( quiddityId ) ) {
            return cb( i18n.t( 'Missing quiddity id parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( quiddityId ) ) {
            return cb( i18n.t( 'Invalid quiddity id (__quiddity__)', {
                lng:      this.lang,
                quiddity: quiddityId
            } ) );
        }

        if ( _.isEmpty( value ) ) {
            return cb( i18n.t( 'Missing nickname parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( value ) ) {
            return cb( i18n.t( 'Invalid nickname (__value__)', {
                lng:  this.lang,
                value: value
            } ) );
        }

        try {
            var result = this.switcherController.quiddityManager.setNickname( quiddityId, value );
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred while setting the nickname __value__ on quiddity __quiddity__ (__error__)', {
                lng:      this.lang,
                quiddity: quiddityId,
                value:    value,
                error:    e.toString()
            } ) );
        }

        if ( !result ) {
            return cb( i18n.t( 'Could not set the nickname __value__ on quiddity __quiddity__', {
                lng:      this.lang,
                quiddity: quiddityId,
                value:    value
            } ) );
        }

        cb();
    }
};