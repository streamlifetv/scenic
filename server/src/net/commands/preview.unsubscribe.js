"use strict";

/**
 * @module server/net/commands/preview/unsubscribe
 */
module.exports = {

    /**
     * Unsubscribe from a shmdata preview
     * 
     * @param {string} shmdata
     */
    execute: function(shmdata) {
        this.switcherController.quiddityManager.disconnectPreview(shmdata);

        delete this.previewSubscriptions[this.config.timelapse.preview.quiddName];
    }
};