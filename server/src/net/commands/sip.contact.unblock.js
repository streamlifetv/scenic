"use strict";

const i18n = require( 'i18next' );

/**
 * @module server/net/commands/sip/contact/unblock
 */
module.exports = {

    /**
     * Unblock all contacts
     *
     * @param {Function} cb Callback
     */
    execute: function ( cb ) {
        try {
            var contacts = this.switcherController.sipManager.unblockAllContact();
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred while unblocking contacts (__error__)', {
                lng:   this.lang,
                error: e.toString()
            } ) );
        }
        cb( null, contacts ? contacts : [] );
    }
};