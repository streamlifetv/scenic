"use strict";

const _    = require( 'underscore' );
const i18n = require( 'i18next' );

/**
 * @module server/net/commands/quiddity/property/remove
 */
module.exports = {

    /**
     * Remove quiddity user data
     *
     * @param {string} quiddityId Quiddity for which we want to remove some user data
     * @param {string} path Tree path to remove
     * @param {Function} cb Callback
     */
    execute: function ( quiddityId, path, cb ) {
        if ( _.isEmpty( quiddityId ) ) {
            return cb( i18n.t( 'Missing quiddity id parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( quiddityId ) ) {
            return cb( i18n.t( 'Invalid quiddity id (__quiddity__)', {
                lng:      this.lang,
                quiddity: quiddityId
            } ) );
        }

        if ( _.isEmpty( path ) ) {
            return cb( i18n.t( 'Missing path parameter', {
                lng: this.lang
            } ) );
        } else if ( !_.isString( path ) ) {
            return cb( i18n.t( 'Invalid path (__path__)', {
                lng:  this.lang,
                path: path
            } ) );
        }

        try {
            var result = this.switcherController.quiddityManager.removeUserData( quiddityId, path );
        } catch ( e ) {
            return cb( i18n.t( 'An error occurred while removing user data __path__ on quiddity __quiddity__ (__error__)', {
                lng:      this.lang,
                quiddity: quiddityId,
                path:     path,
                error:    e.toString()
            } ) );
        }

        if ( !result ) {
            return cb( i18n.t( 'Could not remove user data __path__  on quiddity __quiddity__', {
                lng:      this.lang,
                quiddity: quiddityId,
                path:     path,
            } ) );
        }

        cb();
    }
};