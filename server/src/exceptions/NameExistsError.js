"use strict";

/**
 * Name Exists Error
 */
class NameExistsError extends Error {
    /**
     * Create a Name Exists Error instance
     * @param {string} [message] Custom message for the error
     */
    constructor( message, id ) {
        super( message, id );
        this.name    = 'NameExistsError';
        this.message = message || 'Name already exists';
    }
}

module.exports = NameExistsError;