"use strict";

/**
 * @module server/utils/cleanFileName
 */

/**
 * Clean file name in order to remove the risk of reading/writing
 * where not allowed on the file system
 *
 * @param {string} name - File name to clean
 * @returns {string} - Cleaned file name
 */
function cleanFileName( name ) {
    // Just clean up dots, slashes, backslashes and messy filename stuff
    return name.replace( /(\.|\/|\\|:|;)/g, '' );
}

module.exports = cleanFileName;