"use strict";

/**
 * @module server/utils/check-port
 */

const _ = require('underscore');
const portastic = require('portastic');
const log = require('../lib/logger');

/**
 * Check port for availability
 *
 * @param {string} name - Name given to the usage of this port, used to make sense in the error messages only
 * @param {{port: int, ports:{min: int, max: int}}} config - Configuration
 * @param {Function} callback
 */
module.exports = function( name, config, callback ) {
    if ( config.port ) {
        // Sanity check
        if (typeof config.port != "number" && config.port.toString().length < 4) {
            return callback( 'Invalid ' + name + ' port: ' + config.port );
        }
        // Port test
        portastic.test( config.port, function( err, data ) {
            if (err) {
                return callback(err);
            }
            if (!data) {
                return callback( name + ' port ' + config.port + ' isn\'t available' );
            }
            callback( null, config.port );
        } );
    } else {
        // Find port
        log.debug( 'Autodetecting ' + name + ' port number in the range ' + config.ports.min + '-' + config.ports.max + '...' );
        var portRequest = _.extend( _.clone(config.ports), { retrieve: 1 });
        portastic.find( portRequest, function( err, data ) {
            if (err) {
                return callback(err);
            }
            if (!data) {
                return callback( 'No available ' + name + ' port found in the range ' + config.ports.min + '-' + config.ports.max );
            }
            log.debug('    Found port ' + data);
            config.port = data;
            callback( null, config.port );
        } );
    }
};