"use strict";

/**
 * @module server/utils/mergeConfig
 */

const _ = require( 'underscore' );

/**
 * Handles the merging of allowed keys from the config
 *
 * @param {Object} config Configuration
 * @param {Object} merge Configuration to merge into config
 */
function mergeConfig( config, merge ) {

    if ( merge.privateQuiddities && _.isArray( merge.privateQuiddities ) ) {
        config.privateQuiddities = _.uniq( config.privateQuiddities.concat( merge.privateQuiddities ) );
    }

    if ( merge.disabledPages && _.isArray( merge.disabledPages ) ) {
        config.disabledPages = _.uniq( config.disabledPages.concat( merge.disabledPages ) );
    }

    if ( typeof(merge.disableRTP) != 'undefined' ) {
        config.disableRTP = merge.disableRTP;
    }
    if ( typeof(merge.disableSourcesMenu) != 'undefined' ) {
        config.disableSourcesMenu = merge.disableSourcesMenu;
    }
    if ( typeof(merge.disableDestinationsMenu) != 'undefined' ) {
        config.disableDestinationsMenu = merge.disableDestinationsMenu;
    }
    if ( typeof(merge.disableDestinationsMenu) != 'undefined' ) {
        config.disableDestinationsMenu = merge.disableDestinationsMenu;
    }
    if ( typeof(merge.customMenus) != 'undefined' ) {
        config.customMenus = merge.customMenus;
    }
    if ( typeof(merge.turnServer) != 'undefined' ) {
        config.sip.turnServer = merge.turnServer;
    }
    if ( typeof(merge.stunServer) != 'undefined' ) {
        config.sip.stunServer = merge.stunServer;
    }
    if ( typeof(merge.sameLogin) != 'undefined' ) {
        config.sip.sameLogin = merge.sameLogin;
    }
    if ( typeof(merge.propertiesPage) != 'undefined' ) {
        config.propertiesPage = merge.propertiesPage;
    }
    if ( typeof(merge.disableChat) != 'undefined' ) {
        config.disableChat = merge.disableChat;
    }
}

module.exports = mergeConfig;