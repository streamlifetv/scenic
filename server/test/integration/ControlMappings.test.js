var _          = require( 'underscore' );
var async      = require( 'async' );
var chai       = require( "chai" );
var sinon      = require( "sinon" );
var sinonChai  = require( "sinon-chai" );
var should     = chai.should();
chai.use( sinonChai );

var quiddities = require( '../fixtures/quiddities' );

describe( 'Control Mappings', function () {

    var config;
    var io;
    var switcherController;
    var checkPort;
    var fs;
    var control;

    beforeEach( function ( done ) {
        config           = _.clone( require( '../../src/settings/config' ) );
        config.soap      = _.clone( config.soap );
        config.soap.port = 27182;
        io               = {};
        io.emit          = sinon.spy();
        checkPort        = sinon.stub();
        checkPort.yields();
        fs            = {};
        fs.existsSync = sinon.stub();
        fs.existsSync.returns( true );
        var SwitcherController = require( 'inject!../../src/scenic/SwitcherController')( {
            'fs':                  fs,
            '../utils/check-port': checkPort
        } );
        switcherController     = new SwitcherController( config, io );
        control                = switcherController.controlManager;
        switcherController.initialize( done );
    } );

    afterEach( function () {
        switcherController.close();
        switcherController = null;
        config             = null;
        io                 = null;
        switcherController = null;
        checkPort          = null;
        fs                 = null;
    } );

    describe( 'Creating Quiddities', function () {

        it.skip( 'should map properties', function ( ) {
            var a1 = switcherController.quiddityManager.create( 'audiotestsrc', 'a1' );
            var a2 = switcherController.quiddityManager.create( 'audiotestsrc', 'a2' );
            var result = control.addMapping( a1.id, 'volume', a2.id, 'volume' );
            should.exist( result );
            result.should.be.true;
            var mapper = _.findWhere( switcherController.quiddityManager.getQuiddities(), { 'class': 'property-mapper' } );
            should.exist( mapper );
            should.exist( mapper.tree );
            should.exist( mapper.tree.source );
            should.exist( mapper.tree.source.quiddity );
            mapper.tree.source.quiddity.should.equal( a1.id );
            should.exist( mapper.tree.source.property );
            mapper.tree.source.property.should.equal( 'volume' );
            should.exist( mapper.tree.sink );
            should.exist( mapper.tree.sink.quiddity );
            mapper.tree.sink.quiddity.should.equal( a2.id );
            should.exist( mapper.tree.sink.property );
            mapper.tree.sink.property.should.equal( 'volume' );
        } );

        it.skip( 'should remove mapping when removing source quiddity', function ( done ) {
            var original                                  = switcherController.quiddityManager._onRemoved;
            switcherController.quiddityManager._onRemoved = function ( quiddityId ) {
                original.apply( switcherController.quiddityManager, arguments );

                if ( quiddityId == 'property-mapper0' ) {
                    var mapper = _.findWhere( switcherController.quiddityManager.getQuiddities( {} ), { 'class': 'property-mapper' } );
                    should.not.exist( mapper );
                    done();
                }
            };

            var a1 = switcherController.quiddityManager.create( 'audiotestsrc', 'a2' );
            var a2 = switcherController.quiddityManager.create( 'audiotestsrc', 'a2' );
            control.addMapping( a1.id, 'volume', a2.id, 'volume' );
            switcherController.quiddityManager.remove( a1.id );
        } );

        it.skip( 'should remove mapping when removing sink quiddity', function ( done ) {
            var originalRemoved                           = switcherController.quiddityManager._onRemoved;
            switcherController.quiddityManager._onRemoved = function ( quiddityId ) {
                originalRemoved.apply( switcherController.quiddityManager, arguments );
                if ( quiddityId == 'property-mapper0' ) {
                    var mapper = _.findWhere( switcherController.quiddityManager.getQuiddities(), { 'class': 'property-mapper' } );
                    should.not.exist( mapper );
                    done();
                }
            };

            var a1 = switcherController.quiddityManager.create( 'audiotestsrc', 'a1' );
            var a2 = switcherController.quiddityManager.create( 'audiotestsrc', 'a2' );
            control.addMapping( a1.id, 'volume', a2.id, 'volume' );
            switcherController.quiddityManager.remove( a2.id );
        } );

    } );
} );