var _          = require( "underscore" );
var async      = require( 'async' );
var chai       = require( "chai" );
var sinon      = require( "sinon" );
var sinonChai  = require( "sinon-chai" );
var should     = chai.should();
chai.use( sinonChai );

var logStub    = require( '../fixtures/log' );
var quiddities = require( '../fixtures/quiddities' );

describe( 'Quiddity Lifecycle', function () {

    var config;
    var io;
    var switcherController;
    var checkPort;
    var fs;

    beforeEach( function () {
        config           = _.clone( require( '../../src/settings/config' ) );
        config.soap      = _.clone( config.soap );
        config.soap.port = 27182;
        io               = {};
        io.emit          = sinon.spy();
        checkPort        = sinon.stub();
        checkPort.yields();
        fs                     = {};
        var SwitcherController = require( 'inject!../../src/scenic/SwitcherController')( {
            'fs':                  fs,
            '../utils/check-port': checkPort
        } );
        switcherController     = new SwitcherController( config, io );
    } );

    afterEach( function () {
        switcherController.close();
        config             = null;
        io                 = null;
        switcherController = null;
        checkPort          = null;
        fs                 = null;
    } );

    describe( 'Creating Quiddities', function () {

        it( 'should create a quiddity', function () {
            var quidd  = quiddities.quiddity();
            var result = switcherController.quiddityManager.create( quidd.class, quidd.id, 'socketId' );
            should.exist( result );
            result.should.eql( quidd );
        } );

        it( 'should create a dummy quiddity', function () {
            var result = switcherController.quiddityManager.create( 'dummy', 'dummy', 'socketId' );
            should.exist( result );
        } );

        it( 'should create a quiddity and start it', function () {
            var create_result       = switcherController.quiddityManager.create( 'audiotestsrc', 'audio', 'socketId' );
            var set_property_result = switcherController.quiddityManager.setPropertyValue( create_result.id, 'started', true );
            var val                 = switcherController.switcher.get_property_value( create_result.id, 'started' );
            should.exist( val );
            val.should.equal( true );
        } );

    } );
} );