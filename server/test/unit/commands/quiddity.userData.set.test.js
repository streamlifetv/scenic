var _         = require( 'underscore' );
var chai      = require( 'chai' );
var sinon     = require( 'sinon' );
var sinonChai = require( 'sinon-chai' );
var should    = chai.should();
var expect    = chai.expect;
chai.use( sinonChai );

var quiddities = require( '../../fixtures/quiddities' );

describe( 'Set User Data Command', function () {

    var client;
    var command;
    var cb;

    beforeEach( function () {
        command = require( '../../../src/net/commands/quiddity.userData.set' );

        client  = {
            switcherController: {
                quiddityManager: {
                    setUserData: sinon.stub()
                }
            }
        };
        command = command.execute.bind( client );
        cb      = sinon.stub();
    } );

    afterEach( function () {
        cb.should.have.been.calledOnce;
    } );

    it( 'should return nothing when successful', function () {
        client.switcherController.quiddityManager.setUserData.returns( true );
        command( 'quidd', 'path', 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.have.been.calledOnce;
        client.switcherController.quiddityManager.setUserData.should.have.been.calledWithExactly( 'quidd', 'path', 'value' );
        cb.should.have.been.calledWithExactly();
    } );

    it( 'should return an error when manager throws', function () {
        client.switcherController.quiddityManager.setUserData.throws();
        command( 'quidd', 'path', 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.have.been.calledOnce;
        client.switcherController.quiddityManager.setUserData.should.have.been.calledWithExactly( 'quidd', 'path', 'value' );
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when failing to set userData', function () {
        client.switcherController.quiddityManager.setUserData.returns( false );
        command( 'quidd', 'path', 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.have.been.calledOnce;
        client.switcherController.quiddityManager.setUserData.should.have.been.calledWithExactly( 'quidd', 'path', 'value' );
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is empty', function () {
        command( '', 'path', 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is null', function () {
        command( null, 'path', 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is a number', function () {
        command( 666, 'path', 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is not a string', function () {
        command( ['not a string'], 'path', 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when path is empty', function () {
        command( 'quidd', '', 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when path is null', function () {
        command( 'quidd', null, 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when path is a number', function () {
        command( 'quidd', 666, 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when path is not a string', function () {
        command( 'quidd', ['not a string'], 'value', cb );
        client.switcherController.quiddityManager.setUserData.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    //NOTE: We don't test for value as it can be anything, even null

} );