var _         = require( 'underscore' );
var chai      = require( 'chai' );
var sinon     = require( 'sinon' );
var sinonChai = require( 'sinon-chai' );
var should    = chai.should();
var expect    = chai.expect;
chai.use( sinonChai );

var quiddities = require( '../../fixtures/quiddities' );

describe( 'Set Nickname Command', function () {

    var client;
    var command;
    var cb;

    beforeEach( function () {
        command = require( '../../../src/net/commands/quiddity.nickname.set' );

        client  = {
            switcherController: {
                quiddityManager: {
                    setNickname: sinon.stub()
                }
            }
        };
        command = command.execute.bind( client );
        cb      = sinon.stub();
    } );

    afterEach( function () {
        cb.should.have.been.calledOnce;
    } );

    it( 'should return nothing when successful', function () {
        client.switcherController.quiddityManager.setNickname.returns( true );
        command( 'quidd', 'value', cb );
        client.switcherController.quiddityManager.setNickname.should.have.been.calledOnce;
        client.switcherController.quiddityManager.setNickname.should.have.been.calledWithExactly( 'quidd', 'value' );
        cb.should.have.been.calledWithExactly();
    } );

    it( 'should return an error when manager throws', function () {
        client.switcherController.quiddityManager.setNickname.throws();
        command( 'quidd', 'value', cb );
        client.switcherController.quiddityManager.setNickname.should.have.been.calledOnce;
        client.switcherController.quiddityManager.setNickname.should.have.been.calledWithExactly( 'quidd', 'value' );
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when failing to set nickname', function () {
        client.switcherController.quiddityManager.setNickname.returns( false );
        command( 'quidd', 'value', cb );
        client.switcherController.quiddityManager.setNickname.should.have.been.calledOnce;
        client.switcherController.quiddityManager.setNickname.should.have.been.calledWithExactly( 'quidd', 'value' );
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is empty', function () {
        command( '', 'value', cb );
        client.switcherController.quiddityManager.setNickname.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is null', function () {
        command( null, 'value', cb );
        client.switcherController.quiddityManager.setNickname.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is a number', function () {
        command( 666, 'value', cb );
        client.switcherController.quiddityManager.setNickname.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when quiddity parameter is not a string', function () {
        command( ['not a string'], 'value', cb );
        client.switcherController.quiddityManager.setNickname.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when nickname is null', function () {
        command( 'quidd', null, cb );
        client.switcherController.quiddityManager.setNickname.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

    it( 'should return an error when nickname is not a string', function () {
        command( 'quidd', ['not a string'], cb );
        client.switcherController.quiddityManager.setNickname.should.not.have.been.called;
        cb.should.have.been.calledWithMatch( '' );
    } );

} );