var _          = require( "underscore" );
var chai       = require( "chai" );
var sinon      = require( "sinon" );
var sinonChai  = require( "sinon-chai" );
var should     = chai.should();
var expect     = chai.expect;
chai.use( sinonChai );

var logStub      = require( '../../fixtures/log' );
var switcherStub = require( '../../fixtures/switcher' );
var quiddities   = require( '../../fixtures/quiddities' );

describe( 'Switcher Controller', function () {


    var switcher;
    var config;
    var io;
    var switcherController;
    var checkPort;
    var fs;

    beforeEach( function () {
        config           = _.clone( require( "../../../src/settings/config" ) );
        config.soap      = _.clone( config.soap );
        config.soap.port = 27182;
        io               = {};
        io.emit          = sinon.stub();
        checkPort        = sinon.stub();
        checkPort.yields();
        fs                     = {};
        var SwitcherController = require('inject!../../../src/scenic/SwitcherController')({
            'switcher':            switcherStub,
            'fs':                  fs,
            '../utils/check-port': checkPort,
            '../lib/logger':       logStub()
        } );
        switcherController     = new SwitcherController( config, io );
        switcher               = switcherController.switcher;
    } );

    afterEach( function () {
        switcherController.close();
        switcher           = null;
        config             = null;
        io                 = null;
        switcherController = null;
        checkPort          = null;
        fs                 = null;
    } );

    function setupForInit() {
        switcherController.quiddityManager = sinon.stub( switcherController.quiddityManager );
        switcherController.sipManager      = sinon.stub( switcherController.sipManager );

        fs.existsSync = sinon.stub();
        fs.existsSync.returns( true );
    }

    // Hey, dummy test to get started
    it( 'should exist', function () {
        should.exist( switcherController );
    } );

    describe( 'Initialization', function () {

        it( 'should have been instantiated correctly', function () {
            should.exist( switcherController.config );
            switcherController.config.should.equal( config );

            should.exist( switcherController.io );
            switcherController.io.should.equal( io );

            should.exist( switcherController.quiddityManager );
            switcherController.quiddityManager.should.be.an.instanceOf( require( '../../../src/scenic/QuiddityManager' ) );

            should.exist( switcherController.sipManager );
            switcherController.sipManager.should.be.an.instanceOf( require( '../../../src/scenic/SipManager' ) );
        } );

        it( 'should initialize correctly without soap', function ( done ) {
            setupForInit();

            switcherController.initialize( function ( error ) {
                should.not.exist( error );
                switcher.register_prop_callback.should.have.been.calledOnce;
                switcher.register_signal_callback.should.have.been.calledOnce;

                switcher.create.callCount.should.equal( 6 );
                switcher.create.should.have.been.calledWith( 'rtpsession', config.rtp.quiddName );
                switcher.create.should.have.been.calledWith( 'systemusage', config.systemUsage.quiddName );
                switcher.create.should.have.been.calledWith( 'timelapse', config.timelapse.thumbnail.quiddName );
                switcher.create.should.have.been.calledWith( 'timelapse', config.timelapse.preview.quiddName );
                switcher.create.should.have.been.calledWith( 'sip', config.sip.quiddName );
                switcher.create.should.have.been.calledWith( 'emptyquid', config.userTree.quiddName );

                switcher.set_property_value.callCount.should.equal( 17 );
                switcher.set_property_value.should.have.been.calledWith( config.systemUsage.quiddName, 'period', String( config.systemUsage.period ) );
                // We wont repeat all the timelapse config calls here...

                switcher.invoke.should.not.have.been.called;

                switcherController.quiddityManager.initialize.should.have.been.calledOnce;
                switcherController.sipManager.initialize.should.have.been.calledOnce;

                done();
            } );
        } );

        it( 'should initialize correctly with soap', function ( done ) {
            setupForInit();

            config.soap.enabled = true;

            switcherController.initialize( function ( error ) {
                should.not.exist( error );

                switcher.register_prop_callback.should.have.been.calledOnce;
                switcher.register_signal_callback.should.have.been.calledOnce;

                switcher.create.callCount.should.equal( 7 );
                switcher.create.should.have.been.calledWith( 'rtpsession', config.rtp.quiddName );
                switcher.create.should.have.been.calledWith( 'SOAPcontrolServer', config.soap.quiddName );
                switcher.create.should.have.been.calledWith( 'systemusage', config.systemUsage.quiddName );
                switcher.create.should.have.been.calledWith( 'timelapse', config.timelapse.thumbnail.quiddName );
                switcher.create.should.have.been.calledWith( 'timelapse', config.timelapse.preview.quiddName );
                switcher.create.should.have.been.calledWith( 'sip', config.sip.quiddName );
                switcher.create.should.have.been.calledWith( 'emptyquid', config.userTree.quiddName );

                switcher.set_property_value.callCount.should.equal( 17 );
                switcher.set_property_value.should.have.been.calledWith( config.systemUsage.quiddName, 'period', String( config.systemUsage.period ) );
                // We wont repeat all the timelapse config calls here...

                switcher.invoke.should.have.been.calledOnce;
                switcher.invoke.should.have.been.calledWith( config.soap.quiddName, 'set_port', [config.soap.port] );

                switcherController.quiddityManager.initialize.should.have.been.calledOnce;
                switcherController.sipManager.initialize.should.have.been.calledOnce;

                done();
            } );
        } );

        it( 'should initialize, load file and not set soap port', function ( done ) {
            setupForInit();

            var file        = 'somefile';
            config.loadFile = file;

            switcher.load_history_from_current_state.returns( true );

            switcherController.initialize( function ( error ) {
                should.not.exist( error );
                switcher.load_history_from_current_state.should.have.been.calledOnce;
                switcher.load_history_from_current_state.should.have.been.calledWith( file );
                switcher.invoke.should.not.have.been.called;
                done();
            } );
        } );

        it( 'should initialize, load file and set soap port', function ( done ) {
            setupForInit();
            config.soap.enabled = true;

            var file        = 'somefile';
            config.loadFile = file;

            switcher.load_history_from_current_state.returns( true );

            switcherController.initialize( function ( error ) {
                should.not.exist( error );
                switcher.load_history_from_current_state.should.have.been.calledOnce;
                switcher.load_history_from_current_state.should.have.been.calledWith( file );

                switcher.invoke.should.have.been.calledOnce;
                switcher.invoke.should.have.been.calledWith( config.soap.quiddName, 'set_port', [config.soap.port] );

                done();
            } );
        } );

        it( 'should initialize, fail to load file and set soap port', function ( done ) {
            setupForInit();
            config.soap.enabled = true;

            var file        = 'somefile';
            config.loadFile = file;

            switcher.load_history_from_current_state.returns( true );

            switcherController.initialize( function ( error ) {
                should.not.exist( error );
                switcher.load_history_from_current_state.should.have.been.calledOnce;
                switcher.load_history_from_current_state.should.have.been.calledWith( file );

                switcher.invoke.should.have.been.calledOnce;
                switcher.invoke.should.have.been.calledWith( config.soap.quiddName, 'set_port', [config.soap.port] );

                done();
            } );
        } );

        it( 'should initialize, fail to load file and not set soap port', function ( done ) {
            setupForInit();

            var file        = 'somefile';
            config.loadFile = file;

            switcher.load_history_from_current_state.returns( true );

            switcherController.initialize( function ( error ) {
                should.not.exist( error );
                switcher.load_history_from_current_state.should.have.been.calledOnce;
                switcher.load_history_from_current_state.should.have.been.calledWith( file );

                switcher.invoke.should.not.have.been.called;

                done();
            } );
        } );
    } );

    describe( 'Manager delegation', function () {

        it( 'should bind client to managers', function () {
            var socket                    = { on: sinon.spy() };
            switcherController.sipManager = sinon.stub( switcherController.sipManager );
        } );

        it( 'should forward property changes to managers', function () {
            switcherController.quiddityManager = sinon.stub( switcherController.quiddityManager );
            switcherController.sipManager      = sinon.stub( switcherController.sipManager );

            var quiddity = 'hola';
            var property = 'que';
            var value    = 'tal';

            switcherController._onSwitcherProperty( quiddity, property, value );

            switcherController.quiddityManager.onSwitcherProperty.should.have.been.calledOnce;
            switcherController.sipManager.onSwitcherProperty.should.have.been.calledOnce;

            switcherController.quiddityManager.onSwitcherProperty.should.have.been.calledWith( quiddity, property, value );
            switcherController.sipManager.onSwitcherProperty.should.have.been.calledWith( quiddity, property, value );
        } );

        it( 'should forward signals to managers', function () {
            switcherController.quiddityManager = sinon.stub( switcherController.quiddityManager );
            switcherController.sipManager      = sinon.stub( switcherController.sipManager );

            var quiddity = 'hola';
            var signal   = 'que';
            var value    = 'tal';

            switcherController._onSwitcherSignal( quiddity, signal, value );

            switcherController.quiddityManager.onSwitcherSignal.should.have.been.calledOnce;
            switcherController.sipManager.onSwitcherSignal.should.have.been.calledOnce;

            switcherController.quiddityManager.onSwitcherSignal.should.have.been.calledWith( quiddity, signal, value );
            switcherController.sipManager.onSwitcherSignal.should.have.been.calledWith( quiddity, signal, value );
        } );

    } );

    describe( 'Signal Events', function () {

        /* DEPRECATED it( 'should pass along system usage grafts', function () {
         var id     = 'systemUsage';
         var signal = 'on-tree-grafted';
         var val    = 'smtng';
         var ret    = 'sysinfo';

         switcherController.quiddityManager = sinon.stub( switcherController.quiddityManager );
         switcherController.rtpManager      = sinon.stub( switcherController.rtpManager );
         switcherController.sipManager      = sinon.stub( switcherController.sipManager );

         switcher.get_info.returns( ret );
         switcherController._onSwitcherSignal( id, signal, [val] );
         switcher.get_info.should.have.been.calledOnce;
         switcher.get_info.should.have.been.calledWith( id, val );
         io.emit.should.have.been.calledOnce;
         io.emit.should.have.been.calledWith( 'systemUsage', ret );
         } );*/

        /* DEPRECATED it( 'should not pass along system usage prunes', function () {
         var id     = 'systemUsage';
         var signal = 'on-tree-pruned';
         var val    = 'smtng';

         switcherController.quiddityManager = sinon.stub( switcherController.quiddityManager );
         switcherController.rtpManager      = sinon.stub( switcherController.rtpManager );
         switcherController.sipManager      = sinon.stub( switcherController.sipManager );

         switcherController._onSwitcherSignal( id, signal, [val] );
         io.emit.should.not.have.been.called;
         } );*/


    } );

    describe( 'Shutdown', function () {

        it( 'should emit a shutdown on close', function () {
            switcherController.close();
            io.emit.should.have.been.calledOnce;
            io.emit.should.have.been.calledWith( 'shutdown' );
            switcher.close.should.have.been.calledOnce;
        } );

    } );

    describe( 'File operations', function () {

        var fileName;
        var ext;
        var file;
        var files_fs;
        var files_ui;

        beforeEach( function () {
            setupForInit();

            ext             = '.json';
            fileName        = 'file';
            file            = {
                current: true,
                id:    'file',
                name:    'file',
                path:    'some/path/file.json',
                date:    new Date( '1984/10/23' )
            };
            config.savePath = 'some/path/';
            files_fs        = ['file-a.json', 'file-b.json'];
            files_ui        = [
                {
                    current: false,
                    id:      'file-a',
                    name:    'file-a',
                    path:    'some/path/file-a.json',
                    date:    new Date( '1984/10/23' )
                }, {
                    current: false,
                    id:      'file-b',
                    name:    'file-b',
                    path:    'some/path/file-b.json',
                    date:    new Date( '1984/10/23' )
                }
            ];

            fs.statSync = sinon.stub();
            fs.statSync.returns( { mtime: new Date( '1984/10/23' ) } );
        } );

        describe( 'File list', function () {

            var cb;

            beforeEach( function () {
                cb = sinon.stub();
            } );

            it( 'should get save files', function () {
                config.savePath = 'some/path/';
                fs.readdir      = sinon.stub();
                fs.readdir.yields( null, files_fs );
                switcherController.getFileList( cb );
                fs.readdir.should.have.been.calledOnce;
                fs.readdir.should.have.been.calledWith( config.savePath );
                cb.should.have.been.calledOnce;
                cb.should.have.been.calledWith( null, files_ui );
            } );

            it( 'should get an error when save files loading throws', function () {
                var error  = 'some error';
                fs.readdir = sinon.stub();
                fs.readdir.throws( new Error( error ) );
                switcherController.getFileList( cb );
                cb.should.have.been.calledOnce;
                cb.should.have.been.calledWithMatch( error );
            } );

            it( 'should get an error when save files loading fails', function () {
                var error  = 'some error';
                fs.readdir = sinon.stub();
                fs.readdir.yields( error, null );
                switcherController.getFileList( cb );
                cb.should.have.been.calledOnce;
                cb.should.have.been.calledWithMatch( error );
            } );

        } );

        describe( 'Loading save file', function () {

            it( 'should get a save file', function ( done ) {
                console.log('Loading save start')
                switcher.load_history_from_current_state.returns( true );
                switcherController.loadFile( fileName, function ( error, success ) {

                    switcher.load_history_from_current_state.should.have.been.calledOnce;
                    switcher.load_history_from_current_state.should.have.been.calledWith( config.savePath + fileName + ext );
                    should.not.exist( error );
                    should.exist( success );
                    success.should.be.true;
                    io.emit.should.have.been.calledTwice;
                    io.emit.should.have.been.calledBefore( switcher.load_history_from_current_state );
                    io.emit.should.have.been.calledWithExactly( 'file.loading', fileName );
                    io.emit.should.have.been.calledWithExactly( 'file.loaded', fileName );
                    done();
                } );
            } );

            it( 'should throw an error when loading save file throws', function ( done ) {
                var error = 'some error';
                switcher.load_history_from_current_state.throws( new Error( error ) );
                switcherController.loadFile( fileName, function ( error, success ) {
                    switcher.load_history_from_current_state.should.have.been.calledOnce;
                    switcher.load_history_from_current_state.should.have.been.calledWith( config.savePath + fileName + ext );
                    should.exist( error );
                    error.should.eql( error );
                    should.exist( success );
                    success.should.be.false;
                    io.emit.should.have.been.calledTwice;
                    io.emit.should.have.been.calledBefore( switcher.load_history_from_current_state );
                    io.emit.should.have.been.calledWithExactly( 'file.loading', fileName );
                    io.emit.should.have.been.calledWithExactly( 'file.load.error', fileName );
                    done();
                } );
            } );

            it( 'should return false when loading save file fails', function ( done ) {
                switcher.load_history_from_current_state.returns( false );
                switcherController.loadFile( fileName, function ( error, success ) {
                    should.not.exist( error );
                    should.exist( success );
                    success.should.be.false;
                    io.emit.should.have.been.calledTwice;
                    io.emit.should.have.been.calledBefore( switcher.load_history_from_current_state );
                    io.emit.should.have.been.calledWithExactly( 'file.loading', fileName );
                    io.emit.should.have.been.calledWithExactly( 'file.load.error', fileName );
                    done();
                } );
            } );

            it( 'should clean save file name from junk', function ( done ) {
                switcher.load_history_from_current_state.returns( true );
                switcherController.loadFile( '../my:dirty.file', function ( error, success ) {
                    switcher.load_history_from_current_state.should.have.been.calledOnce;
                    switcher.load_history_from_current_state.should.have.been.calledWith( config.savePath + 'mydirtyfile' + ext );
                    should.not.exist( error );
                    should.exist( success );
                    success.should.be.true;
                    io.emit.should.have.been.calledTwice;
                    io.emit.should.have.been.calledBefore( switcher.load_history_from_current_state );
                    io.emit.should.have.been.calledWithExactly( 'file.loading', 'mydirtyfile' );
                    io.emit.should.have.been.calledWithExactly( 'file.loaded', 'mydirtyfile' );
                    done();
                } );
            } );

        } );

        describe( 'Saving scenic file', function () {

            it( 'should save a save file', function () {
                switcher.save_history.returns( true );
                var result = switcherController.saveFile( fileName );
                switcher.save_history.should.have.been.calledOnce;
                switcher.save_history.should.have.been.calledWith( config.savePath + fileName + ext );
                should.exist( result );
                result.current.should.be.true;
                //result.date.should...
                result.id.should.eql(fileName);
                result.name.should.eql(fileName);
                result.path.should.eql(config.savePath + fileName + ext);
                io.emit.should.have.been.calledOnce;
                io.emit.should.have.been.calledWithExactly( 'file.saved', file );
            } );

            it( 'should get an error when saving save file throws', function () {
                var error = 'some error';
                switcher.save_history.throws( new Error( error ) );
                expect( switcherController.saveFile.bind( switcherController, fileName ) ).to.throw( error );
                switcher.save_history.should.have.been.calledOnce;
                switcher.save_history.should.have.been.calledWith( config.savePath + fileName + ext );
                io.emit.should.not.have.been.called;
            } );

            it( 'should return false when saving save file fails', function () {
                switcher.save_history.returns( false );
                var result = switcherController.saveFile( fileName + ext );
                should.not.exist( result );
                io.emit.should.not.have.been.called;
            } );

            it( 'should remove two dots from name', function () {
                file.id = 'test';
                file.name = 'test';
                file.path = 'some/path/test.json';
                switcher.save_history.returns( true );
                var result = switcherController.saveFile( '..test' );
                switcher.save_history.should.have.been.calledOnce;
                switcher.save_history.should.have.been.calledWith( config.savePath + 'test' + ext );
                should.exist( result );
                result.current.should.be.true;
                //result.date.should...
                result.id.should.eql("test");
                result.name.should.eql("test");
                result.path.should.eql(config.savePath + "test" + ext);
                io.emit.should.have.been.calledOnce;
                io.emit.should.have.been.calledWithExactly( 'file.saved', file );
            } );

            it( 'should remove slashes from name', function () {
                file.id = 'test';
                file.name = 'test';
                file.path = 'some/path/test.json';
                switcher.save_history.returns( true );
                var result = switcherController.saveFile( '/test' );
                switcher.save_history.should.have.been.calledOnce;
                switcher.save_history.should.have.been.calledWith( config.savePath + 'test' + ext );
                should.exist( result );
                result.current.should.be.true;
                //result.date.should...
                result.id.should.eql("test");
                result.name.should.eql("test");
                result.path.should.eql(config.savePath + "test" + ext);
                io.emit.should.have.been.calledOnce;
                io.emit.should.have.been.calledWithExactly( 'file.saved', file );
            } );

            it( 'should remove backslashes from name', function () {
                file.id = 'test';
                file.name = 'test';
                file.path = 'some/path/test.json';
                switcher.save_history.returns( true );
                var result = switcherController.saveFile( '\\test' );
                switcher.save_history.should.have.been.calledOnce;
                switcher.save_history.should.have.been.calledWith( config.savePath + 'test' + ext );
                should.exist( result );
                result.current.should.be.true;
                //result.date.should...
                result.id.should.eql("test");
                result.name.should.eql("test");
                result.path.should.eql(config.savePath + "test" + ext);
                io.emit.should.have.been.calledOnce;
                io.emit.should.have.been.calledWithExactly( 'file.saved', file );
            } );

            it( 'should make a really clean path name from junk from name', function () {
                file.id = 'mytestblahpouet';
                file.name = 'mytestblahpouet';
                file.path = 'some/path/mytestblahpouet.json';
                switcher.save_history.returns( true );
                var result = switcherController.saveFile( '../../my:test.blah/../pouet' );
                switcher.save_history.should.have.been.calledOnce;
                switcher.save_history.should.have.been.calledWith( config.savePath + 'mytestblahpouet' + ext );
                should.exist( result );
                result.current.should.be.true;
                //result.date.should...
                result.id.should.eql("mytestblahpouet");
                result.name.should.eql("mytestblahpouet");
                result.path.should.eql(config.savePath + "mytestblahpouet" + ext);
                io.emit.should.have.been.calledOnce;
                io.emit.should.have.been.calledWithExactly( 'file.saved', file );
            } );

        } );

        describe( 'Deleting scenic file', function () {

            var cb;

            beforeEach( function () {
                cb = sinon.stub();
            } );

            it( 'should delete a save file', function () {
                fs.unlink = sinon.stub();
                fs.unlink.yields( null );
                switcherController.deleteFile( fileName, cb );
                fs.unlink.should.have.been.calledOnce;
                fs.unlink.should.have.been.calledWith( config.savePath + fileName + ext );
                cb.should.have.been.calledOnce;
                cb.should.have.been.calledWithExactly();
                io.emit.should.have.been.calledOnce;
                io.emit.should.have.been.calledWithExactly( 'file.deleted', fileName );
            } );

            it( 'should get an error when deleting a save file throws', function () {
                var error = 'some error';
                fs.unlink = sinon.stub();
                fs.unlink.throws( new Error( error ) );
                switcherController.deleteFile( fileName, cb );
                cb.should.have.been.calledOnce;
                cb.should.have.been.calledWithMatch( error );
                io.emit.should.not.have.been.called;
            } );

            it( 'should get an error when deleting a save file fails', function () {
                var error = 'some error';
                fs.unlink = sinon.stub();
                fs.unlink.yields( error, null );
                switcherController.deleteFile( fileName, cb );
                cb.should.have.been.calledOnce;
                cb.should.have.been.calledWithMatch( error );
                io.emit.should.not.have.been.called;
            } );

            it( 'should clean dirty file names before deleting a save file', function () {
                fs.unlink = sinon.stub();
                fs.unlink.yields( null );
                switcherController.deleteFile( '../my:dirty.file', cb );
                fs.unlink.should.have.been.calledOnce;
                fs.unlink.should.have.been.calledWith( config.savePath + 'mydirtyfile' + ext );
                cb.should.have.been.calledOnce;
                cb.should.have.been.calledWithExactly();
                io.emit.should.have.been.calledOnce;
                io.emit.should.have.been.calledWithExactly( 'file.deleted', 'mydirtyfile' );
            } );

        } );

    } );

} );