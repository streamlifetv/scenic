"use strict";

var gulp             = require( 'gulp' );
var gutil            = require( "gulp-util" );
var webpack          = require( 'webpack' );
var path             = require( 'path' );
var fs               = require( 'fs' );
var nodemon          = require( 'nodemon' );
var mocha            = require( 'gulp-mocha' );
var WebpackDevServer = require( 'webpack-dev-server' );

function onBuild( done ) {
    return function ( err, stats ) {
        if ( err ) {
            throw new gutil.PluginError( "webpack", err );
        }
        gutil.log( "[webpack]", stats.toString( {
            colors: true
        } ) );
        if ( done ) {
            done();
        }
    }
}

/**
 * Front End
 */

gulp.task( 'frontend-build', function ( done ) {
    webpack( require( './webpack/webpack.config.client.production' ) ).run( onBuild( done ) );
} );

gulp.task( 'frontend-watch', function () {
    var config = require( './webpack/webpack.config.client.dev' );
    new WebpackDevServer( webpack( config ), {
        contentBase: '.webpack',
        publicPath:  config.output.publicPath,
        hot:         true,
        progress:    true,
        disableHostCheck: true,
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        stats:       {
            colors: true
        }
    } ).listen( 8080, '0.0.0.0', function ( err, result ) {
        if ( err ) {
            throw new gutil.PluginError( "webpack-dev-server", err );
        }
        gutil.log( "[webpack-dev-server]", "http://localhost:8080/" );
    } );

} );

/**
 * Back End
 */

gulp.task( 'backend-build', function ( done ) {
    webpack( require( './webpack/webpack.config.server.production' ) ).run( onBuild( done ) );
} );

gulp.task( 'backend-watch', function ( done ) {
    var firedDone = false;
    webpack( require( './webpack/webpack.config.server.dev' ) ).watch( 100, function ( err, stats ) {
        if ( !firedDone ) {
            firedDone = true;
            done();
        }
        gutil.log( "[webpack]", stats.toString( {
            colors: true
        } ) );
        nodemon.restart();
    } );
} );

/**
 * Tests
 */

gulp.task( 'tests-build', function ( done ) {
    webpack( require( './webpack/webpack.config.tests' ) ).run( onBuild( done ) );
} );

gulp.task( 'tests-watch', function ( done ) {
    var firedDone = false;
    webpack( require( './webpack/webpack.config.tests' ) ).watch( 100, function ( err, stats ) {
        if ( !firedDone ) {
            firedDone = true;
            done();
        }
        gutil.log( "[webpack]", stats.toString( {
            colors: true
        } ) );
    } );
} );

gulp.task( 'tests-run', function () {
    return gulp.src( '.webpack/tests.js', { read: false } )
               .pipe( mocha( { } ) );
} );

/**
 * Assets
 */

gulp.task( 'assets-build', function () {
    gulp.src( ['package.json'] ).pipe( gulp.dest( 'build/' ) );

    // BEFORE: Only copy locales as the rest is emitted by webpack
    //gulp.src( ['public/locales/**/*'] ).pipe( gulp.dest( 'build/public/locales' ) );

    // NOW: Copy everything event if some files are emitted by webpack as
    //      it misses a lot of files in img's srcset and favicons...
    gulp.src( ['public/**/*'] ).pipe( gulp.dest( 'build/public' ) );
} );

/**
 * Meta
 */

gulp.task( 'build', ['assets-build', 'backend-build', 'frontend-build'] );

gulp.task( 'watch', ['backend-watch', 'frontend-watch'] );

gulp.task( 'run', ['watch'], function () {
    nodemon( {
        execMap: {
            js: 'node'
        },
        script:  path.join( __dirname, '.webpack/server' ),
        exec:    "node {{filename}} -snl switcher",
        ignore:  ['*'],
        watch:   ['client/src/']
    } ).on( 'restart', function () {
        gutil.log( "[nodemon]", "Patched!" );
    } );
} );

gulp.task( 'debug', ['watch'], function () {
    nodemon( {
        execMap: {
            js: 'node'
        },
        script:  path.join( __dirname, '.webpack/server' ),
        exec:    "node --debug-brk=5858 {{filename}} -snl switcher",
        ignore:  ['*'],
        watch:   ['client/src/']
    } ).on( 'restart', function () {
        gutil.log( "[nodemon]", "Patched!" );
    } );
} );

// Watch does not seem to start if we run it as a dependency
gulp.task( 'tdd', ['tests-watch'/*, 'tests-run'*/], function () {
    gulp.watch( ['.webpack/tests.js'], ['tests-run'] );
} );

gulp.task( 'test', ['tests-build'], function () {
    return gulp.src( '.webpack/tests.js', { read: false } )
               .pipe( mocha( {} ) )
               .once( 'error', function () {
                   process.exit( 1 );
               } )
               .once( 'end', function () {
                   process.exit();
               } );
} );

/**
 * Gracefully exit when interrupting process
 * Otherwise nodemon hangs and webpack-dev-server keeps running
 */
process.on( 'SIGINT', () => {
    process.exit( 0 );
} );
