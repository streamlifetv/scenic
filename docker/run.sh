#!/usr/bin/env bash
docker run -ti \
--volume=/home/ubald/workspace/scenic:/src/scenic:rw \
--volume=/home/ubald/workspace/switcher:/src/switcher:rw \
--volume=/home/ubald/workspace/libshmdata:/src/libshmdata:rw \
metalab/scenic:build \
/bin/bash