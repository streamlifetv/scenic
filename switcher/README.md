# Switcher Addon for NodeJS
This is the native switcher addon for nodejs.
It is used locally (not published to npm) as ```file:switcher``` in scenic's ```package.json```.

## Dependencies
Building this addon requires g++ 4.9 minimum.

If you have a version less than 4.9 install it with ```apt-get```

    sudo add-apt-repository ppa:ubuntu-toolchain-r/test
    sudo apt-get update
    sudo apt-get install g++-4.9

## Building
The addon is built automatically when using it via npm. 
In order to build manually do the following:

    node-gyp configure
    node-gyp build
    
If you manually installed g++-4.9 use this command instead:

    CXX=g++-4.9 node-gyp configure
    CXX=g++-4.9 node-gyp build
    
Or for the debug binary:

    node-gyp configure
    node-gyp build --debug
    
Again, if you installed g++-4.9 manually use the following:

    CXX=g++-4.9 node-gyp configure
    CXX=g++-4.9 node-gyp build --debug
    
## Using
The release build is built/used automatically when installing via npm.
In order to use a debug build you first need to rebuild it as a debug binary from your project's root:

    npm rebuild switcher --debug
    
Note that you will need to rebuild as a release binary when switching from the debug build

    npm rebuild switcher
    
If using a manually installed g++-4.9 these commands become:

    CXX=g++-4.9 npm rebuild switcher --debug

and

    CXX=g++-4.9 npm rebuild switcher
   
