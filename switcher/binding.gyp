{
  "variables": {
    "SWITCHER_VERSION%": "0.8"
  },
  "targets": [
    {
      "target_name": "switcher",
      "defines": [
        "USR_PLUGINS=\"/usr/switcher-<(SWITCHER_VERSION)/plugins\"",
        "USR_LOCAL_PLUGINS=\"/usr/local/switcher-<(SWITCHER_VERSION)/plugins\""
      ],
      "sources": [
        "src/switcher-addon.cpp",
        "src/switcher-controller.cpp"
      ],
      "libraries": [
        "<!@(pkg-config switcher-<(SWITCHER_VERSION) --libs)"
      ],
      "include_dirs": [
        "<!@(pkg-config switcher-<(SWITCHER_VERSION) --cflags-only-I | sed s/-I//g)"
      ],
      "cflags_cc!": [
        "-fno-rtti", "-std=c++11", "-std=c++0x", "-std=gnu++11", "-std=gnu++0x"
      ],
      "cflags_cc+": [
        "-std=c++14", "-frtti"
      ],
      'xcode_settings': {
          'MACOSX_DEPLOYMENT_TARGET': '10.9',
          'OTHER_CFLAGS': [
            "-std=c++14",
            "-stdlib=libc++",
            "-frtti"
          ],
        }
    }
  ]
}
