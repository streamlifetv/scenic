"use strict";

// Start the application
var http = require('http');
var express = require('express');
var net = require('net');
var child = require('child_process');

var app;
var server;

app = require('express')();
server = http.createServer(app);
server.listen( 8080 );

app.get('/:shmdata', function(req, res) {

    var date = new Date();
    res.writeHead(200, {
        'Date':date.toUTCString(),
        'Connection':'close',
        'Cache-Control':'private',
        'Content-Type':'video/mp4',
        'Server':'CustomStreamer/0.0.1',
        "Access-Control-Allow-Origin": "*"
    });

    // TODO: Use this instead https://github.com/expressjs/cors
    //res.header("Access-Control-Allow-Origin", "*");

    var tcpServer = net.createServer(function (socket) {
        socket.on('data', function (data) {
            res.write(data);
        });
        socket.on('close', function(error) {
            res.end();
        });
    });

    tcpServer.maxConnections = 1;
    tcpServer.listen(function() {
        var gstMuxer = child.spawn(
            'gst-launch-1.0', [
                '--gst-plugin-path=/usr/lib/gstreamer-1.0/',
                'shmdatasrc', 'socket-path=/tmp/' + req.params.shmdata,
                '!', 'tcpclientsink', 'host=localhost', 'port=' + tcpServer.address().port
            ]);
        gstMuxer.stderr.on('data', onSpawnError);
        gstMuxer.on('exit', onSpawnExit);
        res.connection.on('close', function() {
            gstMuxer.kill();
        });
    });
});

function onSpawnError(data) {
    console.log(data.toString());
}

function onSpawnExit(code) {
    if (code != null) {
        console.error('GStreamer error, exit code ' + code);
    }
}

process.on('uncaughtException', function(err) {
    console.error(err);
});

/**
 * Gracefully exit when interrupting process
 */
process.on('SIGINT', function() {
    process.exit(0);
});