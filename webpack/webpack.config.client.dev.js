var webpack = require( 'webpack' );
var path    = require( 'path' );
var localIP = require( '../server/src/utils/NetworkUtils' ).getLocalIp();

var rootPath = path.dirname( __dirname );

var config = require( './webpack.config.client' );

config.debug = true;
config.output.path = path.join(rootPath, '.webpack');
config.entry.unshift( "webpack-dev-server/client?http://" + localIP + ":8080/", "webpack/hot/dev-server" );
config.output.publicPath = "http://" + localIP + ":8080" + config.output.publicPath;
config.plugins           = config.plugins.concat(
    new webpack.HotModuleReplacementPlugin()
);

module.exports = config;