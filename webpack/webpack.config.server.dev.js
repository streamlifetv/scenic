var webpack = require( 'webpack' );
var path    = require( 'path' );

var rootPath = path.dirname( __dirname );

var config = require('./webpack.config.server');

config.debug   = true;
config.output.path = path.join(rootPath, '.webpack');

module.exports = config;