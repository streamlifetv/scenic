"use strict";

/**
 * isTouch
 * @module client/lib/isTouch
 */
define( [], function () {
    return function isTouch() {
        return (('ontouchstart' in window)
                || (navigator.MaxTouchPoints > 0)
                || (navigator.msMaxTouchPoints > 0));
    }
} );