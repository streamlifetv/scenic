"use strict";

/**
 * Main
 *
 * @module client/main
 */

import 'scss/screen.scss';

import $ from 'jquery';
import 'jquery-ui';
import 'jquery-ui/ui/widgets/mouse'; // Required by touch-punch
import 'jquery-ui-touch-punch';
import 'backbone.marionette.keyshortcuts';
import i18n from 'i18n';
import async from 'async';
import _ from 'underscore';
import  'Backbone.Mutators';
import Marionette from 'marionette';
import Mousetrap from 'mousetrap';
import Sessions from 'model/Sessions';
import Session from 'model/Session';
import Authentication from 'model/Authentication';
import AuthenticationView from 'view/AuthenticationView';
import ApplicationView from 'view/ApplicationView';

export default class Scenic {

    static main() {
        // Override mousetrap stopCallback to prevent jquery ui-widget from interfering
        var _stopCallback                = Mousetrap.prototype.stopCallback;
        Mousetrap.prototype.stopCallback = function ( e, element, combo ) {
            var stopped = _stopCallback( e, element, combo );
            if ( stopped ) {
                return true;
            }

            // Stop mousetrap on jquery ui-widget
            if ( (' ' + element.className + ' ').indexOf( ' ui-widget ' ) > -1 ) {
                return true;
            }

            return false;
        };

        let needsLogin = false;

        async.series( [

            callback => {
                // I18N INITIALIZATION
                i18n.init( {
                    lngWhitelist: ['en', 'en-US', 'en-CA', 'fr', 'fr-FR', 'fr-CA'],
                    lng:          localStorage.getItem( 'lang' ) ? localStorage.getItem( 'lang' ) : 'en',
                    ns:           'client',
                    fallbackLng:  false
                } ).done( () => {
                    // Replace Marionette's renderer with one that supports i18n
                    var render = Marionette.Renderer.render;

                    Marionette.Renderer.render = function ( template, data ) {
                        data = _.extend( data, { _t: i18n.t } );
                        return render( template, data );
                    };

                    // Replace ItemView's attachElContent to run i18n after attaching
                    Marionette.ItemView.prototype.attachElContent = function ( html ) {
                        this.$el.html( html );
                        this.$el.i18n();
                        return this;
                    };

                    // Replace CollectionView's attachElContent to run i18n after attaching
                    Marionette.CollectionView.prototype.attachElContent = function ( html ) {
                        this.$el.html( html );
                        this.$el.i18n();
                        return this;
                    };

                    // Run i18n on what's already there
                    $( document ).i18n();
                    callback();
                } );
            },

            callback => {
                // check before to launch the application view if we need to display the Authentication view
                Backbone.ajax({
                    type: 'GET',
                    contentType: 'application/json',
                    url: '/needlogin',
                    dataType: "json",
                    success: function(data, textStatus, jqXHR){
                        needsLogin = data.authenticate;
                        callback();
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        callback(errorThrown);
                    }
                });
            }

        ], error => {
            if ( error ) {
                alert( error );
                console.error( error );
            } else {
                if ( needsLogin ) {
                    const authenticationView = new AuthenticationView( {model:  new Authentication()});
                    authenticationView.render();

                } else {
                    const sessions = new Sessions();

                    // Create the default session
                    const defaultSession = new Session( { default: true } );
                    window.scenic        = defaultSession;

                    sessions.add( defaultSession ); // Default session

                    const applicationView = new ApplicationView( { sessions: sessions } );
                    applicationView.render();
                }
            }
        } );
    }

}

document.addEventListener("DOMContentLoaded", event => Scenic.main() );