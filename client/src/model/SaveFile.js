"use strict";

define( [
    'underscore',
    'backbone',
    'model/base/ScenicModel'
], function ( _, Backbone, ScenicModel ) {

    /**
     * SaveFile
     *
     * @constructor
     * @extends ScenicModel
     */

    var SaveFile = ScenicModel.extend( {
        defaults:    {
            name:       null,
            date:       null,
            current:    false
        },
        parse: function ( result ) {
            result.date    = new Date( result.date );
            return result;
        },
        methodMap:   {
            'create': function () {
                return ['file.save.as', this.get( 'name' )];
            },
            'update': function () {
                return ['file.save', this.get( 'name' )];
            },
            'patch':  function () {
                return ['file.save', this.get( 'name' )];
            },
            'delete': function () {
                return ['file.delete', this.get( 'name' )];
            },
            'read':   null
        },

        /**
         * Initialize
         */
        initialize: function () {
            ScenicModel.prototype.initialize.apply( this, arguments );
            this.scenic.sessionChannel.vent.on( 'file:renamed', this._onFileRenamed, this );
        },

        loadFile: function ( callback ) {
            this.scenic.socket.emit( 'file.load', this.get( 'name' ), function ( error ) {
                if ( callback ) {
                    callback( error );
                }
            } );
        },

        /**
         * Update the name of the file
         */
        _onFileRenamed( result ) {
            if ( result.filename == this.get( 'name' )) {
                this.set('name',  result.rename );
            }
        },

        renameFile: function ( name ) {
            this.scenic.socket.emit( 'file.rename', this.get( 'name' ), name, error => {
                if ( error ) {
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            });
        },

        getFile: function ( callback ) {
            this.scenic.socket.emit( 'file.get', this.get( 'name' ), ( error, data ) => {
                if ( error ) {
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                } else {
                    callback( data )
                }
            });
        } 

    } );
    return SaveFile;
} );