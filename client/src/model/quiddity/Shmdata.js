"use strict";

import _ from 'underscore';
import ScenicModel from '../base/ScenicModel';

/**
 * Shmdata
 *
 * @constructor
 * @extends ScenicModel
 */
const Shmdata = ScenicModel.extend( {

    defaults: {
        path:       null,
        byte_rate:  0,
        rate:       0,
        category:   null,
        type:       null,
        caps:       null
    },

    mutators: {
        name:           {
            transient: true,
            get:       function () {
                var name = this.get( 'path' ).split( '/' )[ 2 ];
                return name ? name : this.get( 'path' );
            }
        },
        shortName:      {
            transient: true,
            get:       function () {
                var name = this.get( 'path' ).split( '_' )[ 3 ];
                return name ? name : this.get( 'path' );
            }
        },
        hasThumbnail:   {
            transient: true,
            get:       function () {
                return this.get( 'category' ) == 'video';
            }
        },
        capabilityInfo: {
            transient: true,
            get:       function () {
                var caps = this.get( 'caps' );

                return caps ? caps
                    .split( ', ' )
                    .map( function ( capString ) {
                        var parts    = capString.split( '=' );
                        var property = parts[ 0 ];
                        var type     = null;
                        var value    = parts.length > 1 ? parts[ 1 ] : null;
                        if ( parts.length > 1 ) {
                            var info = /\((.*)\)(.*)/.exec( parts[ 1 ] );
                            if ( info && info.length == 3 ) {
                                type  = info[ 1 ];
                                value = info[ 2 ];
                            } else {
                                value = parts[ 1 ];
                            }
                        }
                        return { property: property, type: type, value: value };
                    } )
                    : null;
            }
        },
        capabilities:   {
            transient: true,
            get:       function () {
                var caps    = {};
                var capInfo = this.get( 'capabilityInfo' );
                if ( capInfo ) {
                    capInfo.forEach( function ( cap ) {
                        caps[ cap.property ] = cap.value;
                    } );
                }
                return caps;
            }
        }
    },

    /**
     *  Initialize
     */

    initialize: function () {
        ScenicModel.prototype.initialize.apply( this, arguments );

        // Only bind to socket if we aren't new
        // We don't want temporary models staying referenced by socket.io
        if ( !this.isNew() ) {
            this.onSocket( 'shmdata.update.stat', _.bind( this._onRateUpdated, this ) );
            this.onSocket( 'shmdata.remove', _.bind( this._onRemoved, this ) );
            this.onSocket( 'shmdata.thumbnail', _.bind( this._onThumbnail, this ) );
            this.onSocket( 'shmdata.preview', _.bind( this._onPreview, this ) );
        }
    },

    /**
     * Subscribe to shmdata previews
     */
    subscribeToPreview: function () {
        this.scenic.socket.emit( "preview.subscribe", this.get( 'path' ) );
    },

    /**
     * Unsubscribe from shmdata previews
     */
    unsubscribeFromPreview: function () {
        this.scenic.socket.emit( "preview.unsubscribe", this.get( 'path' ) );
    },

    /**
     * Update byte rate
     * This is only a temporary solution until a better tree management is put in place
     * @private
     */
    _onRateUpdated: function ( quiddityId, shmdataId, value ) {
        if ( quiddityId == this.collection.quiddity.id && shmdataId == this.id ) {
            this.set( 'byte_rate', value.byte_rate );
            this.set( 'rate', value.rate );
        }
    },

    /**
     * Shmdata Remove Handler
     * We delete ourselves if we are from the right quiddity and of the right type, discriminating by path if one
     * was provided, otherwise we delete all of the same type.
     *
     * @param quiddityId
     * @param shmdata
     * @private
     */
    _onRemoved: function ( quiddityId, shmdata ) {
        if ( this.collection.quiddity.id == quiddityId && shmdata.type == this.get( 'type' ) && (!shmdata.path || ( shmdata.path && shmdata.path == this.get( 'path' ) ) ) ) {
            this.trigger( 'destroy', this, this.collection );
        }
    },

    _onThumbnail: function ( shmdata, thumbnail ) {
        if ( shmdata == this.get( 'name' ) ) {
            // Clear preview blob url memory
            var previous = this.get( 'thumbnail' );

            // Create new blob
            var blob = new Blob( [ new Int8Array( thumbnail ) ], { type: 'image/jpg' } );
            this.set( 'thumbnail', URL.createObjectURL( blob ) );

            // Remove previous
            if ( previous ) {
                URL.revokeObjectURL( previous );
            }
        }
    },

    _onPreview: function ( shmdata, preview ) {
        if ( shmdata == this.get( 'name' ) ) {
            // Clear preview blob url memory
            var previous = this.get( 'preview' );

            // Create new blob
            var blob = new Blob( [ new Int8Array( preview ) ], { type: 'image/jpg' } );
            this.set( 'preview', URL.createObjectURL( blob ) );

            // Remove previous
            if ( previous ) {
                URL.revokeObjectURL( previous );
            }
        }
    }

} );

export default Shmdata;