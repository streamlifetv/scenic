"use strict";

import Backbone from "backbone";

/**
 * Categories collection for grouping the board's quiddities
 * Self-updates when quiddities and/or shmdatas are updated
 */
const Categories = Backbone.Collection.extend( {

    /**
     * Initialize
     */
    initialize: function ( models, options ) {
        Backbone.Collection.prototype.initialize.apply( this, arguments );
        this._wrapped = options.collection;
        this.listenTo( this._wrapped, "update", this.updateCategories );
        this.listenTo( this._wrapped, "reset", this.updateCategories );
        this.listenTo( this._wrapped, 'shmdata:update', this.updateCategories );

        // Initialize collection
        this.updateCategories();
    },

    updateCategories: function () {
        this.reset( _.uniq( this._wrapped.pluck( 'classDescription' ).map( function ( classDescription ) {
            if ( classDescription ) {
                return { id: classDescription.get( 'category' ) }
            } else {
                console.warn( 'no description' );
            }
        } ) ) );
    }
} );

export default Categories;