"use strict";

define( [
    'underscore',
    'backbone',
    'model/base/ScenicCollection',
    'model/SaveFile'
], function ( _, Backbone, ScenicCollection, SaveFile ) {

    /**
     * SaveFile Collection
     *
     * @constructor
     * @extends ScenicCollection
     */
    var SaveFiles = ScenicCollection.extend( {
        model:      SaveFile,
        comparator: 'name',
        methodMap:  {
            'create': null,
            'update': null,
            'patch':  null,
            'delete': null,
            'read':   'file.list'
        },

        /**
         * Initialize
         */
        initialize: function ( models, options ) {
            ScenicCollection.prototype.initialize.apply( this, arguments );
            
            // Handlers
            this.onSocket( 'file.reset', _.bind( this._onReset, this ) );
            this.onSocket( 'file.loading', _.bind( this._onLoading, this ) );
            this.onSocket( 'file.loaded', _.bind( this._onLoaded, this ) );
            this.onSocket( 'file.load.error', _.bind( this._onLoadError, this ) );
            this.onSocket( 'file.get.error', _.bind( this._onGetError, this ) );
            this.onSocket( 'file.deleted', _.bind( this._onDeleted, this ) );
            this.onSocket( 'file.renamed', _.bind( this._onRenamed, this ) );
            this.onSocket( 'file.saved', _.bind( this._onSaved, this ) );
            this.onSocket( 'file.import.error', _.bind( this._onImportError, this ) );
            this.onSocket( 'file.imported', _.bind( this._onSaved, this ) );
        },

        _onReset: function () {
            var self = this;
            // Set current file
            this.each( function ( f ) {
                f.set( 'current', false );
            } );

            // Refresh the quiddities after a reload
            this.trigger('quiddities:refresh');
            this.scenic.quiddities.fetch( {
                reset:   true,
                success: function () {
                    self.scenic.sessionChannel.vent.trigger( 'file:reset' );
                },
                error:   function ( error ) {
                    self.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            } );
        },

        _onLoading: function ( file ) {
            this.trigger('quiddities:refresh');
            this.scenic.quiddities.reset( null );
            this.scenic.sessionChannel.vent.trigger( 'file:loading', this.get( file ) );
        },

        _onLoaded: function ( file ) {
            var self = this;
            // Set current file
            this.each( function ( f ) {
                f.set( 'current', f.get( 'name' ) == file );
            } );
            // Refresh the quiddities after a reload
            this.scenic.quiddities.fetch( {
                reset:   true,
                success: function () {
                    self.scenic.sessionChannel.vent.trigger( 'file:loaded', self.get( file ) );
                },
                error:   function ( error ) {
                    self.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            } );
        },

        _onLoadError: function ( file ) {
            this.scenic.sessionChannel.vent.trigger( 'file:load:error', this.get( file ) );
        },

        _onGetError: function ( filename ) {
            this.scenic.sessionChannel.vent.trigger( 'file:get:error', filename );
        },

        _onRenamed: function ( result ) {
            if ( result.filename ) {
                var file = this.get( result.filename );

                // ass the new file locally
                this.add( {
                    id:         result.rename,
                    path:       file.path,
                    name:       result.rename,
                    date:       file.date,
                    current:    file.current
                }, { merge: true } );

                // Destroy the old file locally
                file.trigger( 'destroy', file, this );

                console.log('this ', this)
            }
            this.scenic.sessionChannel.vent.trigger( 'file:renamed', result);
        },

        _onImportError: function ( filename ) {
            this.scenic.sessionChannel.vent.trigger( 'file:import:error', filename );
        },

        /**
         * Delete Handler
         * Destroy this file and its child collections if our id matches the one being removed
         *
         * @param {string} fileName - File name
         * @private
         */
        _onDeleted: function ( fileName ) {
            var file = this.get( fileName );

            // We only have the file if it was deleted in another session
            // Otherwise it's already been removed from the collection.
            if ( file ) {
                // Destroy the file locally
                file.trigger( 'destroy', file, this );
            }

            // Broadcast first so that everyone has a change to identify
            this.scenic.sessionChannel.vent.trigger( 'file:removed', fileName );

        },

        /**
         * Saved Handler
         * Listens to file creations and add/merge new files to the collection
         *
         * @private
         * @param attributes
         */
        _onSaved: function ( attributes ) {
            // Freshly saved file is the current, reset all others before adding saved file
            this.each( function ( file ) {
                file.set( 'current', file.id == attributes.id );
            } );
            // Add saved file
            var file = this.add( attributes, { merge: true } );
            this.scenic.sessionChannel.vent.trigger( 'file:saved', file );
        },

        /**
         * Import scenic file
         *
         * @public
         * @param {string} fileName - File name 
         * @param {string} file - File content 
         */
        importFile: function( filename, file ) {

          var reader = new FileReader();

          reader.onload = ( evt ) => { 
                var contents = evt.target.result;
                this.scenic.socket.emit( 'file.import', filename, contents, ( error ) => {
                    if ( error ) {
                        this.scenic.sessionChannel.vent.trigger( 'error', error );
                    }
                });
          }

          reader.readAsText( file );
        }

    } );
    return SaveFiles;
} );
