"use strict";

import Backbone from "backbone";
import _            from 'underscore';
import UserDataWatcherModel from './base/UserDataWatcherModel';

var saveProperties = UserDataWatcherModel.extend( {

    defaults: function () {
        return {
            quiddities: []
        }
    },

    methodMap: {
        'create': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'update': function () {
            return ['quiddity.userData.set.branch', this.quiddityId, this.userDataPath, JSON.stringify(this) ]
        },
        'patch':  null,
        'delete': function () {
            return ['quiddity.userData.remove', this.quiddityId, this.userDataPath ]
        },
        'read': function () {
            return ['quiddity.userData.get', this.quiddity.id, this.userDataPath]
        },
    },

    /**
     * Initialize
     */
    initialize: function ( models, options ) {
        this.scenic         = options.scenic;
        this.userDataPath   = 'quidditiesProperties';
        this.quiddityId     = this.scenic.config.userTree.quiddName;

        UserDataWatcherModel.prototype.initialize.apply( this, arguments );

        this.listenTo( this.scenic.quiddities, "remove", this._onRemoved );
    },

    _onRemoved: function ( item ) {
        this.remove( item.id );
    },

    add: function ( idQuiddity ) {
        if ( !this.isQuiddityExist( idQuiddity ) ) {
            var quiddities = _.clone(this.get('quiddities'));
            quiddities.map( item => {
                return item.selected = false;
            })
            var item = {
                'id': idQuiddity,
                'selected': true
            }
            quiddities.push( item );
            this.save({quiddities});
        } else {
            this.select( idQuiddity );
        }
    },

    select: function ( idQuiddity ) {
         var quiddities = _.clone(this.get('quiddities'));
         quiddities.map( item  => {
            if (item.id == idQuiddity ) {
                return item.selected = true;
            } else {
                return item.selected = false;
            }
        });
        this.save({quiddities});
    },

    remove: function ( idQuiddity ) {
        if( this.isQuiddityExist( idQuiddity ) ) {
            var quiddities = _.clone(this.get('quiddities'));
            _.each( this.get('quiddities'),  ( item, index ) => {
                if ( item.id == idQuiddity ) {
                    if( item.selected ) {
                        if ( index > 0 ) {
                            quiddities[index - 1].selected = true;
                        } else if ( quiddities[1] ) {
                            quiddities[1].selected = true;
                        }
                    }
                    quiddities.splice(index, 1);
                }
            });

            this.save({quiddities}, {silent: true});
        }
    },

    isQuiddityExist: function ( idQuiddity ) {
        var result = false;

        _.each( this.get('quiddities'), quiddity  => {
            if (quiddity.id == idQuiddity ) {
                result = true;
            }
        });

        return result;
    },

    userDataChanged: function( saveProperties ) {
        if ( saveProperties ) {
            if( this.get('quiddities') !== saveProperties.quiddities ) {
                this.set('quiddities', JSON.parse(JSON.stringify( saveProperties.quiddities )));
                this.trigger( 'change:quiddities' );
            }
        }
    }

} );

export default saveProperties;