"use strict";

import _ from 'underscore';
import ObjectPath from 'object-path';
import ScenicModel from './base/ScenicModel';
import Properties from './quiddity/Properties';
import Methods from './quiddity/Methods';
import Shmdatas from './quiddity/Shmdatas';

/**
 * Quiddity
 *
 * @constructor
 * @extends ScenicModel
 */

const Quiddity = ScenicModel.extend( {

    defaults: function () {
        return {
            "id":             null,
            "name":           null,
            "initName":       null,
            "class":          null,
            "readOnly":       false,
            "tree":           {},
            "userData":       {},
            'initProperties': {},
            "actions":        { 'edit': true , 'remove': true },
            // Dynamic
            "maxReaders":     null
        }
    },

    mutators: {
        transient:        true,
        classDescription: function () {
            return this.scenic.classes.get( this.get( 'class' ) );
        }
    },

    /**
     *  Initialize
     */
    initialize: function ( attributes, options ) {
        ScenicModel.prototype.initialize.apply( this, arguments );
        // Setup child collections
        this.properties = new Properties( this.get( 'properties' ), { scenic: this.scenic, quiddity: this, parse: true } );
        this.methods    = new Methods( this.get( 'methods' ), { scenic: this.scenic, quiddity: this, parse: true } );
        this.shmdatas   = new Shmdatas( this.get( 'tree' ) ? this.get( 'tree' ).shmdata : null, { scenic: this.scenic, quiddity: this, parse: true } );

        // Bubble up sub collection changes to the quiddity collection
        // This is used for filtering/sorting the boards depending on child collections of the quiddity
        // Like not showing an occasional-writer without shmdatas for example.
        this.listenTo( this.properties, 'update', this.trigger.bind( this, 'property:update' ) );
        this.listenTo( this.methods, 'update', this.trigger.bind( this, 'methods:update' ) );
        this.listenTo( this.shmdatas, 'update', this.trigger.bind( this, 'shmdata:update' ) );

        // Only fetch the rest when we are not new
        // This prevents fetches and socket binding being done by temporary quiddities
        if ( !this.isNew() ) {
            this.properties.bindToSocket();
            this.methods.bindToSocket();
            this.shmdatas.bindToSocket();

            // Handlers
            this.onSocket( 'quiddity.removed', _.bind( this._onRemoved, this ) );
            this.onSocket( 'quiddity.tree.updated', _.bind( this._onTreeUpdated, this ) );
            this.onSocket( 'quiddity.focused', _.bind( this._onFocused, this ) );
            this.onSocket( 'quiddity.userData.updated', _.bind( this._onUserDataUpdated, this ) );
            this.onSocket( 'quiddity.nickname.updated', _.bind( this._onNicknameUpdated, this ) );
        }

        // Cache of the table connections, keeps track of calls to can sink caps in order to speed up
        // display of the table without having to call the server for every possible connection
        this._connectionCache = {};

        if( this.id ) {
            this.getNickname();
        }
    },

    /**
     * Delete Handler
     * Destroy this quiddity and its child collections if our id matches the one being removed
     * This is called when the quiddity has been destroyed remotely and we need to react to the desrtuction
     *
     * @param {string} quiddityId
     * @private
     */
    _onRemoved: function ( quiddityId ) {
        if ( this.id == quiddityId ) {
            // Destroy ourselves
            this.trigger( 'destroy', this, this.collection );
        }
    },

    _onDestroy() {
        // Broadcast first so that everyone has a change to identify
        this.scenic.sessionChannel.vent.trigger( 'quiddity:removed', this );
        ScenicModel.prototype._onDestroy.apply( this, arguments );
        // Destroy child collections
        this.properties.destroy();
        this.methods.destroy();
        this.shmdatas.destroy();
    },

    /**
     * Tree Updated Handler
     *
     * @param {string} quiddityId
     * @private
     * @param path
     * @param value
     */
    _onTreeUpdated: function ( quiddityId, path, value ) {
        if ( this.id == quiddityId ) {
            var tree = this.get( 'tree' );
            if ( value == null ) {
                ObjectPath.del( tree, path );
            } else {

                try {
                    ObjectPath.set( tree, path, value );
                } catch ( e ) {
                    // switcher remplace {} by null, for example { source : {} } become {source : null}
                    // ObjectPath return error when accessing null value>
                    var tmp = path.slice( 0, path.lastIndexOf("."));
                    if ( ObjectPath.has( tree, tmp) ) {
                        ObjectPath.set( tree, tmp, {} );
                        ObjectPath.set( tree, path, value );
                    }
                }
            }
            // Force change trigger, because from the backbone perspective, the tree didn't change
            // since it's the same object (the previous value ends up referencing the current one in backbone)
            this.trigger( 'change:tree' );
            this.trigger( 'change:tree:' + path );
        }
    },

    /**
     * Quiddity focused Handler
     *
     * @param {string} quiddityId
     * @private
     * @param path
     * @param value
     */
    _onFocused: function ( quiddityId, path, value ) {
        if ( this.id == quiddityId && value == 'true' ) {
            this.edit( null );
        }
    },

    /**
     * Quiddity nickname  updated Handler
     *
     * @param {string} quiddityId
     * @private
     * @param value
     */
    _onNicknameUpdated: function ( quiddityId, value ) {
        if ( this.id == quiddityId ) {
            this.set('name', value)
        }
    },

    /**
     * User Data Updated Handler
     *
     * @param {string} quiddityId
     * @private
     * @param path
     * @param value
     */
    _onUserDataUpdated: function ( quiddityId, path, value ) {
        if ( this.id == quiddityId ) {
            var userData = this.get( 'userData' );
            if ( value == null ) {
                ObjectPath.del( userData, path );
            } else {

                try {
                    ObjectPath.set( userData, path, value );
                } catch ( e ) {
                    // switcher remplace {} by null, for example { source : {} } become {source : null} 
                    // ObjectPath return error when accessing null value>
                    var tmp = path.slice( 0, path.lastIndexOf("."));
                    if ( ObjectPath.has( userData, tmp) ) {
                        ObjectPath.set( userData, tmp, {} );
                        ObjectPath.set( userData, path, value );
                    }
                }

            }

            // Force change trigger, because from the backbone perspective, the userData didn't change
            // since it's the same object (the previous value ends up referencing the current one in backbone)
            this.trigger( 'change:userData' );
            this.trigger( 'change:userData:' + path );
        }
    },

    /**
     *  Edit Quiddity
     */
    edit: function ( options ) {
        this.scenic.sessionChannel.commands.execute( 'quiddity:edit', this, options );
    },

    rename: function ( name ) {
        this.setNickname( name );
    },
    /**
     * Subscribe to the Quiddity's updates
     */
    subscribe: function () {
        var self = this;
        this.scenic.socket.emit( "quiddity.subscribe", this.id, function ( error ) {
            if ( error ) {
                self.scenic.sessionChannel.vent.trigger( 'error', error );
            }
        } );
    },

    /**
     * Unsubscribe from the Quiddity's updates
     */
    unsubscribe: function () {
        var self = this;
        this.scenic.socket.emit( "quiddity.unsubscribe", this.id, function ( error ) {
            if ( error ) {
                self.scenic.sessionChannel.vent.trigger( 'error', error );
            }
        } );
    },

    /**
     * Get the order of the quiddity for the view passed as a parameter.
     *
     * @param {String} view
     * @returns {Number}
     */
    getOrder: function ( view ) {
        var userData = this.get( "userData" );
        return userData.order && "undefined" != typeof userData.order[view] ? userData.order[view] : -1;
    },

    /**
     * Get user data for a specified path
     *
     * @param {String} path
     * @returns {*}
     */
    getUserData: function ( path ) {
        return ObjectPath.get( this.get( "userData" ), path );
    },

    /**
     * Set user data for a specified path
     *
     * @param {String} path
     * @param {*} value
     */
    setUserData: function ( path, value ) {
        this.scenic.socket.emit( "quiddity.userData.set", this.id, path, value,  ( error, result )  => {
            if ( error ) {
                this.scenic.sessionChannel.vent.trigger( 'error', error );
            }
        } );
    },

    /**
     * Set user data for a specified path
     *
     * @param {String} path
     * @param {JSON} value
     */
    setUserDataBranch: function ( path, value ) {
        this.scenic.socket.emit( "quiddity.userData.set.branch", this.id, path, value,  ( error, result )  => {
            if ( error ) {
                this.scenic.sessionChannel.vent.trigger( 'error', error );
            }
        } );
    },

    /**
     * Set user data for a specified path
     *
     * @param {String} path
     * @param {*} value
     */
    removeUserData: function ( path ) {
        this.scenic.socket.emit( "quiddity.userData.remove", this.id, path,  ( error, result ) => {
            if( cb ) {
               this.callbacks[ path ] = cb;
            }
            if ( error ) {
                this.scenic.sessionChannel.vent.trigger( 'error', error );
            }
        } );
    },

    /**
     * Connect a shmdata to a destination
     *
     * @param shmdata
     */
    connect: function ( shmdata ) {
        var existingConnectionCount = this.shmdatas.where( { type: 'reader' } ).length;
        var maxReaders              = this.get( 'maxReaders' );
        if ( maxReaders > existingConnectionCount || maxReaders == 0 ) {
            this.scenic.socket.emit( 'quiddity.method.invoke', this.id, 'connect', [shmdata.get( 'path' )],  ( error, result ) => {
                if ( error ) {
                    console.error( error );
                    this.scenic.sessionChannel.vent.trigger( 'error', error );
                }
            } );
        } else {
            if ( maxReaders == 1 ) {
                this.scenic.socket.emit( 'quiddity.method.invoke', this.id, 'disconnect-all', [],  ( error, result ) => {
                    if ( error ) {
                        console.error( error );
                    }
                    this.scenic.socket.emit( 'quiddity.method.invoke', this.id, 'connect', [shmdata.get( 'path' )], ( error, result ) => {
                        if ( error ) {
                            console.error( error );
                        }
                    } );
                } );
            } else {
                this.scenic.sessionChannel.vent.trigger( 'error', i18n.t( 'You have reached the maximum number of connections. The limit is __limit__', { limit: maxReaders } ) );
            }
        }
    },

    /**
     * Disconnect a shmdata from a destination
     *
     * @param shmdata
     */
    disconnect: function ( shmdata ) {
        this.scenic.socket.emit( 'quiddity.method.invoke', this.id, 'disconnect', [shmdata.get( 'path' )], ( error, data ) => {
            if ( error ) {
                console.log( error );
            }
        } );
    },

    /**

     * @param {Shmdata} source
     * @param {callback} callback
     */
    canConnect: function ( shmdata, callback ) {
        // Check if we already called the server for that particular hash and use that value if we have one
        // instead of calling the server every time

        if ( this._connectionCache[shmdata.get( 'caps' )] !== undefined ) {
            callback( this._connectionCache[shmdata.get( 'caps' )] );
        } else {
            this.scenic.socket.emit( 'quiddity.method.invoke', this.id, 'can-sink-caps', [shmdata.get( 'caps' )], ( error, canSink ) => {
                if ( error ) {
                    console.error( error );
                    this._connectionCache[shmdata.get( 'caps' )] = false;
                    callback( false );
                }
                this._connectionCache[shmdata.get( 'caps' )] = canSink;
                callback( canSink );
            } );
        }
    },

    /**
     * Check if this quiddity is connected to a shmdata
     *
     * @param {Shmdata} source
     * @return {boolean}
     */
    isConnected: function ( source ) {
        // Check if already connected
        var shmdataReaders = this.shmdatas.where( {
            type: 'reader'
        } );
        if ( !shmdataReaders || shmdataReaders.length == 0 ) {
            return false;
        }
        return _.some( shmdataReaders, function ( shm ) {
            return ( shm.get( 'path' ) == source.get( 'path' ) );
        } );
    },

    /**
     * Get nickname
     *
     * @returns {*}
     */
    getNickname: function () {
        this.scenic.socket.emit( "quiddity.nickname.get", this.id,  ( error, result )  => {
            if ( error ) {
                this.scenic.sessionChannel.vent.trigger( 'error', error );
            } else if ( result ) {
                this.set('name', result)
            }
        } );
    },

    /**
     * Set nickname
     *
     * @param {*} value
     */
    setNickname: function ( value ) {
        this.scenic.socket.emit( "quiddity.nickname.set", this.id, value,  ( error, result )  => {
            if ( error ) {
                this.scenic.sessionChannel.vent.trigger( 'error', error );
            } else {
                this.set('name', value);
            }
        } );
    },

    /**
     * Check if the quiddity matches a passed filter term
     * This was put here because it was bloating the code that does the filtering
     * and has the potential of being reused.
     *
     * @param filter
     * @returns {boolean}
     */
    matchesFilter( filter ) {
        const classCategory = filter == this.get( 'classDescription' ).get( 'category' );
        if ( classCategory ) {
            return true;
        }

        const shmdataCategory = this.shmdatas.some( shmdata => {
            const shmCategory = shmdata.get( 'category' );
            return !_.isEmpty( shmCategory ) && shmCategory.indexOf( filter ) != -1;
        } );
        if ( shmdataCategory ) {
            return true;
        }

        return false;
    }

} );

export default Quiddity;