"use strict";

import Backbone    from "backbone";
import _           from 'underscore';
import i18n        from 'i18n';
import Page        from 'model/Page';

/**
 * Properties Page
 *
 * @constructor
 * @extends module:Backbone.Model
 */

var SwitchScenes = Page.extend( {
    defaults: function () {
        return {
            id:          "switchScenes",
            name:        i18n.t( 'Connections' ),
            type:        'switchScenes',
            description: i18n.t( "Manage the activation of a scene" )
        }
    },

    /**
     * Initialize
     */
    initialize: function ( attributes, options ) {
        Page.prototype.initialize.apply( this, arguments );
    },

    switchScene: function ( idScene ) {
        if ( this.scenic.scenes.get( idScene ).get('properties').get('active') == true ) {
            this.scenic.scenes.deactivateScene( idScene );
        } else {
            this.scenic.scenes.switchScene(idScene);
        }
    }
} );

export default SwitchScenes;