"use strict";

define( [
    'underscore',
    'backbone',
    'i18n',
    'model/Page',
    'model/Authentication',
    'async'
], function (_, Backbone, i18n, Page, Authentication, async ) {

    /**
     * Settings Page
     *
     * @constructor
     * @extends Page
     */

    var Settings = Page.extend( {

        defaults: function () {
            return {
                id:          "settings",
                name:        i18n.t( 'Settings' ),
                type:        'settings',
                description: i18n.t( "Manage Scenic settings" ),
                authOn:  false,
                haveUser:  false
            }
        },

        mutators: {
            config: {
                transient: true,
                get: function() {
                    return this.scenic.config;
                }
            },
            sip: {
                transient: true,
                get: function() {
                    return this.scenic.quiddities.get(this.scenic.config.sip.quiddName);
                }
            }
        },

        /**
         * Initialize
         */
        initialize: function (attributes, options) {
            Page.prototype.initialize.apply( this, arguments );

            this.authentification = new Authentication();

            async.series( [

                callback => {
                    this.authentification.isEnabled((  error, result ) => {
                        if ( result ) {
                            this.set( 'haveUser', Boolean(result.nbUser));
                            this.set( 'authOn', result.authenticate);
                            callback();
                        } else {
                            callback( error );                             
                        }
                    });
                },

                callback => {
                    this.on( 'change:authOn', () => {
                        this.authentification.enable( this.get( 'authOn' ), ( error, result ) => {
                            console.log('enable plop ', result)
                            if ( result ) {
                                if ( result.enable ) {
                                    this.scenic.sessionChannel.vent.trigger( 'info', i18n.t( 'Authentication has been enabled' ));                           
                                } else {
                                    this.scenic.sessionChannel.vent.trigger( 'info', i18n.t( 'Authentication has been disabled' ));                             
                                }
                            } else {
                                this.scenic.sessionChannel.vent.trigger( 'error', i18n.t( 'Authentication cannot be changed' ));
                                callback(error);                      
                            }
                        });
                    });
                }

            ], error => {
                if ( error ) {
                    console.log( error );
                }
            } );
        },

        /**
         * Call the route /reset to delete the actual user
         */
        resetLogin: function (){
            this.authentification.resetLogin(( error, result ) => {
                if ( result ) {
                    this.scenic.sessionChannel.vent.trigger( 'info', i18n.t( 'Password has been deleted' ));
                    this.set( 'haveUser', false);    
                } else {
                    this.scenic.sessionChannel.vent.trigger( 'error', i18n.t( 'Password cannot be deleted' ));    
                }
            });
        },

        /**
         * Call the route /signup to create a user with the username admin
         */
        createLogin: function ( newpassword ){
            var credentials = { username: "admin", password: newpassword};

            this.authentification.createLogin( credentials, ( error, result ) => {
                if ( result ) {
                    this.set( 'haveUser', true);
                    this.scenic.sessionChannel.vent.trigger( 'info', i18n.t( 'Password has been created' ));   
                } else {
                    this.scenic.sessionChannel.vent.trigger( 'error', i18n.t( 'Password cannot be created' ));                          
                }
            });
        }

    } );

    return Settings;
} );