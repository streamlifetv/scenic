"use strict";

define( [
    'underscore',
    'backbone',
    'model/Page',
    'model/Quiddity'
], function ( _, Backbone, Page, Quiddity ) {

    /**
     * Table Page
     *
     * @constructor
     * @extends Page
     */

    var Table = Page.extend( {

        /**
         * Initialize
         */
        initialize: function ( attributes, options ) {
            Page.prototype.initialize.apply( this, arguments );
            this.set( 'filter', [] );
            this.destinationsReorderable = true;
        },

        /**
         * Filter class for access in this table
         *
         * @param {ClassDescription} classDescription
         * @param {String[]} filterTags
         * @param {boolean} [allTagsShouldMatch] Set to true to AND tags instead of the default OR
         * @param {boolean} [allowReadOnly] - Should read-only quiddities be included
         * @returns {boolean} Is the class allowed by the filter tags
         * @protected
         */
        _filterClass: function ( classDescription, filterTags, allTagsShouldMatch, allowReadOnly ) {

            // Reject classes listed as readOnly (can't be created from the UI) if caller doesn't want them
            if ( !allowReadOnly && classDescription.get( 'readOnly' ) ) {
                return false;
            }
            
            // Reject classes listed as private quiddity
            if ( _.contains( this.scenic.config.privateQuiddities, classDescription.get('class') ) ) {
                return false;
            }

            // Filter by tags
            var f    = allTagsShouldMatch ? _.every : _.some;
            var tags = classDescription.get( 'tags' );
            return f( filterTags, function ( tag ) {
                return _.contains( tags, tag );
            } );
        },

        /**
         * Filter Quiddity for access from this table
         *
         * @param {Quiddity} quiddity - Quiddity to test
         * @param {string[]} [filterTags] - List of allowed tags
         * @param {boolean} [allTagsShouldMatch] Set to true to AND tags instead of the default OR
         * @param {boolean} [filterCategory] - Use category filter
         * @param {boolean} [allowReadOnly] - Should read-only quiddities be included
         * @returns {boolean} Is the quiddity allowed by the filter tags and/or filter category
         * @protected
         */
        _filterQuiddity: function ( quiddity, filterTags, allTagsShouldMatch, filterCategory, allowReadOnly ) {
            // Protected quiddities should not be shown
            if ( quiddity.get('protected')) {
                return false;
            }

            // Reject classes listed as readOnly (can't be created from the UI) if caller doesn't want them
            if ( !allowReadOnly && quiddity.get( 'readOnly' ) ) {
                return false;
            }

            var classDescription = quiddity.get( 'classDescription' );

            // Quiddity class tags filtering
            var classIncluded    = filterTags ? this._filterClass( classDescription, filterTags, allTagsShouldMatch, allowReadOnly ) : true;
 
            var categoryIncluded = false;

            if( classIncluded && filterCategory && (this.get( 'filter' ).length > 0) ){
                categoryIncluded = _.some( this.get( 'filter' ), filter => quiddity.matchesFilter( filter ) );
            } else {
                categoryIncluded = true;
            }
            
            return classIncluded && categoryIncluded;
        },

        /**
         * Get source collection
         *
         * This is the collection from which sources are picked/filtered. This is not
         * the actual displayed sources, only the collection from where they are chosen.
         *
         * Override in concrete table classes to retrieve the actual collection
         *
         * @returns {Quiddities}
         */
        getQuidditySourceCollection: function () {
            return this.scenic.quiddities;
        },

        /**
         * Source sort comparator function
         *
         * @param {Object} source
         * @returns {*}
         */
        quidditySourceComparator: function ( source ) {
            return source.getUserData( "order.source" );
        },
        
        /**
         * Create a quiddity
         *
         * @param {Object} info
         */
        createQuiddity: function ( info ) {
            var self     = this;
            var quiddity = new Quiddity( null, { scenic: this.scenic } );
            quiddity.save( { 'class': info.type, 'name': info.name }, {
                success: function ( quiddity ) {
                    quiddity.rename(info.name);
                    if ( info.device ) {
                        //TODO: What is this I don't even
                        alert( 'What is this I don\'t even' );
                        quiddity.setProperty( 'device', info.device );
                    }
                }
            } );
        },

        /**
         * Subscribe to shmdata previews
         */
        subscribeToShmdataThumbnails: function () {
            this.scenic.socket.emit( "preview.thumbnails.subscribe" );
        },

        /**
         * Unsubscribe from shmdata previews
         */
        unsubscribeFromShmdataThumbnails: function () {
            this.scenic.socket.emit( "preview.thumbnails.unsubscribe" );
        }
    } );

    return Table;
} );
