"use strict";

define( [
    'underscore',
    'backbone',
    'model/Inspector'
], function ( _, Backbone, Inspector ) {

    /**
     *  @constructor
     *  @augments module:Backbone.Collection
     */
    var Inspectors = Backbone.Collection.extend( {
        model:        Inspector,

        initialize: function ( models, options ) {
            this.scenic = options.scenic;
        },

        /**
         * Set current inspector
         *
         * @param inspector
         */
        setCurrentInspector: function ( inspector ) {
            this.each( function ( i ) {
                i.set( 'active', i == inspector );
            } );

            this.trigger( 'change:current', inspector ) ;
        }
    } );

    return Inspectors;
} );
