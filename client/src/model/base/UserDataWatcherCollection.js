"use strict";

define( [
    'underscore',
    'backbone',
    'backbone.cocktail',
    'model/base/ScenicCollection',
    'model/mixins/UserDataWatcher'
], function ( _, Backbone, Cocktail, ScenicCollection, UserDataWatcher ) {

    var UserDataWatcherCollection = ScenicCollection.extend( {

        /**
         * @inheritdoc
         */
        modelOptions: {},

        /**
         * @override
         */
        initialize: function ( models, options ) {
            ScenicCollection.prototype.initialize.apply( this, arguments );
        },

        /**
         * Set the watched userData value as our collection when it changes.
         * This is called by the UserDataWatcher mixin.
         */
        userDataChanged: function ( value ) {
            if ( value ) {
                this.set( value, this.modelOptions );
            } else {
                this.reset();
            }
        }
    } );

    // Mixin the UserDataWatcher
    Cocktail.mixin( UserDataWatcherCollection, UserDataWatcher );

    return UserDataWatcherCollection;
} );
