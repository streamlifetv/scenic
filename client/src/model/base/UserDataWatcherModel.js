"use strict";

import _ from "underscore";
import ScenicModel from "../base/ScenicModel";


var UserDataWatcherModel = ScenicModel.extend( {

    /**
     * The quiddity to watch for
     * @abstract
     * @type {string}
     */
    quiddityId: null,

    /**
     * The userData to watch for
     * @abstract
     * @type {string}
     */
    userDataPath: null,

    methodMap: {
        'create':   null,
        'update':   null,
        'patch':    null,
        'delete':   null,
        'read':     null
    },
    /**
     * Initialize
     * @override
     */
    initialize: function(models, options) {
        this.scenic = options.scenic;

        // Setup listeners for quiddity additions/removals
        this.listenTo( this.scenic.quiddities, 'add', this._onQuiddityAdded );
        this.listenTo( this.scenic.quiddities, 'remove', this._onQuiddityRemoved );

        // Setup the sip quiddity (if it already exists)
        this.quiddity = this.scenic.quiddities.get( this.quiddityId );
        this._registerQuiddity();
        
        ScenicModel.prototype.initialize.apply( this, arguments );  
    },

    /**
     * Quiddity Added Handler
     * Checks if the added quiddity is the one we are watching for and start watching for its userData.
     *
     * @private
     * @param {Object} quiddity
     * @param {Object[]} quiddities
     * @param {Object} options
     */
    _onQuiddityAdded: function ( quiddity, quiddities, options ) {
        if ( quiddity.id == this.quiddityId ) {
            this.quiddity = quiddity;
            this._registerQuiddity();
        }
    },

    /**
     * Quiddity Removed Handler
     * Stops watching the quiddity's userData when it is removed
     *
     * @private
     * @param {Object} quiddity
     * @param {Object[]} quiddities
     * @param {Object} options
     */
    _onQuiddityRemoved: function ( quiddity, quiddities, options ) {
        if ( quiddity.id == this.quiddityId ) {
            this._unregisterQuiddity();
        }
    },

    /**
     * Registers the quiddity event handlers
     * If the userData we are watching is available, subscribe to its value change otherwise,
     * watch for userData addition/removal in order to then track changes to the userData's value.
     *
     * @private
     */
    _registerQuiddity: function () {
        if ( this.quiddity ) {
            this.listenTo( this.quiddity, "change:userData:" + this.userDataPath, this._checkUserData );
        }

        this._checkUserData();
    },

    /**
     * Unregisters the quiddity event handlers
     * Will stop watching for userData addition/removal and/or userData value change
     *
     * @private
     */
    _unregisterQuiddity: function () {
        if ( this.quiddity ) {
            this.stopListening( this.quiddity, "change:userData:" + this.userDataPath, this._checkUserData );
            this.quiddity = null;
        }
        this._checkUserData();
    },

    /**
     * Checks the userData for a change, get the watched value and call the userDataChanged method to notify
     * a mixin user of the change.
     */
    _checkUserData: function () {
        if ( this.quiddity ) {
            this.userDataChanged( this.quiddity.getUserData(this.userDataPath) );
            return;
        }
        this.userDataChanged( null );
    },

    /**
     * userData Changed Handler
     *
     * @abstract
     * @param {*} value - userData value
     */
    userDataChanged: function( value ) {
        //noop
    }
} );

export default UserDataWatcherModel;