"use strict";

define( [
    'underscore',
    'backbone'
], function ( _, Backbone ) {

    /**
     * Base Model
     * In case we need to extend Backbone.Model
     *
     * @constructor
     * @extends Backbone.Model
     * @exports client/model/base/BaseModel
     */
    var BaseModel = Backbone.Model.extend( {

        /**
         * Initialize the model
         * @override
         */
        initialize: function () {
            Backbone.Model.prototype.initialize.apply(this, arguments);
        }
    } );

    return BaseModel;
} );
