"use strict";

define( [
    'underscore',
    'backbone'
], function ( _, Backbone ) {

    /**
     * Inspector
     *
     * @constructor
     * @extends module:Backbone.Model
     */

    var Inspector = Backbone.Model.extend( {

        defaults: {
            id:           null,
            name:         null,
            description:  null,
            icon:         null,
            view:         null,
            currentOptions: null,
            lastOptions: null
        },

        /**
         * Initialize
         */
        initialize: function ( attributes, options ) {
            this.scenic = options.scenic;
        },

        /**
         * Activate a inspector
         */
        activate: function (options, saveState) {
            this.set('currentOptions', options);
            if ( saveState ) {
                this.set('lastOptions', options);
            }
            this.collection.setCurrentInspector( this );
        },

        /**
         * Get View Instance
         */
        getViewInstance: function () {
            return new (this.get( 'view' ))( { model: this, scenic: this.scenic } );
        }
    } );

    return Inspector;
} );