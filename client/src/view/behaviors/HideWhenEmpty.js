"use strict";

import Marionette from "marionette";

/**
 * Marionette Behavior to hide a CompositeView entirely when nothing was rendered
 * It requires some CSS as it is only applying a css class when empty.
 */
const HideWhenEmpty = Marionette.Behavior.extend( {
    _checkChildrenForVisibility() {
        const childrenCount = this.view.children.length;
        if ( childrenCount ) {
            this.$el.removeClass( 'empty' );
        } else {
            this.$el.addClass( 'empty' );
        }
        // We need to count at the table level,
        // By notifying it of count changes it can track if it is empty or not
        // This is far from ideal but it works until we use React ;) ...
        if ( this.view.options.onUpdate ) {
            this.view.options.onUpdate( childrenCount );
        }
    },

    onRender() {
        this._checkChildrenForVisibility();
    },

    onAddChild() {
        this._checkChildrenForVisibility();
    },

    onRemoveChild() {
        this._checkChildrenForVisibility();
    }
} );

export default HideWhenEmpty;