"use strict";

define( [
    // Lib
    'underscore',
    'backbone',
    'marionette',
    // Template
    './DisconnectedView.html'
], function ( _, Backbone, Marionette, DisconnectedTemplate ) {

    /**
     * @constructor
     * @augments module:Marionette.ItemView
     */
    var DisconnectedView = Marionette.ItemView.extend( {
        template:  DisconnectedTemplate ,
        className: 'connection-closed',
        initialize: function ( ) {}
    } );
    return DisconnectedView;
} );
