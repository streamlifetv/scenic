"use strict";

import $ from 'jquery';
import _ from 'underscore';
import i18n from 'i18n';
import Marionette from 'marionette';
import chatViewTemplate from './ChatView.html';
import MessagesView from './MessagesView';
import 'jquery-ui/ui/widgets/draggable';
import 'jquery-ui/ui/widgets/resizable';

const ChatView = Marionette.LayoutView.extend( {
    tagName:   'div',
    className: 'window-panel chat',
    template:   chatViewTemplate ,

    ui: {
        title:   '.title',
        close:   '.close',
        inputMessage: '.inputMessage'
    },

    regions: {
        messages: '.chatArea'
    },

    events: {
        'click .close'              : 'close',
        'keypress @ui.inputMessage' : 'checkForKey'
    },

    modelEvents: {
        'change:message': 'displayMessage',
        'change:nicknames': 'render'
    },

    templateHelpers: function () {
        return {
            title: this.model.get('nicknames').length > 0 ? this.model.get('nicknames').join() : this.model.get('id')
        }
    },

    initialize: function ( options ) {
        this.scenic = options.scenic;
    },

    onShow: function() {
        var self = this;

        // Draggable
        this.$el.draggable( {
            cursor:      "move",
            //handle:      ".title",
            containment: ".page",
            stack:       ".window-panel",
            opacity:     0.75
        } ).resizable({
            minWidth: 160,
            minHeight: 120,
            aspectRatio: false,
            resize: function( event, ui ) {
                self.onResize(ui);
            }
        });
        // We need absolute at the start... to align right
        this.$el.css( 'position', '' );
        this.resize( 400, 300 );
        this.$el.fadeIn( 250 );
    },

    onRender: function () {
        this.messages = new MessagesView( {model: this.model });
        this.showChildView( 'messages', this.messages );
    },


    close: function () {
        if ( this.getRegion( 'content' ) ) {
            this.getRegion( 'content' ).empty();
        }
        this.$el.fadeOut( 250 );
        this.scenic.chats.remove(this.model.id);
        this.destroy();
    },

    onResize: function(ui) {
        if ( this.currentPanel && this.currentPanel.onResize ) {
            var width  = ui.size.width;
            var height = ui.size.height - this.ui.content.position().top;
            this.currentPanel.onResize( width, height );
        }
    },

    resize: function(width, height) {
        this.$el.css('width', width);
        this.$el.css('height', height);
    },

    checkForKey: function ( event ) {
        var key = event.which || event.keyCode;
        if (key == 13) {
            event.preventDefault();
            this.sendMessage(event);
        }
    },

    sendMessage: function ( event ) {
        if( this.ui.inputMessage.val() ) {
            this.model.sendMessage( this.ui.inputMessage.val() );
            this.messages.addMessage( {'username' : 'Me', 'mess': this.ui.inputMessage.val() } );
            this.ui.inputMessage.val('');
        }
    },

    displayMessage: function ( event ) {
        this.messages.addMessage( this.model.get('message') );
    }

} );

export default ChatView;