"use strict";

import _ from "underscore";
import Marionette from "marionette";
import TooltipsTemplate from "./Tooltips.html";

export default Marionette.ItemView.extend( {
    template:  TooltipsTemplate,
    className: "tooltip",
    templateHelpers() {
        return {
            title:   this.tooltip ? this.tooltip.title : null,
            content: this.tooltip ? this.tooltip.content : null
        }
    },
    initialize( options ) {
        this.scenic = this.options.scenic;
        this.scenic.scenicChannel.vent.on( 'tooltip', this._handleToolTip, this );
    },

    onBeforeDestroy() {
        this.scenic.scenicChannel.vent.off( 'tooltip', this._handleToolTip, this );
    },

    _handleToolTip( tooltip ) {
        if ( this.fadeTimeout ) {
            clearTimeout( this.fadeTimeout );
        }

        this.tooltip = tooltip;
        this.render();
        this.$el.addClass( 'visible' );

        this.fadeTimeout = setTimeout( () => {
            this.$el.removeClass( 'visible' );
        }, 3000 );
    }

} );