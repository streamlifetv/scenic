"use strict";

define( [
    'underscore',
    'view/scenic/inspector/information/AddQuiddity',
    'view/scenic/inspector/information/CreateQuiddity',
    'view/scenic/inspector/information/EditQuiddity',
    'view/scenic/inspector/information/CreateRTP',
    'view/scenic/inspector/information/EditRTP',
    'view/scenic/inspector/information/ShmdataInfo',
    'view/scenic/inspector/sip/AddContact',
    'view/scenic/inspector/sip/EditContact'
], function ( _,
              AddQuiddityView, CreateQuiddityView, EditQuiddityView, CreateRTPView, EditRTPView, ShmdataInfoView, AddContactView, EditContactView ) {

    var InspectorMediator = function InspectorMediator( options ) {
        this.inspectors = options.inspectors;
        this.scenic     = options.scenic;

        this.scenic.sessionChannel.commands.setHandler( 'quiddity:add', _.bind( this._onQuiddityAdd, this ) );
        this.scenic.sessionChannel.commands.setHandler( 'quiddity:create', _.bind( this._onQuiddityCreate, this ) );
        this.scenic.sessionChannel.commands.setHandler( 'quiddity:edit', _.bind( this._onQuiddityEdit, this ) );

        this.scenic.sessionChannel.commands.setHandler( 'rtp:create', _.bind( this._onRTPCreate, this ) );
        this.scenic.sessionChannel.vent.on( 'rtp:created', _.bind( this._onRTPCreated, this ) );
        this.scenic.sessionChannel.commands.setHandler( 'rtp:edit', _.bind( this._onRTPEdit, this ) );

        this.scenic.sessionChannel.commands.setHandler( 'contact:add', _.bind( this._onContactAdd, this ) );
        this.scenic.sessionChannel.commands.setHandler( 'contact:edit', _.bind( this._onContactEdit, this ) );

        this.scenic.sessionChannel.commands.setHandler( 'shmdata:info', _.bind( this._onShmdataInfo, this ) );

        this.scenic.sessionChannel.vent.on( 'file:loading', _.bind( this.close, this ) );
    };

    InspectorMediator.prototype.close = function () {
        this.scenic.sessionChannel.commands.execute( 'inspector:close' );
    };

    /**
     * Add Quiddity Handler
     * Display the quiddity selection form
     * Uses a callback to confirm the creation request
     *
     * @param callback
     * @private
     */
    InspectorMediator.prototype._onQuiddityAdd = function ( callback ) {
        this.scenic.sessionChannel.commands.execute(
            'inspector:show',
            'info',
            { view: AddQuiddityView, options: { scenic: this.scenic, callback: callback } },
            false
        );
    };

    /**
     * Create Quiddity Handler
     * Display the quiddity creation form
     * Uses a callback to confirm the creation request
     *
     * @param {ClassDescription} classDescription
     * @param callback
     * @private
     */
    InspectorMediator.prototype._onQuiddityCreate = function ( classDescription, callback ) {
        this.scenic.sessionChannel.commands.execute(
            'inspector:show',
            'info',
            { view: CreateQuiddityView, options: { scenic: this.scenic, model: classDescription, callback: callback } },
            false
        );
    };

    /**
     * Edit Quiddity Handler
     * Display the quiddity edition form
     *
     * @param {Quiddity} quiddity
     * @private
     */
    InspectorMediator.prototype._onQuiddityEdit = function ( quiddity, options ) {
        options = options || {};
        this.scenic.sessionChannel.commands.execute(
            'inspector:show',
            'settings',
            { view: EditQuiddityView, options: { scenic: this.scenic, model: quiddity, advanced: options.advanced } },
            true
        );
    };

    /**
     * Create RTP Destination Handler
     * Display the RTP destination creation form
     * Uses a callback to confirm the creation request
     *
     * @param callback
     * @private
     */
    InspectorMediator.prototype._onRTPCreate = function ( callback ) {
        this.scenic.sessionChannel.commands.execute(
            'inspector:show',
            'info',
            { view: CreateRTPView, options: { scenic: this.scenic, callback: callback } },
            false
        );
    };

    /**
     * RTP Destination Created Handler
     * Closes the panel
     *
     * @private
     */
    InspectorMediator.prototype._onRTPCreated = function () {
        this.close();
    };

    /**
     * Edit RTP Handler
     * Display the rtp edition form
     *
     * @param {RTPDestination} RTP Destination
     * @private
     */
    InspectorMediator.prototype._onRTPEdit = function ( rtpDestination, callback ) {
        this.scenic.sessionChannel.commands.execute(
            'inspector:show',
            'info',
            { view: EditRTPView, options: { scenic: this.scenic, model: rtpDestination, callback: callback } },
            false
        );
    };

    /**
     * Show Information Panel for shmdata
     *
     * @param {Shmdata} shmdata
     * @private
     */
    InspectorMediator.prototype._onShmdataInfo = function ( shmdata ) {
        this.scenic.sessionChannel.commands.execute(
            'inspector:show',
            'info',
            { view: ShmdataInfoView, options: { scenic: this.scenic, model: shmdata } },
            true
        );
    };

    /**
     * Add Contact Handler
     * Display the contact add form
     *
     * @private
     */
    InspectorMediator.prototype._onContactAdd = function ( callback ) {
        var self = this;
        this.scenic.sessionChannel.commands.execute( 'inspector:show', 'sip', {
            view: AddContactView, options: {
                scenic: this.scenic, callback: function () {
                    self.close();
                    callback.apply( this, arguments );
                }
            }
        }, false );
    };

    /**
     * Edit Contact Handler
     * Display the contact edition form
     *
     * @param {Contact} contact
     * @private
     */
    InspectorMediator.prototype._onContactEdit = function ( contact, callback ) {
        var self = this;
        this.scenic.sessionChannel.commands.execute( 'inspector:show', 'sip', {
            view: EditContactView, options: {
                scenic: this.scenic, model: contact, callback: function () {
                    self.close();
                    callback.apply( this, arguments );
                }
            }
        }, false );
    };

    return InspectorMediator;

} );