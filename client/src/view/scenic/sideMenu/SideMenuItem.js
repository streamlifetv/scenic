"use strict";

import _ from 'underscore';
import Marionette from 'marionette';
import SideMenuItemTemplate from './SideMenuItem.html';

/**
 * Side Menu Item View
 *
 * @constructor
 * @extends module:Marionette.ItemView
 */
export default Marionette.ItemView.extend( {
    getTemplate: function () {
        if ( this.model.id != 'spacer' ) {
            return  SideMenuItemTemplate ;
        }
        return  _.template('') ;
    },
    tagName:     'li',
    className:   'tab',
    events:      {
        'click': 'activate'
    },

    attributes: function () {
        return {
            class: ['tab', this.model.get( 'id' ), this.model.get( 'active' ) ? 'active' : 'inactive'].join( ' ' ),
            title: this.model.get( 'description' ) || this.model.get( 'name' )
        }
    },

    modelEvents: {
        "change:active": "render"
    },

    /**
     * Initialize
     */
    initialize: function () {
        Marionette.ItemView.prototype.initialize.apply( this, arguments );
        if ( this.model.get('type') == 'inspector' ) {
            this.options.scenic.sessionChannel.vent.on( 'inspector:opened', this.updateActiveState.bind(this) );
            this.options.scenic.sessionChannel.vent.on( 'inspector:closed', this.updateActiveState.bind(this) );
        }
    },

    updateActiveState: function(id) {
        this.model.set('active', id && id == this.model.id );
    },

    /**
     * Attach handler
     */
    onRender: function () {
        // Update Dynamic Attributes
        this.$el.attr( this.attributes() );
    },

    /**
     * Activate Handler
     * Activates the model assigned to this tab
     */
    activate: function () {
        const command = this.model.get('command');
        if ( command ) {
            this.options.scenic.sessionChannel.commands.execute( command.name, command.params );
        }
    }
} );