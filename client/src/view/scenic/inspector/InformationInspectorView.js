define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/inspector/InspectorPanelView'
], function ( _, Backbone, Marionette, InspectorPanelView ) {

    /**
     *  @constructor
     *  @augments module:InspectorPanelView
     */
    var InformationInspector = InspectorPanelView.extend( {
        className: 'info',
        initialize: function (  ) {
            InspectorPanelView.prototype.initialize.apply(this, arguments);
        }
    } );
    return InformationInspector;
} );