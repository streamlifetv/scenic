"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'jquery',
    'view/scenic/inspector/sip/contacts/ContactView',
    'jquery-ui/ui/widgets/sortable'
], function ( _, Backbone, Marionette, $,ContactView ) {

    /**
     * Contact List View
     *
     * @constructor
     * @extends module:Marionette.CollectionVIew
     */
    var ContactUnauthorizedListView = Marionette.CollectionView.extend( {
        className:        'unAuthorizedList contact-list',
        id:               "unAuthorizedList",
        childView:        ContactView,
        childViewOptions: function () {
            return {
                table: this.options.table,
                collection: this.options.collection
            }
        },

        /**
         * Initialize
         */
        initialize: function () {
            this.listenTo( this.options.collection, 'change', this.render );
        },

        onShow:     function () {
            this.$el.sortable( {
                axis:                 'y',
                cursor:               "move",
                connectWith:          '.authorizedList',
                receive: ( event, ui ) => {
                    var contact = this.options.collection.findWhere( { id: ui.item.attr('id') });
                    contact.authorize( false );
                }
            } ).disableSelection();
        },
        /**
         * Contacts View Filter
         *
         * @param contact
         * @returns {boolean}
         */
        filter:     function ( contact ) {
            return !contact.get( 'whitelisted' ) && !contact.get( 'self' );
        }
    } );

    return ContactUnauthorizedListView;
} );
