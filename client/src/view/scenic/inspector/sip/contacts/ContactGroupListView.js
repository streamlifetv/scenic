"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'jquery',
    'view/scenic/inspector/sip/contacts/ContactGroupView'
], function ( _, Backbone, Marionette, $, ContactGroupView ) {
    /**
     * Contact Authorized List View
     *
     * @constructor
     * @extends module:Marionette.CollectionVIew
     */
    var ContactGroupListView = Marionette.CollectionView.extend( {
        className:        'contactGroupList contact-list',
        id:               "contactGroupList",
        childView:        ContactGroupView,

        childViewOptions: function () {
            return {
                table: this.options.table,
                collection: this.options.collection
            }
        },

        /**
         * Initialize
         */
        initialize: function () {
            this.listenTo( this.options.collection, 'change', this.render );
        },

        /**
         * Contacts View Filter
         *
         * @param contact
         * @returns {boolean}
         */
        filter:     function ( contact ) {
            return contact.get( 'whitelisted' ) && !contact.get( 'self' );
        }
    } );

    return ContactGroupListView;
} );
