"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    './ContactGroupView.html'
], function ( $, _, Backbone, Marionette, ContactGroupTemplate ) {

    /**
     * SIP Self View
     *
     * @constructor
     * @extends module:Marionette.ItemVIew
     */
    var ContactGroupView = Marionette.ItemView.extend( {
        template:   ContactGroupTemplate,

        className: 'contact-group',

        ui: {
            'name':             'contact-name',
            'status':           '.contact-status',
            'info':             '.contact-info'
        },

        events: {
            'click @ui.info':        'select',
        },

        modelEvents: {
            'change:selected': 'render'
        },

        templateHelpers: function () {
            return {
                selected: this.model.get('selected')
            }
        },

        attributes: function () {
            var selected = this.model.get( 'selected' ) ? 'selected' : ''
            return {
                class: ['contact-group ' + selected],
                id: this.model.get( 'id' )
            }
        },

        /**
         * Initialize
         */
        initialize: function () {
        },

        select: function () {
            if ( this.model.get('selected') == false ) {
                this.model.set('selected', true)
            } else {
                this.model.set('selected', false)
            }
        },

        onRender: function () {
            // Update Dynamic Attributes
            this.$el.attr( this.attributes() );
        },
    } );

    return ContactGroupView;
} )
;
