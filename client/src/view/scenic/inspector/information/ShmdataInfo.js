define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    './ShmdataInfo.html'
], function ( _, Backbone, Marionette, i18n, ShmdataInfoTemplate ) {

    /**
     * Shmdata Info Panel
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */
    var ShmdataInfo = Marionette.ItemView.extend( {
        template:   ShmdataInfoTemplate ,
        className: 'shmdata-info',

        modelEvents: {
            'change':   'render',
            'destroy': '_onShmdataRemoved'
        },

        events: {
            'keypress': 'checkForEscapeKey'
        },
        
        initialize: function ( options ) {
            this.scenic = options.scenic;
            this.title  = i18n.t( 'Shmdata info for __shmdata__', { shmdata: this.model.get( 'shortName' ) } );
        },

        checkForEscapeKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ) {
                event.preventDefault();
                this.scenic.sessionChannel.commands.execute( 'inspector:close' );
            }
        },

        _onShmdataRemoved: function ( shmdata ) {
            this.scenic.sessionChannel.commands.execute( 'inspector:close', true );
        }
    } );

    return ShmdataInfo;
} );