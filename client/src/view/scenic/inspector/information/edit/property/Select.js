"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/inspector/information/edit/property/Field',
    './Select.html',
    'jquery-ui/ui/widgets/selectmenu'
], function ( _, Backbone, Marionette, FieldView, SelectTemplate ) {

    /**
     * Select View
     *
     * @constructor
     * @extends FieldView
     */
    var SelectProperty = FieldView.extend( {
        template:  SelectTemplate ,

        ui: {
            property: '.property'
        },

        events: {
            'change .property': 'updateModel'
        },

        /**
         * Initialize
         */
        initialize: function () {
            FieldView.prototype.initialize( this, arguments );
        },

        onAttach: function () {
            var self = this;
            this.ui.property.selectmenu( {
                    disabled: !this.model.get( 'writable' ) || this.model.get( 'disabled' ),
                    width:    '100%',
                    position: {
                        my: "left top", 
                        at: "left bottom",
                        // show the menu opposite side if there
                        // is not enough available space
                        collision: "flipfit"  
                    },
                    change:   function ( event, ui ) {
                        self.model.updateValue( ui.item.value );
                    }
                } )
                .selectmenu( "menuWidget" )
                .addClass( "overflow" );
        },

        /**
         * Update
         *
         * @param event
         */
        updateModel: function ( event ) {
            this.model.updateValue( this.ui.property.val() );
        },

        /**
         * Set the value of the slider
         * @inheritdoc
         */
        onModelValueChanged: function ( model, value, options ) {
            if ( this.ui.property.val() != value ) {
                this.ui.property.val(value);
            }
        }
    } );

    return SelectProperty;
} );
