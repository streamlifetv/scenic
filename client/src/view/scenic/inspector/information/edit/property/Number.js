"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/inspector/information/edit/property/Field',
    './Number.html',
    'jquery-ui/ui/widgets/slider'
], function ( _, Backbone, Marionette, FieldView, NumberTemplate ) {

    /**
     * Number View
     *
     * @constructor
     * @extends FieldView
     */
    var NumberProperty = FieldView.extend( {
        template:  NumberTemplate ,

        ui: {
            property: '.property',
            slider:   '.slider'
        },

        events: {
            'change .property': 'updateModel'
        },

        /**
         * Initialize
         */
        initialize: function () {
            FieldView.prototype.initialize( this, arguments );
        },

        onRender: function () {
            var self    = this;
            var type    = this.model.get( 'type' );
            var value = self.model.get( "value" );
            var min = this.model.get( 'min' );
            if ( min < -9007199254740991/*Number.MIN_SAFE_INTEGER*/ ) {
                min = -9007199254740991;//Number.MIN_SAFE_INTEGER;
            }
            var max = this.model.get( 'max' );
            if ( max > 9007199254740991/*Number.MAX_SAFE_INTEGER*/ ) {
                max = 9007199254740991;//Number.MAX_SAFE_INTEGER;
            }
            var step;

            switch ( type ) {
                case 'int':
                case 'int64':
                case 'short':
                case 'long':
                case 'long long':
                case 'uint':
                case 'unsigned int':
                case 'unsigned short':
                case 'unsigned long':
                case 'unsigned long long':
                    step = 1;
                    break;
                default:
                    step = 0.01;
            }

            this.ui.slider.slider( {
                disabled: !this.model.get('writable') || this.model.get('disabled'),
                range: "min",
                value: value,
                step:  step,
                min:   min,
                max:   max,
                animate: false,
                slide: function ( event, ui ) {
                    self.model.updateValue( ui.value );
                }
            } );
        },

        /**
         * Update
         *
         * @param event
         */
        updateModel: function ( event ) {
            this.model.updateValue( parseFloat(this.ui.property.val()) );
        },

        /**
         * Set the value of the slider
         * @inheritdoc
         */
        onModelValueChanged: function ( model, value, options ) {
            this.ui.property.val( value );
            this.ui.slider.slider( 'value', value );
        }
    } );

    return NumberProperty;
} );
