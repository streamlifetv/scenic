"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/inspector/information/edit/property/Field',
    './Fraction.html',
    'jquery-ui/ui/widgets/slider'
], function ( _, Backbone, Marionette, FieldView, FractionTemplate ) {

    /**
     * Fraction View
     *
     * @constructor
     * @extends FieldView
     */
    var FractionProperty = FieldView.extend( {
        template:  FractionTemplate ,

        ui: {
            property: '.property',
            numerator: '.property.numerator-input',
            numeratorSlider:   '.numerator-slider',
            denominator: '.property.denominator-input',
            denominatorSlider:   '.denominator-slider',
            update:   '.update'
        },

        events: {
            'click @ui.update': 'updateModel',
            'keypress @ui.property': 'checkForEnterKey'
        },

        templateHelpers: function() {
            var val = this.model.get('value' ).split('/');
            return {
                numerator: val[0],
                denominator: val[1]
            }
        },

        /**
         * Initialize
         */
        initialize: function () {
            FieldView.prototype.initialize( this, arguments );
        },

        onRender: function () {
            var self = this;
            var val  = this.model.get( "value" ).split('/');

            this.ui.numeratorSlider.slider( {
                disabled: !this.model.get('writable') || this.model.get('disabled'),
                range: "min",
                value: val[0],
                step:  1,
                min:   this.model.get( 'minNumerator' ),
                max:   this.model.get( 'maxNumerator' ),
                animate: false,
                slide: function ( event, ui ) {
                    self.model.updateValue( ui.value + '/' + self.model.get( "value" ).split('/')[1] );
                }
            } );

            this.ui.denominatorSlider.slider( {
                disabled: !this.model.get('writable') || this.model.get('disabled'),
                range: "min",
                value: val[1],
                step:  1,
                min:   this.model.get( 'minDenominator' ),
                max:   this.model.get( 'maxDenominator' ),
                animate: false,
                slide: function ( event, ui ) {
                    self.model.updateValue( self.model.get( "value" ).split('/')[0] + '/' + ui.value );
                }
            } );
        },

        /**
         * Update
         *
         * @param event
         */
        updateModel: function ( event ) {
            this.model.updateValue( this.ui.numerator.val() + '/' + this.ui.denominator.val() );
        },

        /**
         * Set the value of the slider
         * @inheritdoc
         */
        onModelValueChanged: function( model, value, options ) {
            var val = model.get('value' ).split('/');
            this.ui.numerator.val( val[0] );
            this.ui.numeratorSlider.slider( 'value', val[0] );
            this.ui.denominator.val( val[1] );
            this.ui.denominatorSlider.slider( 'value', val[1] );
        },

        checkForEnterKey: function() {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.updateModel();
            }
        }
    } );

    return FractionProperty;
} );
