"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/inspector/information/edit/property/Field',
    './Bool.html'
], function ( _, Backbone, Marionette, FieldView, BoolTemplate ) {

    /**
     * Bool View
     *
     * @constructor
     * @extends FieldView
     */
    var BoolProperty = FieldView.extend( {
        template:  BoolTemplate ,

        ui: {
            property: '.property'
        },

        events: {
            'change .property': 'updateModel'
        },

        /**
         * Initialize
         */
        initialize: function () {
            FieldView.prototype.initialize( this, arguments );
        },

        /**
         * Update Model Value
         *
         * @param event
         */
        updateModel: function ( event ) {
            // We have a custom checkbox, this is needed to toggle and set the actual value
            var checked = this.ui.property.is( ':checked' );
            this._updateChecked( checked );
            // Update the model
            this.model.updateValue( this.ui.property.val() == 'true' );
        },

        /**
         * Set the value of the checkbox
         * @inheritdoc
         */
        onModelValueChanged: function ( model, value, options ) {
            this._updateChecked( value );
        },

        _updateChecked( checked ) {
            this.ui.property.val( checked ).attr( 'checked', checked );
            this.ui.property.addClass( checked ? 'checked' : 'unchecked' ).removeClass( checked ? 'unchecked' : 'checked' );
        }
    } );

    return BoolProperty;
} );
