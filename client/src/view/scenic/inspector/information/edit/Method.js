"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'view/scenic/inspector/information/edit/method/ArgumentView',
    './Method.html'
], function ( _, Backbone, Marionette, i18n, ArgumentView, MethodTemplate ) {

    /**
     * Method View
     *
     * @constructor
     * @extends module:Marionette.CompositeView
     */
    var Method = Marionette.CompositeView.extend( {
        template:  MethodTemplate ,
        tagName: 'li',
        className: 'method',

        childView: ArgumentView,
        childViewContainer: '.arguments-container',

        ui: {
            invoke: '.invoke'
        },

        events: {
            'click @ui.invoke': 'invoke'
        },

        modelEvents: {
            'change': 'renderOnChange'
        },

        templateHelpers: function () {
            return {
                name:  i18n.t( this.model.get( 'name' ))
            };
        },
        /**
         * Initialize
         */
        initialize: function( ) {
            this.collection = this.model.args;
        },

        renderOnChange: function() {
            this.render();
        },

        invoke: function() {
            this.model.invoke();
        }
    } );

    return Method;
} );
