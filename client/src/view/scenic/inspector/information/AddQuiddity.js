define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    './AddQuiddity.html',
    'jquery-ui/ui/widgets/autocomplete'
], function ( _, Backbone, Marionette, i18n, AddQuiddityTemplate ) {

    /**
     * Add Quiddity Form
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */
    var AddQuiddity = Marionette.ItemView.extend( {
        template:   AddQuiddityTemplate ,
        className: 'add-quiddity dialog',
        ui:        {
            'type':   '#quiddityType',
            'name':   '#quiddityName',
            'create': '#create',
            'cancel': '#cancel'
        },
        events:    {
            'click @ui.create':  'add',
            'click @ui.cancel':  'cancel',
            'keydown':           'checkForEscapeKey',
            'keypress @ui.type': 'checkForEnterKey',
            'keypress @ui.name': 'checkForEnterKey'
        },

        initialize: function ( options ) {
            this.scenic   = options.scenic;
            this.title    = i18n.t( 'Add a quiddity' );
            this.callback = options.callback;
        },

        _findQuiddity( term, full ) {
            const lcTerm = term.toLowerCase();
            let results  = [];
            var offset =0;
            this.scenic.classes.each( model => {
                var classPos = model.get( 'class' ).toLowerCase().indexOf( lcTerm );
                var namePos  = full ? model.get( 'name' ).toLowerCase().indexOf( lcTerm ) : -1;
                var descPos  = full ? model.get( 'description' ).toLowerCase().indexOf( lcTerm ) : -1;


                if ( classPos == 0 || namePos == 0 || descPos == 0 ) {
                    var result = {
                        label: model.get('name') + " (" + model.get("class") + ")",
                        value: model.get('class')
                    };
                    if ( lcTerm.length == model.get( 'class' ).length ) {
                        results.unshift(result);
                        offset++;
                    } else{
                        results.splice( offset, 0, result );
                    }
                } else if ( full && ( classPos > 0 || namePos > 0 || descPos > 0 ) ) {
                    results.push( {
                        label: model.get( 'name' ) + " (" + model.get( "class" ) + ")",
                        value: model.get( 'class' )
                    } );
                }
            } );
            return results;
        },

        onAttach: function () {
            this.ui.type.autocomplete( {
                minLength: 0,
                source:    ( request, response ) => response( this._findQuiddity( request.term, true ) )
            } )
                .autocomplete( "widget" )
                .addClass( "overflow" );

            _.defer( _.bind( this.ui.type.focus, this.ui.type ) );
        },

        checkForEscapeKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ) {
                event.preventDefault();
                this.scenic.sessionChannel.commands.execute( 'inspector:close' );
            }
        },

        checkForEnterKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.add();
            }
        },

        add: function () {
            const term    = this.ui.type.val();
            const results = this._findQuiddity( term, true );
            this.callback( {
                type: results.length > 0 ? results[0].value : term,
                name: this.ui.name.val()
            } );
        },

        cancel: function () {
            this.scenic.sessionChannel.commands.execute( 'inspector:close' );
        }

    } );
    return AddQuiddity;
} );