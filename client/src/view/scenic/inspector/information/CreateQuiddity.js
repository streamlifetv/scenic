define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    './CreateQuiddity.html'
], function ( _, Backbone, Marionette, i18n, CreateQuiddityTemplate ) {

    /**
     * Create Quiddity Form
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */
    var CreateQuiddity = Marionette.ItemView.extend( {
        template:   CreateQuiddityTemplate ,
        className: 'create-quiddity dialog',
        ui:        {
            'name':   '#quiddityName',
            'device': '#device',
            'create': '#create',
            'cancel': '#cancel'
        },
        events:    {
            'click @ui.create':  'create',
            'click @ui.cancel':  'cancel',
            'keydown':           'checkForEscapeKey',
            'keypress @ui.name': 'checkForEnterKey'
        },

        initialize: function ( options ) {
            this.scenic   = options.scenic;
            this.title    = i18n.t( 'Create __quiddityName__', { quiddityName: this.model.get( 'name' ) } );
            this.callback = options.callback;

            //TODO: Handle devices stuff
            /*classDescription.loadDevices( function ( error, devices ) {
             if ( error ) {
             return;
             }
             }
             );*/
        },

        onAttach: function () {
            _.defer( _.bind( this.ui.name.focus, this.ui.name ) );
        },

        checkForEscapeKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ) {
                event.preventDefault();
                this.scenic.sessionChannel.commands.execute( 'inspector:close' );
            }
        },

        checkForEnterKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.create();
            }
        },

        create: function () {
            this.callback( {
                type:   this.model.get( 'class' ),
                name:   this.ui.name.val(),
                device: this.ui.device.val()
            } );
        },

        cancel: function () {
            this.scenic.sessionChannel.commands.execute( 'inspector:close' );
        }

    } );
    return CreateQuiddity;
} );