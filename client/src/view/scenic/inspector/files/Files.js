"use strict";

define( [
    'underscore',
    'jquery',
    'backbone',
    'marionette',
    'i18n',
    'view/scenic/inspector/files/File',
    './Files.html'
], function ( _, $, Backbone, Marionette, i18n, FileView, FilesTemplate ) {

    /**
     * Files View
     *
     * @constructor
     * @extends module:Marionette.CompositeView
     */

    var FilesView = Marionette.CompositeView.extend( {
        template:   FilesTemplate ,
        className: 'file-open',
        childView: FileView,
        childViewContainer: '.files',
        childViewOptions: function() {
            return {
                scenic: this.scenic
            }
        },

        childEvents: {
            'closeList': 'close'
        },

        ui: {
            'import':              '.import',
            'fileLoader':          '.fileLoader'
        },

        events:    {
            'click @ui.import':       'clickImport',
            'change @ui.fileLoader':  'importFile',
        },
        /**
         * Initialize
         */
        initialize: function ( options ) {
            this.scenic     = options.scenic;
            this.close      = options.close;
            this.title      = i18n.t( 'Open file' );
        },

        clickImport: function() {
            this.ui.fileLoader.click();
        },

        importFile: function( event ) {
            var file = event.target.files[0]; 
            if ( file ) {
                this.scenic.saveFiles.importFile( file.name, file );
            }
        }

    } );

    return FilesView;
} );
