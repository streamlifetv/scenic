"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './Information.html'
], function ( _, Backbone, Marionette, InformationTemplate ) {

    /**
     *  @constructor
     *  @augments module:Marionette.ItemView
     */
    var InformationView = Marionette.ItemView.extend( {
        template:  InformationTemplate ,

        ui : {
            'modal':    '.modal',
            'dialog':   '.dialog',
            'close':    '.close'
        },

        events: {
            'click @ui.close': 'close',
        },
        keyShortcuts: {
            'enter':   function () {
                this.callback();
            },
            'esc':   function () {
                this.callback();
            }
        },
        initialize: function ( options ) {
            this.message = options.message;
            this.callback = options.callback;
        },

        serializeData: function(){
            return {
                message: this.message
            };
        },

        close: function(){
            this.callback();
        }

    } );

    return InformationView;
} );
