"use strict";

import TableView          from './base/TableView';
import PropertiesTemplate from './PropertiesTableView.html';
import PropertiesMenuView from './properties/PropertiesMenuView';
import PropertiesInfoView from './properties/PropertiesInfoView';

/**
 * PropertiesTableView
 *
 * @constructor
 * @extends module:Backbone.Model
 */

var PropertiesTableView = TableView.extend( {
    className: 'table propertiesPage',
    template: PropertiesTemplate,

    ui: {
        fullscreen: '.fullscreen'
    },

    events: {
        'click @ui.fullscreen': 'setFullscreen'
    },

    regions: {
        'content':      '.page-content',
        'menu':         '.menu-bar'
    },

    modelEvents: {
        'change:fullscreen':  'render'
    },

    /**
     * Initialize
     */
    initialize: function( options ) {
        TableView.prototype.initialize.apply( this, arguments );
        this.listenTo( this.scenic.saveProperties, 'change:quiddities', this.showPropertiesView );
    },

    /**
     * Before Show Handler
     *
     * @private
     */
    onBeforeShow: function () {
        if ( !this.scenic.get('fullscreen') ){
            this.showChildView( 'menu', new PropertiesMenuView({
                scenic: this.scenic,
                model: this.scenic.saveProperties
            }));
        }
        this.showPropertiesView();
    },

    onRender: function () {
        if ( !this.model.get('fullscreen') ) {
            this.showChildView('menu', new PropertiesMenuView({
                scenic: this.scenic,
                model: this.scenic.saveProperties
            }));
        }
        this.showPropertiesView();
    },

    setFullscreen: function () {
        if ( !this.model.get('fullscreen') ){
            this.model.set('fullscreen', true );
        } else {
            this.model.set('fullscreen', false );
        }
        this.scenic.sessionChannel.commands.execute( 'set:fullscreen' );
    },

    showPropertiesView: function () {
        var quiddity = this.scenic.saveProperties.get('quiddities').find( item => { return item.selected == true});
        if ( quiddity ) {
            this.showChildView('content', new PropertiesInfoView({
                scenic: this.scenic,
                model: this.scenic.quiddities.get(quiddity.id)
            }));
        }
    }
} );

export default PropertiesTableView;
