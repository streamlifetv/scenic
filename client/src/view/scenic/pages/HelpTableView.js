"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './HelpTableView.html'
], function ( _, Backbone, Marionette, HelpTemplate ) {

    /**
     *  @constructor
     *  @augments PageView
     */
    var HelpTableView = Marionette.ItemView.extend( {
        tagName: 'div',
        className: 'help',
        template: HelpTemplate,

        /**
         * Initialize
         */
        initialize: function( ) {

        }
    } );

    return HelpTableView;
} );
