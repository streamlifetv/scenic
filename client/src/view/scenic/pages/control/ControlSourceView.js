"use strict";

import _ from 'underscore';
import QuidditySourceView from '../base/table/QuidditySourceView';
import ControlPropertyView from './ControlPropertyView';

/**
 * Control Source View
 *
 * @constructor
 * @extends SourceView
 */
const ControlSourceView = QuidditySourceView.extend( {
    childView: ControlPropertyView,

    initialize: function ( options ) {
        QuidditySourceView.prototype.initialize.apply( this, arguments );
        this.collection = this.model.properties;
    },

    filter: function ( property ) {
        return _.contains( this.table.allowedPropertyTypes, property.get( 'type' ) );
    },

    onSourceOver: function ( source ) {
        this.$el.addClass( this.model.id == source.collection.quiddity.id ? "highlighted" : "faded" );
    }
} );

export default ControlSourceView;