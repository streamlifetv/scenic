"use strict";

import i18n from "i18n";
import DestinationsView from '../base/table/DestinationsView';
import ControlDestinationView from './ControlDestinationView';

/**
 * Control Destination Collection
 *
 * @constructor
 * @augments DestinationsView
 */
const ControlDestinations = DestinationsView.extend( {
    childView:  ControlDestinationView,
    /**
     * Initialize
     */

    initialize: function ( options ) {
        DestinationsView.prototype.initialize.apply( this, arguments );
        this.title = i18n.t('Properties');
    }
} );

export default ControlDestinations;
