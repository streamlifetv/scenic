"use strict";

import _ from 'underscore';
import ConnectionView from '../base/table/ConnectionView';
import ControlConnectionTemplate from './ControlConnectionView.html';

/**
 * Control Connection View
 *
 * @constructor
 * @extends ConnectionView
 */
const ControlConnectionView = ConnectionView.extend( {
    template: ControlConnectionTemplate,

    ui: _.extend( {}, ConnectionView.prototype.ui, {
        edit: '.action.edit'
    } ),

    events: _.extend( {}, ConnectionView.prototype.events, {
        'click @ui.edit': 'editMapper'
    } ),

    /**
     * Initialize
     */
    initialize: function ( options ) {
        ConnectionView.prototype.initialize.apply( this, arguments );
        this.listenTo( this.table.destinations, 'update', this.render );
    },

    canConnect( source, destination, callback ) {
        return this.options.table.canConnect( source, destination, callback );
    },

    isConnected( source, destination ) {
        return this.options.table.isConnected( source, destination );
    },

    connect( source, destination ) {
        return this.options.table.connect( source, destination );
    },

    disconnect( source, destination ) {
        return this.options.table.disconnect( source, destination );
    },

    editMapper: function ( event ) {
        event.stopImmediatePropagation();
        var mapper = this.table.getConnection( this.source, this.destination );
        if ( mapper ) {
            mapper.edit();
        }
    }
} );

export default ControlConnectionView;
