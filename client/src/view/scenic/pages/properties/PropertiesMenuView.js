"use strict";

import TableMenusView from 'view/scenic/pages/base/TableMenusView';
import PropertiesMenuTemplate from './PropertiesMenuView.html';

const PropertiesMenuView = TableMenusView.extend( {
    template: PropertiesMenuTemplate,

    ui:  {
        button: '.buttonLabel',
        delete: '.delete'
    },

    events: {
        'click @ui.button': 'displayProperties',
        'click @ui.delete': 'deleteQuiddity'
    },

    modelEvents: {
        'change': 'render',
        'change:quiddities': 'render'
    },
    templateHelpers: function () {
        return {
            quiddities: this.model.get('quiddities')
        }
    },

    initialize: function () {
        TableMenusView.prototype.initialize.apply( this, arguments );
    },

    displayProperties: function ( event ) {
        this.model.select( event.target.id );
    },
    deleteQuiddity: function ( event ) {
        this.model.remove( event.target.id );
    }

} );

export default PropertiesMenuView;