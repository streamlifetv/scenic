"use strict";

import i18n from 'i18n';
import TableMenusView from 'view/scenic/pages/base/TableMenusView';
import ScenesMenuTemplate from './ScenesMenuView.html';

const ScenesMenuView = TableMenusView.extend( {
    template: ScenesMenuTemplate,

    ui:  {
        button:    '.buttonLabel',
        delete:    '.delete',
        add:       '.add',
        rename:    '.rename',
        inputName: '.inputSceneName'
    },

    events: {
        'click @ui.button':         'displayScene',
        'click @ui.add':            'addScene',
        'click @ui.delete':         'deleteScene',
        'dblclick @ui.rename':       'displayInput',
        'keypress @ui.inputName':   'checkForKey'
    },

    modelEvents: {
        'add remove':    'render',
        'change:select': 'render',
        'change:active': 'render',
        'change:name':   'render'
    },

    templateHelpers: function () {
        return {
            editName: this.editName,
            scenes: this.model
        }
    },

    initialize: function ( options ) {
        TableMenusView.prototype.initialize.apply( this, arguments );
        this.scenic = options.scenic;
        this.editName = false;

        if ( this.model.getToggleScene() ) {
            this.model.selectScene( this.model.getToggleScene().id );
        }
    },

    displayScene: function ( event ) {
        var scene = this.model.get( event.target.id  );
        if( !scene.get('properties').get('select')) {
            this.model.selectScene(event.target.id);
        }
    },

    addScene: function ( event ) {
        this.model.add();
    },

    checkForKey: function ( event ) {
        var key = event.which || event.keyCode;
        if (key == 27) {
            event.preventDefault();
            this.inputEscape( event );
        }
        if (key == 13) {
            event.preventDefault();
            this.inputEnter( event );
        }
    },

    inputEscape: function ( event ) {
        this.editName = false;
    },

    inputEnter: function ( event ) {
        var scene = this.model.getSelectedScene();
        if ( scene && scene.get('properties').get('name') != this.ui.inputName.val()) {
            scene.rename( this.ui.inputName.val());
        }
        this.editName = false;
        this.render();
    },

    displayInput: function ( event ) {
        this.editName = true;
        this.render();
    },

    deleteScene: function ( event ) {
        var scene = this.model.get( event.target.id  );
        if ( scene.get('properties').get('active')) {
            this.scenic.scenicChannel.commands.execute( 'inform', "Deactivate this scene before to erase it");
        } else {
            this.scenic.scenicChannel.commands.execute( 'confirm', i18n.t('Are you sure you want to remove the scene __scene__ ?', {scene: this.model.get(event.target.id).get('properties').get('name')}), (confirmed) => {
                if (confirmed) {
                    this.model.remove(event.target.id);
                }
            });
        }
    }
} );

export default ScenesMenuView;