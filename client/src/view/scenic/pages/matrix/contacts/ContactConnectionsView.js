"use strict";

import ConnectionsView from "../../base/table/ConnectionsView"
import ContactConnectionView from "./ContactConnectionView";

const ContactConnectionsView = ConnectionsView.extend( {
    childView: ContactConnectionView,

    initialize() {
        ConnectionsView.prototype.initialize.apply( this, arguments );
        this.listenTo( this.collection, 'update', this.render );
    },

    viewComparator: function ( destination ) {
        return this.options.table.contactDestinationComparator( destination );
    },

    filter: function ( destination ) {
        return this.options.table.filterContactDestination( destination, true );
    }
} );
export default ContactConnectionsView;