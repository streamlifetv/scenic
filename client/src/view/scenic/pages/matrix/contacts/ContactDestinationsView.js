"use strict";

import i18n from 'i18n';
import DestinationsView from '../../base/table/DestinationsView';
import ContactDestinationView from './ContactDestinationView';

/**
 * SIP Destination Collection
 *
 * @constructor
 * @augments DestinationsView
 */
const ContactDestinationsView = DestinationsView.extend( {
    childView: ContactDestinationView,

    initialize: function () {
        DestinationsView.prototype.initialize.apply( this, arguments );
        this.title = i18n.t( 'Contacts' );
        this.listenTo( this.collection, 'change:connections', this.render );
    },

    viewComparator: function ( destination ) {
        return this.options.table.contactDestinationComparator( destination );
    },

    filter: function ( destination ) {
        return this.table.filterContactDestination( destination, true );
    }

} );

export default ContactDestinationsView;