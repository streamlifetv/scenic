"use strict";

import i18n from 'i18n'
import DestinationsView from '../../base/table/DestinationsView';
import SinkDestinationView from './SinkDestinationView';

/**
 * Destination Collection
 *
 * @constructor
 * @augments DestinationsView
 */
const SinkDestinationsView = DestinationsView.extend( {
    childView: SinkDestinationView,

    initialize() {
        DestinationsView.prototype.initialize.apply( this, arguments );
        this.title = i18n.t( 'Destinations' );
    },

    viewComparator: function ( destination ) {
        return this.options.table.sinkDestinationComparator( destination );
    },

    filter: function ( destination ) {
        return this.options.table.filterSinkDestination( destination, true );
    }
} );

export default SinkDestinationsView;
