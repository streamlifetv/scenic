"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'view/scenic/pages/base/table/DestinationView',
    './SinkDestinationView.html'
], function ( _, Backbone, Marionette, i18n, DestinationView, DestinationTemplate ) {

    /**
     * Destination View
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */
    var SinkDestinationView = DestinationView.extend( {
        template:  DestinationTemplate ,
        className: 'destination',

        templateHelpers: function() {
            return {
                classDescription: this.model.has('classDescription' ) ? this.model.get('classDescription' ).toJSON() : null,
                editName:         this.model.get('editName')
            }
        },
        attributes: function () {
            return {
                class: [ 'sink', 'destination'].join(' ')
            }
        },
        

        ui: {
            edit:       '.info.edit',
            name:       '.name',
            inputName:  '.inputName',
            moreEdit:   '.actions-menu .items .item.edit',
            remove:     '.actions-menu .items .item.remove',
            more:       '.action.more-action'
        },

        events: {
            'click @ui.edit':           'editDestination',
            'click @ui.remove':         'removeDestination',
            'click @ui.moreEdit':       'editDestination',
            'click @ui.more':           'dropMore',
            'dblclick @ui.edit':        'displayInput',
            'dblclick @ui.name':        'displayInput',
            'click @ui.inputName':      'focusInputName',
            'keypress @ui.inputName':   'checkForKey'
        },

        modelEvents: {
            'change:name':  'render',
            'change:editName':  'focusInputName'
        },

        focusInputName: function ( event ) {
            this.render();
            if (  this.model.get('editName') ) {
                this.ui.inputName.focus();
                this.ui.inputName.select();
            }
        },

        checkForKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 27 ){
                event.preventDefault();
                this.inputEscape();
            }
            if ( key == 13 ) {
                event.preventDefault();
                this.inputEnter();
            }
        },

        inputEscape: function ( ) {
            this.model.set('editName', false );
        },

        inputEnter: function ( ) {
            this.renameQuiddity(this.ui.inputName.val());
            this.model.set('editName', false );
        },

        displayInput: function( event ) {
            this.model.set('editName', true );
        },

        renameQuiddity: function( name ) {
            this.model.rename( name );
        },

        onRender: function() {
            // Unfortunately, we have to keep the id in the DOM
            // for jQuery sortable to know what element was dragged.
            this.$el.data("id", this.model.id);
        },

        /**
         * Edit Handler
         * @param event
         */
        editDestination: function( event ) {
            this.closeMenu();
            this.model.edit();
        },

        /**
         * Drop the menu More with edit and remove actions
         * @param event
         */
        dropMore: function ( event ) {
            this.drop( this.$el, this.model.get( 'actions' ) )
        },

        /**
         * Remove Handler
         * @param event
         */
        removeDestination: function( event ) {
            this.closeMenu();
            var self = this;
            this.scenic.scenicChannel.commands.execute( 'confirm', i18n.t('Are you sure you want to remove the __destinationName__ destination?', {destinationName:this.model.get('name')}), function( confirmed ) {
                if ( confirmed ) {
                    self.model.destroy();
                }
            });
        },

        onDestinationOver: function(destination) {
            this.$el.addClass( this.model.id == destination.id ? "highlighted" : "faded" );
        },

        onDestinationOut: function(destination) {
            this.$el.removeClass("highlighted faded");
        }
    } );

    return SinkDestinationView;
} );

// destinationGroupView