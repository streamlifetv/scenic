"use strict";

import Field from 'view/scenic/inspector/information/edit/property/Field';
import SceneActivationTemplate from './SceneActivationView.html';

const ScenesActivationView = Field.extend( {
    template: SceneActivationTemplate,


    ui: {
        'checkboxScene':  '#checkboxScene',
    },

    events: {
        'change @ui.checkboxScene': 'updateModel'
    },

    templateHelpers: function () {
        return {
            started:  this.model.scenic.scenes.getSelectedScene() ? this.model.scenic.scenes.getSelectedScene().get('properties').get('active') : false
        }
    },

    /**
     * Initialize
     */
    initialize: function () {
        this.listenTo( this.model.scenic.scenes, 'add remove', this.render );
        this.listenTo( this.model.scenic.scenes, 'change:active', this.render );
        this.listenTo( this.model.scenic.scenes, 'change:select', this.render );
    },

    /**
     * Update Model Value
     *
     * @param event
     */
    updateModel: function ( event ) {
        // We have a custom checkbox, this is needed to toggle and set the actual value
        var checked = this.ui.checkboxScene.is( ':checked' );
        this._updateChecked( checked );

        // Update the model
        if ( checked ) {
            this.model.scenic.scenes.switchScene( this.model.scenic.scenes.getSelectedScene().id );
        } else {
            this.model.scenic.scenes.deactivateScene( this.model.scenic.scenes.getSelectedScene().id );
        }
    },

    /**
     * Set the value of the checkbox
     * @inheritdoc
     */
    onModelValueChanged: function ( model, value, options ) {
        this._updateChecked( value );
    },

    _updateChecked( checked ) {
        this.ui.checkboxScene.val( checked ).attr( 'checked', checked );
        this.ui.checkboxScene.addClass( checked ? 'checked' : 'unchecked' ).removeClass( checked ? 'unchecked' : 'checked' );
    }
} );

export default ScenesActivationView;