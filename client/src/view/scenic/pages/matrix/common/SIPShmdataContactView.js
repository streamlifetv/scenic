"use strict";

import Marionette from 'marionette';
import ShmdataView from './ShmdataView';
import SIPShmdataContactViewTemplate from './SIPShmdataContactView.html';

/**
 * SIP Shmdata Contact View
 *
 * @constructor
 * @extends module:Marionette.CompositeView
 */
const SIPShmdataContactView = Marionette.CompositeView.extend( {
    template:           SIPShmdataContactViewTemplate,
    className:          'contact',
    childView:          ShmdataView,
    childViewOptions:   function () {
        return {
            scenic:     this.scenic,
            table:      this.options.table
        }
    },
    childViewContainer: '.shmdatas',

    attributes: function () {
        return {
            class: ['contact', this.model.get( 'status' ).toLowerCase(), this.model.get( 'subscription_state' ).toLowerCase()].join( ' ' )
        }
    },

    modelEvents: {
        'change': 'render'
    },

    /**
     * Initialize
     */
    initialize: function ( options ) {
        this.scenic = options.scenic;
    },

    onRender: function () {
        // Update Dynamic Attributes
        this.$el.attr( this.attributes() );
    },

    /**
     * Only show shmdata for this sip contact
     *
     * @param shmdata
     * @param index
     * @param collection
     * @returns {boolean}
     */
    filter: function ( shmdata, index, collection ) {
        return this.model.get( 'uri' ) == shmdata.get( 'uri' );
    }
} );

export default SIPShmdataContactView;
