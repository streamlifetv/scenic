"use strict";

import Marionette from 'marionette';
import SinkConnectionsView from '../sink/SinkConnectionsView';
import ContactConnectionsView from '../contacts/ContactConnectionsView';
import ShmdataTemplate from './ShmdataView.html';

/**
 * Shmdata View
 *
 * @constructor
 * @extends module:Marionette.CompositeView
 */
const ShmdataView = Marionette.LayoutView.extend( {
    template:  ShmdataTemplate,
    className: 'source-child shmdata',

    regions: {
        'sink':     '.sink.connections',
        'contacts': '.contact.connections',
        'rtp':      '.rtp.connections'
    },

    modelEvents: {
        'change:byte_rate': 'updateByteRate',
        'change:thumbnail': 'updateThumbnail'
    },

    ui: {
        info:           '.actions .action.more',
        preview:        '.preview',
        thumbnailImage: '.preview .thumbnail img',
        sink:           '.sink.connections',
        contacts:       '.contact.connections',
        rtp:            '.rtp.connections',
        byteRate:       '.info-byte-rate'
    },

    events: {
        'click @ui.info':    'showInfo',
        'click @ui.preview': 'showPreview'
    },

    viewComparator: function ( destination ) {
        return this.options.table.sinkDestinationComparator( destination );
    },

    /**
     * Initialize
     */
    initialize: function ( options ) {
        Marionette.LayoutView.prototype.initialize.apply( this, arguments );
    },

    /**
     * Before Show Handler
     *
     * @private
     */
    onBeforeShow: function () {
        this.showChildView( 'sink', new SinkConnectionsView( {
            scenic:     this.options.scenic,
            table:      this.options.table,
            source:     this.model,
            collection: this.options.table.getSinkDestinationCollection()
        } ) );

        this.showChildView( 'contacts', new ContactConnectionsView( {
            scenic:     this.options.scenic,
            table:      this.options.table,
            source:     this.model,
            collection: this.options.table.getContactDestinationCollection()
        } ) );
    },

    updateThumbnail: function () {
        this.ui.thumbnailImage.attr( 'src', this.model.get( 'thumbnail' ) );
    },

    /**
     * Render Handler
     */
    onRender: function () {
        // Check the byte rate to update status
        this.updateByteRate();
    },

    /**
     * Show Info
     * @param event
     */
    showInfo: function ( event ) {
        this.options.scenic.sessionChannel.commands.execute( 'shmdata:info', this.model );
    },

    /**
     * Show Preview
     * @param event
     */
    showPreview: function ( event ) {
        if ( this.model.get( 'hasThumbnail' ) ) {
            this.options.scenic.sessionChannel.commands.execute( 'shmdata:preview', this.model );
        }
    },

    /**
     * Update byte rate
     */
    updateByteRate: function () {
        if ( !this.model.get( 'byte_rate' ) ) {
            this.ui.byteRate.addClass( 'deactive' );
            this.ui.byteRate.removeClass( 'active' );
        } else {
            this.ui.byteRate.addClass( 'active' );
            this.ui.byteRate.removeClass( 'deactive' );
        }
    },

    /**
     * Filter destinations in order to display connections for shmdata
     *
     * @param destination
     * @returns {boolean}
     */
    filter: function ( destination ) {
        // Get back up to the table model to filter the displayed connections
        return this.options.table.filterSinkDestination( destination, true );
    }
} );

export default ShmdataView;
