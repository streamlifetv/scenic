"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    'view/scenic/pages/advanced/QuidditiesView',
    './AdvancedTableView.html'
], function ( _, Backbone, Marionette, QuidditiesView, AdvancedTemplate ) {

    /**
     *  @constructor
     *  @augments Marionette.ItemView
     */
    var AdvancedTableView = Marionette.LayoutView.extend( {
        tagName: 'div',
        className: 'advanced table',
        template: AdvancedTemplate,
        regions: {
            'list': '.quiddities-list'
        },
        
        /**
         * Initialize
         */
        initialize: function( options ) {
            Marionette.CompositeView.prototype.initialize.apply(this, arguments);
            this.scenic = options.scenic;
        },

        /**
         * Before Show Handler
         *
         * @private
         */
        onBeforeShow: function () {
            this.showChildView( 'list', new QuidditiesView( {
                scenic: this.scenic,
                model:  this.model
            } ) );
        }
    } );

    return AdvancedTableView;
} );
