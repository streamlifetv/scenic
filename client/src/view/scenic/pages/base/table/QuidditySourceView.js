"use strict";

import $ from 'jquery';
import _ from "underscore";
import i18n from "i18n";
import SourceView from "./SourceView";
import QuidditySourceTemplate from "./QuidditySourceView.html";

const QuidditySourceView = SourceView.extend( {
    template:  QuidditySourceTemplate,

    className: 'quiddity source',
    
    id: function(){
        return this.model.id;
    },

    templateHelpers: function () {
        return {
            readOnly:         this.model.get( 'readOnly' ),
            startable:        this.model.properties.get( 'started' ) != null,
            started:          this.model.properties.get( 'started' ) ? this.model.properties.get( 'started' ).get( 'value' ) : true,
            classDescription: this.model.get( 'classDescription' ).toJSON(),
            editName:         this.model.get('editName')
        }
    },

    ui: _.defaults( {
        edit:       '.info.edit',
        name:       '.name',
        inputName:  '.inputName',
        moreEdit:   '.actions-menu .items .item.edit',
        remove:     '.actions-menu .items .item.remove',
        more:       '.action.more-action',
        power:      '.action.power'
    }, SourceView.prototype.ui ),

    events: _.defaults( {
        'click @ui.edit':           'editSource',
        'click @ui.moreEdit':       'editSource',
        'click @ui.remove':         'removeSource',
        'click @ui.more':           'dropMore',
        'click @ui.power':          'togglePower',
        'dblclick @ui.edit':        'displayInput',
        'dblclick @ui.name':        'displayInput',
        'click @ui.inputName':      'focusInputName',
        'keypress @ui.inputName':   'checkForKey'
    }, SourceView.prototype.events ),

    modelEvents: {
        'change:name':          'render',
        'change:editName':  'focusInputName'
    },

    /**
     * Initialize
     */
    initialize: function ( options ) {
        SourceView.prototype.initialize.apply( this, arguments );
        if ( this.model.properties.get( 'started' ) ) {
            this.listenTo( this.model.properties.get( 'started' ), 'change:value', this.render );
        }
    },

    focusInputName: function ( event ) {
        this.render();
        if (  this.model.get('editName') ) {
            this.ui.inputName.focus();
            this.ui.inputName.select();
        }
    },

    checkForKey: function ( event ) {
        var key = event.which || event.keyCode;
        if ( key == 27 ){
            event.preventDefault();
            this.inputEscape();
        }
        if ( key == 13 ) {
            event.preventDefault();
            this.inputEnter();
        }
    },

    inputEscape: function ( ) {
        this.model.set('editName', false );
    },

    inputEnter: function ( ) {
        this.renameQuiddity(this.ui.inputName.val());
        this.model.set('editName', false );
    },

    displayInput: function( event ) {
        this.model.set('editName', true );
        this.ui.inputName.focus();
        this.ui.inputName.select();
    },

    renameQuiddity: function( name ) {
        this.model.rename( name );
    },

    /**
     * Edit Handler
     * @param event
     */
    editSource: function ( event ) {
        this.closeMenu();
        this.model.edit();
    },

    /**
     * Remove Handler
     * @param event
     */
    removeSource: function ( event ) {
        this.closeMenu();
        var self = this;
        this.scenic.scenicChannel.commands.execute( 'confirm', i18n.t( 'Are you sure you want to remove the __sourceName__ source?', { sourceName: this.model.get('name') } ), function ( confirmed ) {
            if ( confirmed ) {
                self.model.destroy();
            }
        } );
    },

    /**
     * Drop the menu More with edit and remove actions
     * @param event
     */
    dropMore: function ( event ) {
        this.drop( this.$el, this.model.get( 'actions' ) )
    },

    togglePower: function ( event ) {
        var self = this;
        if ( this.model.properties.get( 'started' ) ) {
            if ( this.model.properties.get( 'started' ).get( 'value' ) ) {
                this.scenic.scenicChannel.commands.execute( 'confirm', i18n.t( 'Are you sure you want to stop __quiddity__ source?', { quiddity: this.model.get('name') } ), function ( confirmed ) {
                    if ( confirmed ) {
                        self.model.properties.get( 'started' ).updateValue( false );
                    }
                } );
            } else {
                self.model.properties.get( 'started' ).updateValue( true );
            }
        }
    },

    onSourceOver: function ( source ) {
        this.$el.addClass( this.model.id == source.id ? "highlighted" : "faded" );
    },

    onSourceOut: function ( source ) {
        this.$el.removeClass( "highlighted faded" );
    }
} );

export default QuidditySourceView;