"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './ConnectionView.html'
], function ( _, Backbone, Marionette, ConnectionTemplate ) {

    /**
     * Connection View
     *
     * @constructor
     * @extends module:Marionette.CompositeView
     */
    var ConnectionView = Marionette.ItemView.extend( {
        template:  ConnectionTemplate,
        className: 'connection',

        ui: {
            connect:    '.action.connect',
            disconnect: '.action.disconnect',
            select: '.action.selected',
        },

        events: {
            'click @ui.connect':    'toggleConnection',
            'click @ui.disconnect': 'toggleConnection',
            'click @ui.select':     'toggleConnection',
            'mouseover':            'onMouseOver',
            'mouseout':             'onMouseOut'
        },

        templateHelpers: function () {
            return {
                connected: this.isConnected( this.source, this.destination ) && this.scenic.scenes.isActiveScene(),
                selected:  this.isSelected( this.source, this.destination ) ,
            }
        },

        /**
         * 
         * Initialize
         */
        initialize: function ( options ) {
            // Keep options internally
            this.scenic      = options.scenic;
            this.source      = options.source;
            this.destination = options.model;
            this.selected    = this.isSelected( this.source, this.destination );
            this.connected   = this.isConnected( this.source, this.destination );

            // Check if we can connect, this only need to happen once
            this.canConnect( this.source, this.destination, canConnect => {
                this._canConnect = canConnect;
                this.$el.removeClass( 'enabled disabled' ).addClass( this._canConnect ? 'enabled' : 'disabled' );
            } );

            this.listenTo( this.scenic.scenes, "change:select", this.renderSelect );
            this.listenTo( this.scenic.scenes, "change:connections", this.renderSelect );
            this.listenTo( this.scenic.scenes, "change:active", this.renderActive );
        },

        renderSelect: function () {
            if( this.isSelected( this.source, this.destination ) != this.selected ) {
                this.selected = this.isSelected( this.source, this.destination );
                this.render();
            }
        },

        renderActive: function () {
            if(this.isSelected(this.source,this.destination ) && this.isConnected( this.source, this.destination ) && this.scenic.scenes.isActiveScene()){
               this.render();
           }
        },

        toggleConnection: function () {
            if ( !this._canConnect ) {
                return;
            }

            if ( this.scenic.scenes.isActiveScene() ) {
                if ( this.isConnected( this.source, this.destination )) {
                    this.deselect( this.source, this.destination );
                    this.disconnect(this.source, this.destination);
                } else {
                    this.select( this.source, this.destination );
                    this.connect(this.source, this.destination);
                }
            } else if ( this.isSelected( this.source, this.destination )) {
                this.deselect( this.source, this.destination );
            } else {
                this.select( this.source, this.destination );
            }
        },

        isSelected( source, destination ) {
            return this.scenic.scenes.getSelectedScene() ? this.scenic.scenes.getSelectedScene().getConnection( source.collection.quiddity.id, destination.id ) : false;
        },

        select( source, destination ) {
            throw new Error('Please override select() in subclass');
        },

        deselect( source, destination ) {
            throw new Error('Please override canConnect() in subclass');
        },

        canConnect( source, destination, callback ) {
            throw new Error('Please override deselect() in subclass');
        },

        isConnected( source, destination ) {
            throw new Error('Please override isConnected() in subclass');
        },

        connect( source, destination ) {
            throw new Error('Please override connect() in subclass');
        },

        disconnect( source, destination ) {
            throw new Error('Please override disconnect() in subclass');
        },

        onMouseOver: function () {
            this.scenic.sessionChannel.vent.trigger( 'source:over', this.source );
            this.scenic.sessionChannel.vent.trigger( 'destination:over', this.destination );
        },

        onMouseOut: function () {
            this.scenic.sessionChannel.vent.trigger( 'source:out', this.source );
            this.scenic.sessionChannel.vent.trigger( 'destination:out', this.destination );
        }
    } );

    return ConnectionView;
} );
