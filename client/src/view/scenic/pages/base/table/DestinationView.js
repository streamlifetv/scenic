"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'jquery-ui/ui/widgets/accordion',
    'jquery-ui/ui/widgets/button',
    'view/scenic/pages/matrix/common/QuiddityActionMenuView.html'
], function ( $, _, Backbone, Marionette, i18n, accordion, button, QuiddityActionMenuView  ) {

    /**
     * Destination View
     *
     * @constructor
     * @extends module:Marionette.ItemView
     */
    var DestinationView = Marionette.ItemView.extend( {
        className: 'destination',

        id: function(){ 
            return this.model.id;
        },

        /**
         * Initialize
         */
        initialize: function( options ) {
            this.scenic = options.scenic;
            this.table = options.table;
            this.scenic.sessionChannel.vent.on( 'destination:over', this.onDestinationOver, this );
            this.scenic.sessionChannel.vent.on( 'destination:out', this.onDestinationOut, this );

            this.QuiddityActionMenuView = QuiddityActionMenuView;
        },

        onBeforeDestroy: function() {
            this.scenic.sessionChannel.vent.off( null, null, this );
        },

        onDestinationOver: function(destination) {
            this.$el.addClass( this.model.id == destination.id ? "highlighted" : "faded" );
        },

        onDestinationOut: function(destination) {
            this.$el.removeClass("highlighted faded");
        },

        drop( anchor, data ) {
            this.closeMenu();
            this.ui.more.addClass('selected');
            if ( data.length == 0 ) {
                return;
            }
            $( anchor ).append( this.QuiddityActionMenuView(data) );
            $( anchor ).i18n();

            $( '.actions-menu').addClass('destinationMenu');
            $( '.actions-menu .content' ).accordion( {
                collapsible: true,
                active:      false,
                heightStyle: 'content',
                icons:       false,
                animate:     125
            } );

            //TODO: Just style the link correctly
            $( '.actions-menu a' ).button();

            this.bodyClickHandler = _.bind( this.closeMenu, this );

            const $overlay = $( '.overlay' );
            $overlay.on( 'click', this.bodyClickHandler );
        },

        closeMenu() {
            $( '.actions-menu' ).remove();
            this.ui.more.removeClass('selected');
            $( '.overlay' ).off( 'click', this.bodyClickHandler );
        }
    } );

    return DestinationView;
} );
