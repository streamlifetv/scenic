"use strict";

define( [
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'i18n',
    'jquery-ui/ui/widgets/accordion',
    'jquery-ui/ui/widgets/button',
    'view/scenic/pages/matrix/common/QuiddityActionMenuView.html'

], function ( $, _, Backbone, Marionette, i18n, accordion, button, QuiddityActionMenuView ) {

    /**
     * Source View
     *
     * @constructor
     * @extends module:Marionette.CompositeView
     */
    var SourceView = Marionette.CompositeView.extend( {

        className: 'source',

        childViewOptions: function () {
            return {
                scenic:         this.scenic,
                table:          this.options.table,
                source:         this.model
            }
        },

        childViewContainer: '.source-children',

        ui: {
            
        },

        events: {
            'mouseover':        'onMouseOver',
            'mouseout':         'onMouseOut'
        },

        /**
         * Initialize
         */
        initialize: function ( options ) {
            this.scenic     = options.scenic;
            this.table      = options.table;
            
            this.scenic.sessionChannel.vent.on( 'source:over', this.onSourceOver, this );
            this.scenic.sessionChannel.vent.on( 'source:out', this.onSourceOut, this );

            this.QuiddityActionMenuView = QuiddityActionMenuView;
        },

        onRender: function () {
            // Unfortunately, we have to keep the id in the DOM
            // for jQuery sortable to know what element was dragged.
            this.$el.data( "id", this.model.id );
        },

        onBeforeDestroy: function () {
            this.scenic.sessionChannel.vent.off( null, null, this );
        },
        
        onSourceOver: function ( source ) {
            //this.$el.addClass( this.model.id == source.collection.quiddity.id ? "highlighted" : "faded" );
        },

        onSourceOut: function ( source ) {
            //this.$el.removeClass( "highlighted faded" );
        },

        drop( anchor, data ) {
            this.closeMenu();
            this.ui.more.addClass('selected');
            if ( data.length == 0 ) {
                return;
            }

            $( anchor ).append( this.QuiddityActionMenuView(data) );
            $( anchor ).i18n();
            $( '.actions-menu').addClass('sourceMenu');
            $( '.actions-menu .content' ).accordion( {
                collapsible: true,
                active:      false,
                heightStyle: 'content',
                icons:       false,
                animate:     125
            } );

            //TODO: Just style the link correctly
            $( '.actions-menu a' ).button();

            this.bodyClickHandler = _.bind( this.closeMenu, this );

            const $overlay = $( '.overlay' );
            $overlay.on( 'click', this.bodyClickHandler );
        },

        closeMenu() {
            $( '.actions-menu' ).remove();
            this.ui.more.removeClass('selected');
            $( '.overlay' ).off( 'click', this.bodyClickHandler );
        }

    } );

    return SourceView;
} );
