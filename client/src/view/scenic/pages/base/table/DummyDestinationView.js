"use strict";

import _ from 'underscore';
import DestinationView from './DestinationView';
import DummyDestinationTemplate from './DummyDestinationView.html';

/**
 * Dummy Destination View
 *
 * @constructor
 * @extends module:DestinationView
 */
const DummyDestinationView = DestinationView.extend( {
    template: DummyDestinationTemplate,

    className: 'dummy destination',

    events: _.defaults( {
        'click': 'addSource'
    }, DestinationView.prototype.events ),

    templateHelpers: function () {
        return {
            disableDestinationsMenu: this.scenic.config.disableDestinationsMenu
        };
    },
    
    initialize: function ( options ) {
        this.scenic = options.scenic;
        if ( this.scenic.config.disableDestinationsMenu ) {
             this.$el.addClass('nohover');
        }
    },

    addSource() {
        if ( this.options.onClick ) {
            this.options.onClick();
        }
    }
} );

export default DummyDestinationView;