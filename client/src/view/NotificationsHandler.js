"use strict";

import i18n from 'i18n';
import spin from 'lib/spin';

export default class NotificationsHandler {

    constructor(scenic) {
        this.scenic = scenic;

        this.scenic.sessionChannel.vent.on( 'quiddity:added', this._onQuiddityAdded, this );
        this.scenic.sessionChannel.vent.on( 'quiddity:removed', this._onQuiddityRemoved, this );
        this.scenic.sessionChannel.vent.on( 'file:reset', this._onReset, this );
        this.scenic.sessionChannel.vent.on( 'file:saved', this._onFileSaved, this );
        this.scenic.sessionChannel.vent.on( 'file:removed', this._onFileRemoved, this );
        this.scenic.sessionChannel.vent.on( 'file:renamed', this._onFileRenamed, this );
        this.scenic.sessionChannel.vent.on( 'file:loading', this._onFileLoading, this );
        this.scenic.sessionChannel.vent.on( 'file:loaded', this._onFileLoaded, this );
        this.scenic.sessionChannel.vent.on( 'file:load:error', this._onFileLoadError, this );
        this.scenic.sessionChannel.vent.on( 'file:get:error', this._onFileGetError, this );
        this.scenic.sessionChannel.vent.on( 'file:import:error', this._onFileImportError, this );
        this.scenic.sessionChannel.vent.on( 'chat:connected', this._onChatConnected, this );

        this._onLogMessage = this._onLogMessage.bind(this);
        this.scenic.socket.on( 'log.message', this._onLogMessage );
    }

    /**
     * Remove the listener on message from swtichwer to prevent multiple notification subscriptions
     * @public
     */
    removeMessageListener() {
        this.scenic.socket.removeAllListeners( 'log.message', this._onLogMessage );
        this.scenic.sessionChannel.vent.off( null, null, this );
    }

    //   ██████╗ ██╗   ██╗██╗██████╗ ██████╗ ██╗████████╗██╗███████╗███████╗
    //  ██╔═══██╗██║   ██║██║██╔══██╗██╔══██╗██║╚══██╔══╝██║██╔════╝██╔════╝
    //  ██║   ██║██║   ██║██║██║  ██║██║  ██║██║   ██║   ██║█████╗  ███████╗
    //  ██║▄▄ ██║██║   ██║██║██║  ██║██║  ██║██║   ██║   ██║██╔══╝  ╚════██║
    //  ╚██████╔╝╚██████╔╝██║██████╔╝██████╔╝██║   ██║   ██║███████╗███████║
    //   ╚══▀▀═╝  ╚═════╝ ╚═╝╚═════╝ ╚═════╝ ╚═╝   ╚═╝   ╚═╝╚══════╝╚══════╝

    /**
     * Quiddity Added Handler
     *
     * @param quiddity
     * @private
     */
    _onQuiddityAdded( quiddity ) {
        this.scenic.sessionChannel.vent.trigger( 'success', i18n.t( 'Quiddity __name__ added', { name: quiddity.id } ) );
    }

    /**
     * Quiddity Removed Handler
     *
     * @param quiddity
     * @private
     */
    _onQuiddityRemoved( quiddity ) {
        this.scenic.sessionChannel.vent.trigger( 'success', i18n.t( 'Quiddity __name__ removed', { name: quiddity.id } ) );
    }

    //  ███████╗██╗██╗     ███████╗███████╗
    //  ██╔════╝██║██║     ██╔════╝██╔════╝
    //  █████╗  ██║██║     █████╗  ███████╗
    //  ██╔══╝  ██║██║     ██╔══╝  ╚════██║
    //  ██║     ██║███████╗███████╗███████║
    //  ╚═╝     ╚═╝╚══════╝╚══════╝╚══════╝

    /**
     * Reset Handler
     * @private
     */
    _onReset() {
        this.scenic.sessionChannel.vent.trigger( 'success', i18n.t( 'Session reset successfully' ) );
    }

    /**
     * File Saved Handler
     *
     * @param file
     * @private
     */
    _onFileSaved( file ) {
        this.scenic.sessionChannel.vent.trigger( 'info', i18n.t( 'File __name__ saved', { name: file.get( 'name' ) } ) );
    }

    /**
     * File Removed Handler
     *
     * @param fileName
     * @private
     */
    _onFileRemoved( fileName ) {
        this.scenic.sessionChannel.vent.trigger( 'info', i18n.t( 'File __name__ removed', { name: fileName } ) );
    }

    /**
     * File Loading Handler
     *
     * @param {SaveFile} file
     * @private
     */
    _onFileLoading( file ) {
        this.scenic.sessionChannel.vent.trigger( 'info', i18n.t( 'Loading file __name__', { name: file.get( 'name' ) } ) );
        this.stopSpinner = spin();
    }

    /**
     * File Loaded Handler
     * @param {SaveFile} file
     * @private
     */
    _onFileLoaded( file ) {
        this.scenic.sessionChannel.vent.trigger( 'success', i18n.t( 'File __name__ loaded successfully', { name: file.get( 'name' ) } ) );
        this.stopSpinner();
    }

    /**
     * File Renamed Handler
     * @param {SaveFile} file
     * @private
     */
    _onFileRenamed( fileName ) {
        console.log('fileName ', fileName)
        this.scenic.sessionChannel.vent.trigger( 'success', i18n.t( 'File __name__ renamed successfully', { name: fileName.rename } ) );
    }

    /**
     * File Error Handler
     *
     * @param {SaveFile} file
     * @private
     */
    _onFileLoadError( file ) {
        this.scenic.sessionChannel.vent.trigger( 'error', i18n.t( 'Could not load file __name__', { name: file.get( 'name' ) } ) );
        if ( this.stopSpinner ) {
            this.stopSpinner();
        }
    }

    /**
     * File Error Handler
     *
     * @param {SaveFile} file
     * @private
     */
    _onFileGetError( fileName ) {
        this.scenic.sessionChannel.vent.trigger( 'error', i18n.t( "File __name__ doesn't exist", { name: fileName } ) );
    }
    /**
     * File Error Handler
     *
     * @param {ImportFile} file
     * @private
     */
    _onFileImportError( fileName ) {
        this.scenic.sessionChannel.vent.trigger( 'error', i18n.t( 'File __name__ already exists', { name: fileName } ) );
    }
    /**
     * Message Handler
     *
     * @param {error} error message
     * @private
     */
    _onLogMessage( message ) {
        if( message.indexOf('ERROR:') != -1 ){
            const errorMessage = message.split( 'ERROR:' )[1];
            if ( errorMessage ) {
                this.scenic.sessionChannel.vent.trigger( 'error', errorMessage.trim() );
            }
        }
    }

    _onChatConnected( ) {
        this.scenic.sessionChannel.vent.trigger( 'success', i18n.t( 'Connected to the chat' ) );
    }


}