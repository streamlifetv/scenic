"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './AuthenticationView.html'
], function ( _, Backbone, Marionette, AuthenticationView ) {

    /**
     * Login View
     *
     * @constructor
     * @extends Marionette.ItemView
     * @exports client/view/AuthenticationView
     */

    var AuthenticationView = Marionette.ItemView.extend( {
        template: AuthenticationView,
        el:       '#authenticate',

        ui: {
            password:      '#authPassword',
            login:         '#authLogin',
        },
        modelEvents: {
            'change:message': 'render'
        },
        events: {
            'click @ui.login':       'login',
            'keypress @ui.password': 'checkForEnterKey'
        },
        templateHelpers: function () {
            return {
                message: this.model.get( 'message' )
            };
        },
        /**
         * Initialize
         */
        initialize: function () {

        },
        checkForEnterKey: function ( event ) {
            var key = event.which || event.keyCode;
            if ( key == 13 ) {
                event.preventDefault();
                this.login();
            }
        },
        login: function () {
            this.model.login(this.ui.password.val());
        }
    } );

    return AuthenticationView;
} );
