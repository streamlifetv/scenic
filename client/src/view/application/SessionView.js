"use strict";

define( [
    'underscore',
    'backbone',
    'marionette',
    './SessionView.html'
], function ( _, Backbone, Marionette, SessionTemplate ) {

    /**
     * Session View
     *
     * @constructor
     * @extends Marionette.ItemView
     * @exports client/view/SessionView
     */

    var SessionView = Marionette.ItemView.extend( {

        template: SessionTemplate,

        events:    {
            'click': 'activate'
        },

        modelEvents: {
            "change:active": "render"
        },

        /**
         * Initialize
         */
        initialize: function (options) {

        },

        /**
         * Activate Handler
         * Activates the model assigned to this session
         */
        activate: function() {
            this.model.activate();
        }
    } );

    return SessionView;
} );
