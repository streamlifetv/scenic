![Scenic Logo](/public/assets/images/scenic-signature-horizontal.png)

# Scenic
Scenic is a stage teleconference system that allows for real time transmission of audiovisual data and arbitrary data flow over any IP network. Telepresence systems can be used in various artistic contexts, so that two different creative spaces can communicate with each other or present a combined performance. Scenic is a rewrite and improvement of the previous Scenic software.

Scenic graphical user interface is available through use of a web browser. It is supporting multiple simultaneous users and secured with password.

The current version of Scenic has been tested with Ubuntu 16.04.

## License
GNU GPL v3

## Project URL
https://gitlab.com/sat-metalab/scenic

## Dependencies

- [shmdata](https://gitlab.com/sat-metalab/shmdata)
- [switcher](https://gitlab.com/sat-metalab/switcher)

### Installation

* Install [shmdata](https://gitlab.com/sat-metalab/shmdata)
* Install [switcher](https://gitlab.com/sat-metalab/switcher)
* Install the required packages

        apt-get install nodejs nodejs-legacy npm chromium-browser

* Clone the **scenic** repo

        git clone https://gitlab.com/sat-metalab/scenic.git

* cd into the cloned repo

        cd scenic

* On your first checkout do the following ( subsequent pulls and build should update packages automatically )

        npm install

* Build and install the application ( your default g++ compiler must be 4.9 or greater ) 

        make build
        sudo make install

* Run scenic for the first time and check out the command line arguments with ```--help```

        scenic --help


### Development

#### Setup

* Install system dependencies:

        apt-get install nodejs nodejs-legacy npm chromium-browser

* Clone the **scenic** repo

        git clone https://gitlab.com/sat-metalab/scenic.git

* cd into the cloned repo

        cd scenic

* Install local libraries ( your default g++ compiler must be 4.9 or greater ) 

        npm install

* Launch scenic from the development directory in watch mode for development

        npm start


#### Running Tests
Tests are written for the server part of the application. You can run tests with the following command:

    npm test


#### Building documentation
In order to rebuild the documentation run the following command:

    make doc


#### Working on the node addon
When making changes to the node addon in **switcher**. The addon needs to be rebuilt and reinstalled locally in
**node_modules**. In order to do that you only need to tell npm to reinstall it using:

    npm install switcher

If you want to rebuild the node addon in debug use:

    npm rebuild --debug

To switch back to release build:

    npm rebuild switcher


#### Translation

##### Updating translation files:
Updates the localization files. It separates into 3 namespaces: switcher, server and client. Making it much easier to
manage and update.

    make i18n

The translations that were in the json file but were not found while scanning the source code will be moved to a
namespace_old.json file, where the can be retrieved if necessary or for reference purposes (when part of a string
changed, for example)

##### Using localized strings
In the source code use ```i18n.t('your string')```, in html use ```data-i18n="your string"```. Both are parsed by the
18n-parser and added to the localization resource files. The jQuery methos ```$.t('your string')``` can also be used.

##### How to translate

1. Run ```make i18n```
2. Translate the untranslated strings in locales/fr/namespace.json
3. Recover old translations from the namespace_old.json file if needed.

## Usage
Please refer to the documentation on Github: https://github.com/sat-metalab/SCENIC/wiki

### "Embedded" Configuration
When installing on a Scenic box a configuration file can be used to limit what is available in the interface. The file
can either be located in ~/.scenic/config.json or be specified on the command-line with -c or --config and providing a
valid json config file.

The config file should be in that format:

    {
        "privateQuiddities": [],
        "disabledPages": [],
        "disableRTP": true|false,
        "disableSourcesMenu": true|false,
        "disableDestinationsMenu": true|false,
        "propertiesPage": true|false,
        "stunServer": string,
        "turnServer": string,
        "sameLogin": true|false,
        "customMenus": []
    }

The following options are supported:

* **privateQuiddities**
    
    An array of quiddity class names that should not be available to the user

* **disabledPages**

    An array of quiddity class names that should not be available to the user

* **disabledPages**

    An array of page id (tabs on the left in the interface) that should not be available to the user. The currently
    available pages are: ```matrix```, ```control```, ```advanced```, ```settings``` and ```help```. Note that disabling
    the ```matrix``` page is counter intuitive as it is the default page.
    
* **disableRTP**

    Disables the creation of RTP destinations.

* **disableSourcesMenu**

    Disables the sources menu.

* **disableDestinationsMenu**
 
    Disables the destinations menu.
    
* **propertiesPage**
     
    Enable the properties page (Default to false).
    
* **stunServer**

    The address of the STUN server.

* **turnServer**
 
    The address of the TURN server.
    
* **sameLogin**
     
    Enable the same login for the SIP server and the TURN server.

* **customMenus**

    List of custom pre-configured quiddities to add to the connections table. 
    Each custom menu requires a label to display and a list of custom quiddities
    It's possible to add sub menus or directly a list of custom quiddities.

        "customMenus":[  
          {  
             "label":"label of the menu",
             "items":[  
                {  
                   "name":"Special Frequency 1",
                   "quiddity":"audiotestsrc",
                   "properties":{  
                      "frequency":420
                   }
                }
             ]
          },
          {  
             "label":"label of the menu2",
             "subMenus":[  
                {  
                   "label":"label submenu1",
                   "items":[  
                      {  
                         "name":"Special Frequency 1",
                         "quiddity":"audiotestsrc",
                         "properties":{  
                            "frequency":420
                         }
                      },
                      {  
                         "name":"Special Frequency 1",
                         "quiddity":"audiotestsrc",
                         "properties":{  
                            "frequency":420
                         }
                      }
                   ]
                },
                {  
                   "label":"label submenu2",
                   "items":[  
                      {  
                         "name":"Special Frequency 1",
                         "quiddity":"audiotestsrc",
                         "properties":{  
                            "frequency":420
                         }
                      },
                      {  
                         "name":"Special Frequency 1",
                         "quiddity":"audiotestsrc",
                         "properties":{  
                            "frequency":420
                         }
                      }
                   ]
                }
             ]
          }]

### Specify switcher configuration

It is possible to specify a switcher configuration file manually from scenic with the _-d_ option:

    scenic -d /path/to/custom/switcher/file.json


### Touch support

For optimal touch support consider launching **chromium-browser** with ```--touch-events```
